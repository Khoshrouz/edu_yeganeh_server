<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Only strings in profile view
    |--------------------------------------------------------------------------
    |
    |
    */

    'edit_admin' => 'ویرایش اطلاعات مدیریت',
    'edit' => 'ویرایش اطلاعات شخصی',
    'edit_description' => 'لطفا تمامی اطلاعات را تکمیل نمایید',
    'national_no' => 'کد ملی',
    'first_name' => 'نام',
    'last_name' => 'نام خانوادگی',
    'email' => 'آدرس الکترونیک',
    'melliCode' => 'کد ملی',
    'phone' => 'شماره همراه',
    'university_entrance' => 'سال ورود به دانشگاه',
    'licence_no' => 'کد پروانه کاربری',
    'group' => 'گروه کاربری',
    'major' => 'رشته',
    'address' => 'آدرس محل سکونت',
    'area' => 'منطقه فعالیت',
    'province' => 'استان فعالیت',
    'submit' => 'ثبت اطلاعات',
    'submit_new_major' => 'ثبت اطلاعات جدید رشته برای گروه منتخب',
    'submit_new' => 'ثبت اطلاعات جدید برای گروه انتخاب شده',
    'submit_update' => 'به روز رسانی',
    'viewed_at' => 'تاریخ مشاهده',
    'views' => 'مشاهده شده',
    'views_list' => 'لیست کاربر هایی که مشاهده کرده اند',
    'views_list_count' => 'تعداد کل مشاهده کنندگان:',
    'replies_list' => 'لیست کاربر هایی که پاسخ داده اند',
    'replies_list_count' => 'تعداد کل پاسخ دهنده ها:',
    'user' => 'کاربر',
    'writer' => 'نویسنده',
    'admin' => 'مدیریت',
    'admin_description' => 'لیست مدیران مجموعه',
    'action' => 'مدیریت',
    'image_user' => 'تصویر کاربر',
    'question_image' => 'تصویر سوال',
    'answer_image' => 'تصویر پاسخ',
    'image' => 'تصویر',
    'icon' => 'آیکن',
    'images' => 'تصاویر',
    'image_submit' => 'ثبت تصویر',
    'mails' => 'اطلاعیه ها',
    'news' => 'اخبار',
    'back_to_profile' => 'بازگشت به صفحه جزییات',
    'last_mails' => 'آخرین اطلاعیه ها',
    'last_news' => 'آخرین اخبار',
    'admin-mails' => 'لیست اطلاعیه های ارسال شده به توسط این مدیر',
    'admin-news' => 'لیست اخبار ارسال شده به توسط این مدیر',
    'user-mails' => 'لیست اطلاعیه های ارسال شده به کاربر',
    'all-mails' => 'لیست اطلاعیه های ارسال شده',
    'all-news' => 'لیست اخبار ارسال شده',
    'replies' => 'پاسخ های داده شده توسط کاربر به اطلاعیه',
    'all-types' => 'فرقی ندارد',
    'all' => 'همه',

    'created_at' => 'تاریخ ثبت',
    'updated_at' => 'تاریخ آخرین تغییرات',

    'mail_at' => 'تاریخ ثبت اطلاعیه',
    'reply_at' => 'تاریخ ثبت پاسخ',
    'replies_count' => 'تعدا پاسخ ها',
    'mail' => 'اطلاعیه',
    'news_item' => 'اخبار',
    'bill' => 'فیش',
    'attached_bill' => 'پیوست های به فیش',
    'attachments' => 'پیوست ها',
    'reply' => 'پاسخ',
    'title' => 'عنوان',
    'contains_images' => 'شامل عکس می باشد',
    'attached_images' => 'عکس های پیوستی',
    'mail_title' => 'عنوان اطلاعیه',
    'news_title' => 'عنوان اخبار',
    'bill_title' => 'عنوان فیش',
    'mail_content' => 'متن اطلاعیه',
    'news_content' => 'متن اخبار',
    'reply_title' => 'عنوان پاسخ',
    'reply_content' => 'متن پاسخ',
    'description' => 'توضیحات',

    'password_reset' => 'تغییر رمز ورود به پنل',
    'password_new' => 'رمز جدید',
    'password_confirm' => 'تکرار رمز جدید',
    'password_old' => 'رمز قدیمی',
    'password_submit' => 'تغییر رمز',

    'reset' => 'انتخاب از ابتدا',
    'targets' => 'مخاطب های این مطلب',
    'selected_targets' => 'لیست انتخاب شده ها',
    'broadcast' => 'ارسال شده برای تمامی کاربر ها در تمامی گروه ها',
    'before_update' => 'وضعیت اطلاعیه در حال حاضر',
    'replace_confirmation' => 'در صورت تایید لیست مخاطبین به روز شده و جایگزین قبلی می شوند',
    'previous_targets' => 'گروه قبلی',
    'new_targets' => 'محاطبین جدید اطلاعیه',
    'add_group' => 'گروه جدید',
    'add' => 'اضافه شود',
    'create' => 'ایجاد آیتم جدید',
    'delete' => 'حذف',
    'add_image' => 'اضافه کردن تصویر',
    'delete_image' => 'حذف تصویر',
    'delete_question' => 'این کاربر از سیستم حذف شود',
    'update' => 'ویرایش',
    'activities' => 'فعالیت کاربر',
    'publishing_status' => 'وضعیت نمایش',
    'activation_status' => 'وضعیت فعالیت',
    'available_status' => 'وضعیت دسترسی',
    'delete_status' => 'حذف کاربر',
    'last_update' => 'اخرین فعالیت',
    'enabled' => 'نمایش',
    'disabled' => 'عدم نمایش',
    'activated' => 'فعال',
    'deactivated' => 'مسدود , غیر فعال',

    'mails_count' => 'نامه های ارسال کرده',
    'news_count' => 'اخبار ارسال کرده',
    'details' => 'مشاهده جزییات',
    'profile' => 'مشاهده پروفایل',
    'select_files' => 'فایل ها را برای بارگزاری انتخاب کنید:',
    'select_files_screenshots' => 'عکس های قسمت توضیحات برای بارگزاری انتخاب کنید:',
    'select_file' => 'فایل  را برای بارگزاری انتخاب کنید:',
    'select_file_icon' => 'آیکن اصلی  را برای بارگزاری انتخاب کنید:',

    'admins_count' => 'تعداد کل مدیران',
    'users' => 'کاربران',
    'users_description' => 'لیست کاربران بهمراه جزییات',
    'users_details' => 'آمار کاربران',
    'users_count' => 'تعداد کاربران',
    'mails_sent' => 'تعداد اطلاعیه ها',
    'news_sent' => 'تعداد اخبار',
    'replies_income' => 'تعداد پاسخ ها',
    'last_reply' => 'آخرین پاسخ به اطلاعیه',


    'create_target_group' => '(گروهها / افراد) دریافت کننده',
    'create_group' => 'ایجاد گروه جدید',
    'update_group' => 'مدیرت گروه',
    'create_major' => 'ایجاد رشته جدید',
    'update_major' => 'مدیریت رشته',
    'placeholder_update_major' => 'ورودی برای به روزرسانی رشته...',
    'placeholder_new_major' => 'رشته جدید...',
    'no-change' => 'بدون تغییر',
    'placeholder_update_group' => 'ورودی برای به روزرسانی گروه...',
    'placeholder_new_group' => 'گروه جدید...',



    'parts' => 'بخش ها',
    'questions' => 'سوالات',
    'part' => 'بخش',
    'price' => 'مبلغ',
    'price_rial' => 'مبلغ (ریال)',
    'comment' => 'نظر',
    'lesson' => 'درس',
    'chapters' => 'فصل ها',
    'chapter' => 'فصل',
    'actions' => 'امکانات',
    'course' => 'مبحث',
    'stars' => 'ستاره',
    'stars_total' => 'ستاره کل',
    'question_content' => 'متن سوال',
    'options' => 'گزینه ها',
    'first_option' => 'گزینه اول',
    'second_option' => 'گزینه دوم',
    'third_option' => 'گزینه سوم',
    'fourth_option' => 'گزینه چهارم',
    'answer' => 'پاسخ درست',
    'answer_content' => 'پاسخ تشریحی',
    'comment_content' => ' متن نظر',
    'report_content' => ' متن ریپورت',
    'report_option' => ' نوع ریپورت',
    'answer_image' => 'تصویر پاسخ',
    'back' => 'بازگشت به صفحه قبلی',
    'downloads_count' => 'تعداد دانلود',
    'questions_count' => 'تعداد سوالات',
    'comprehensive_questions_count' => 'تعداد سوالات جامع',

    'reports' => 'ریپورت ها',
    'transaction_title' => 'عنوان تراکنش',
    'bank_data' => 'اطلاعات دریافتی از بانک',
    'status' => 'وضعیت',
    'comments' => 'نظرات',
    'favorites' => 'علاقه ها',
    'transactions' => 'تراکنش های مالی',
    'confirmed_comments' => 'نظرات تایید شده',
    'not_confirmed_reports' => 'ریپورت های تایید نشده',
    'not_confirmed_comments' => 'نظرات تایید نشده',
    'confirmed' => 'تایید شده',
    'not_confirmed' => 'تایید شود؟',
    'import_excel_description' => 'فایل اکسل مورد نظر را بارگذاری کنید',
    'import_excel_description_course_need' => 'مبحثی یافت نشد',
    'duration_months' => 'ماهه',
    'duration' => 'مدت زمان',
    'expires_at' => 'پایان اعتبار',


);
