<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Only strings in menu
    |--------------------------------------------------------------------------
    |
    |
    */

    'dashboard' => 'داشبورد',
    'profile' => 'پروفایل مدیر',
    'profile_user' => 'پروفایل کاربر',
    'create_admin' => 'ایجاد مدیر جدید',
    'create_user' => 'ایجاد کاربر جدید',
    'create_lesson' => 'ایجاد درس جدید',
    'create_chapter' => 'ایجاد فصل جدید',
    'create_news' => 'ایجاد اخبار جدید',
    'admins' => 'مدیران',
    'groups' => 'مدیرت گروه ها',
    'users' => 'کاربران',
    'import_excel' => 'بارگذاری سوالات',
    'import_excel_description' => 'بارگذاری سوالات به صورت اکسل',
    'add-lesson' => 'افزودن درس جدید',
    'add-chapter' => 'افزودن فصل جدید',
    'add-part' => 'افزودن بخش جدید',
    'add-news' => 'افزودن اخبار جدید',
    'add-user' => 'افزودن کاربر جدید',
    'add-admin' => 'افزودن مدیر جدید',
    'add-question' => 'افزودن سوال جدید',
    'lessons' => 'تمامی درس ها',
    'reports' => 'ریپورت ها',
    'comments' => 'نظرات',
    'transactions' => 'مراودات مالی',
    'chapters' => 'تمامی فصل ها',
    'news' => 'تمامی اخبار',
    'lesson' => 'جزییات درس',
    'chapter' => 'جزییات فصل',
    'part' => 'جزییات بخش',
    'news-item' => 'جزییات اخبار',
    'update_lesson' => 'ویرایش درس',
    'update_chapter' => 'ویرایش فصل',
    'update_news' => 'ویرایش خبر',


];