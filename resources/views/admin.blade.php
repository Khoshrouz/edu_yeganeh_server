@extends('layouts.rtl-admin')

@section('content')


<div class="row">
    <div class="col-md-4 pt-5">
        <div class="card card-profile">
            <div class="card-avatar">
                @if(isset(auth('admin')->user()->image->link))
                <a target="_blank" href="{{route('admin-image',['filename'=> auth('admin')->user()->image->link])}}">
                    <img class="img" src="{{route('admin-image',['filename'=> auth('admin')->user()->image->link])}}" />
                </a>
                @else
                <img class="img" src="{{asset('assets/img/faces/marc.jpg')}}" />
                @endif
            </div>

            <div class="card-body">
                <div class="row text-right d-flex justify-content-center">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="2">
                                        <h5 class="card-category text-center text-dark">{{auth('admin')->user()->name}}</h5>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="2">
                                        <h5 class="card-category text-center text-dark">{{auth('admin')->user()->email}}</h5>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-8 pt-5">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header card-header-primary card-header-icon">
                        <div class="card-icon">
                            <i class="material-icons">group</i>
                        </div>
                        <p class="card-category">{{__('menu.admins')}}</p>
                    </div>
                    <div class="card-footer">
                        <div class="stats">
                            <a class="nav-link" href="{{route('admin.admins')}}">
                                <i class="material-icons">details</i>
                                {{__('profile.details')}}
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header card-header-rose card-header-icon">
                        <div class="card-icon">
                            <i class="material-icons">group</i>
                        </div>
                        <p class="card-category">{{__('menu.users')}}</p>
                    </div>
                    <div class="card-footer">
                        <div class="stats">
                            <a class="nav-link" href="{{route('admin.users')}}">
                                <i class="material-icons">details</i>
                                {{__('profile.details')}}
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

{{--reports section--}}
<div class="col-md-12">
    <div class="card">
        <div class="card-header card-header-primary">
            <div class="d-flex justify-content-between">
                <h4 class="card-title">{{__('profile.reports')}} - ({{__('profile.not_confirmed')}}: {{$not_confirmed_reports_count}} )</h4>
                <button class="btn btn-white" data-toggle="collapse" href="#collapseReportsBody">
                    <i class="material-icons">view_headline</i>
                </button>
            </div>

        </div>

        <div class="collapse show" id="collapseReportsBody">
            <div class="card-body">
                <div class="table-responsive table-striped">
                    <table class="table table-hover overflow-auto text-right" id="searchTable">
                        <thead class="thead-dark">
                            <th>
                                #
                            </th>
                            <th>
                                {{__('profile.part')}}
                            </th>
                            <th>
                                {{__('profile.question_content')}}
                            </th>
                            <th>
                                {{__('profile.report_content')}}
                            </th>
                            <th>
                                {{__('profile.report_option')}}
                            </th>
                            <th>
                                {{__('profile.user')}}
                            </th>
                            <th>
                                {{__('profile.confirmed')}}
                            </th>

                        </thead>

                        @foreach($not_confirmed_reports as $report)
                        <tr>
                            <td>
                                {{$report->id}}
                            </td>
                            <td>
                                <a target="_blank" href="{{route('admin.profile-part',['id'=>$report->question->part->id])}}">
                                    {{$report->question->part->title}}
                                </a>
                            </td>
                            <td>
                                {!! substr($report->question->question_content , 0 ,100) !!}...
                            </td>
                            <td class="max-width-td">
                                {{$report->content}}
                            </td>
                            <td>
                                {{$report->report_option->title}}
                            </td>
                            <td>
                                <a href="{{route('admin.profile-user', ['id'=>$report->user->id])}}">{{$report->user->email}}</a>
                            </td>
                            <td>
                                <a class="btn {{($report->confirm)? "btn-success":"btn-danger"}}" href="{{route('admin.report-confirm',['id'=>$report->id])}}">
                                    {{($report->confirm)?__('profile.confirmed'):__('profile.not_confirmed')}}
                                </a>
                            </td>
                        <tr>
                            @endforeach

                    </table>

                    <div class="d-flex justify-content-center border-top pt-2">
                        {{$not_confirmed_reports->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{--not confirmed comment section--}}
<div class="col-md-12">
    <div class="card">
        <div class="card-header card-header-primary">
            <div class="d-flex justify-content-between">
                <h4 class="card-title">{{__('profile.comments')}} - ({{__('profile.not_confirmed')}}: {{$not_confirmed_comments_count}} )</h4>
                <button class="btn btn-white" data-toggle="collapse" href="#collapseNotConfirmedCommentsBody">
                    <i class="material-icons">view_headline</i>
                </button>
            </div>
        </div>
        <div class="collapse show" id="collapseNotConfirmedCommentsBody">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover overflow-auto text-right" id="searchTable">
                        <thead class="thead-dark">
                            <th scope="col">#</th>
                            <th scope="col">{{__('profile.chapter')}}</th>
                            <th scope="col">{{__('profile.comment_content')}}</th>
                            <th scope="col">{{__('profile.user')}}</th>
                            <th scope="col">{{__('profile.stars')}} {{__('profile.comment')}}</th>
                            <th scope="col">{{__('profile.stars')}} {{__('profile.chapter')}}</th>
                            <th scope="col">{{__('profile.confirmed')}}</th>
                        </thead>

                        <tbody>
                            @foreach($not_confirmed_comments as $comment)
                            <tr>
                                <td>
                                    {{$comment->id}}
                                </td>
                                <td>
                                    <a href="{{route('admin.profile-chapter',['id'=>$comment->commentable->id])}}">
                                        {{$comment->commentable->title}}
                                    </a>
                                </td>
                                <td>
                                    {{$comment->content}}
                                </td>
                                <td>
                                    <a href="{{route('admin.profile-user', ['id'=>$comment->user->id])}}">{{$comment->user->email}}</a>
                                </td>
                                <td>
                                    {{$comment->stars}}
                                </td>
                                <td>
                                    {{$comment->commentable->stars}}
                                </td>
                                <td>
                                    <a class="btn {{($comment->confirm)? "btn-success":"btn-danger"}}" href="{{route('admin.comment-confirm',['id'=>$comment->id])}}">
                                        {{($comment->confirm)?__('profile.confirmed'):__('profile.not_confirmed')}}
                                    </a>
                                </td>
                            <tr>
                                @endforeach
                        </tbody>
                    </table>
                    <div class="d-flex justify-content-center border-top pt-2">
                        {{$not_confirmed_comments->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{--
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header card-header-danger">
                        <h4 class="card-title">{{__('profile.last_mails')}}</h4>
<p class="card-category">{{$mails_count}}</p>
</div>
<div class="card-body table-responsive">
    <table class="table table-hover overflow-auto text-nowrap">
        <thead class="text-warning">
            <th>#</th>
            <th>{{__('profile.mail_at')}}</th>
            <th>{{__('profile.updated_at')}}</th>
            <th>{{__('profile.title')}}</th>
        </thead>
        <tbody>
            @foreach($mails as $mail)
            <tr>
                <td>
                    {{$mail->id}}
                </td>
                <td>
                    {{\Morilog\Jalali\Jalalian::fromDateTime($mail->created_at)}}
                </td>
                <td>
                    {{\Morilog\Jalali\Jalalian::fromDateTime($mail->updated_at)}}
                </td>
                <td>
                    <a href="{{route('admin.mail', ['id'=>$mail->id])}}">{{$mail->title}}</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
</div>
</div>
<div class="col-lg-12 col-md-12">
    <div class="card">
        <div class="card-header card-header-warning">
            <h4 class="card-title">{{__('profile.users_details')}}</h4>
            <p class="card-category">{{$users_count}}</p>
        </div>
        <div class="card-body table-responsive">
            <table class="table table-hover overflow-auto text-nowrap">
                <thead class="text-warning">
                    <th>#</th>
                    <th>{{__('profile.first_name')}} {{__('profile.last_name')}}</th>
                    <th>{{__('profile.email')}}</th>
                </thead>
                <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td>
                            {{$user->id}}
                        </td>
                        <td>
                            <a href="{{route('admin.profile-user', ['id'=>$user->id])}}">{{$user->name}}</a>
                        </td>
                        <td>
                            {{$user->email}}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
--}}

<script>
    document.getElementById("view_name").innerHTML = "{{__('menu.dashboard')}}";
</script>


@endsection