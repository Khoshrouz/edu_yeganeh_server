@extends('layouts.rtl-admin')

@section('content')

<div class="row">
    <div class="col-12">
        <a class="btn btn-info" href="{{route('admin.profile-lesson',['id'=>$lesson_id])}}">
            {{__('profile.back')}}
        </a>
    </div>
    <div class="col-md-12">
        {{--personal info section--}}
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">{{__('profile.create')}}</h4>
            </div>
            <div class="card-body">
                <form method="POST" action='{{ route('admin.create-chapter') }}' aria-label="{{ __('forms.create_admin') }}" enctype="multipart/form-data" onsubmit="return ConfirmDelete();">
                    @csrf
                    <div class="row mb-2">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="bmd-label-floating">{{__('profile.lesson')}}</label>

                                <select class="custom-select mr-sm-2" name="lesson_id" class="form-control @error('lesson') is-invalid @enderror">
                                    <option value="{{$lesson->id}}">{{$lesson->title}}</option>
                                </select>

                                @error('lesson')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror

                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="bmd-label-floating">{{__('profile.title')}}</label>
                                <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" required value="{{ old('title') }}" autocomplete="title" autofocus>
                                @error('title')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12 p-3">
                            <label for="file" class="d-block">{{__('profile.select_file_icon')}}</label>
                            <input type="file" id="file" name="file">
                            @error('file')
                            <strong class="text-danger">{{ $errors->first('file')  }}</strong>
                            @enderror
                        </div>

                        {{--end of profile details--}}
                    </div>

                    <button type="submit" class="btn btn-primary pull-right">{{__('profile.submit')}}</button>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
        {{--end of personal info section--}}


    </div>
</div>

@endsection

@section('footer')

<script>
    document.getElementById("view_name").innerHTML = "{{__('menu.create_chapter')}}";
</script>

@endsection