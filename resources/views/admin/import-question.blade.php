@extends('layouts.rtl-admin')

@section('content')

<div class="row">
    <div class="col-md-12">
        {{--personal info section--}}
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">{{__('menu.import_excel_description')}}</h4>
            </div>
            <div class="card-body">
                @if(sizeof($courses) == 0)
                <h4 class="card-title">{{__('profile.import_excel_description_course_need')}}</h4>
                @else
                <form method="POST" action='{{ route('admin.import-question') }}' aria-label="{{ __('forms.create_admin') }}" enctype="multipart/form-data" onsubmit="return ConfirmDelete();">
                    @csrf
                    <div class="row mb-2 p-3">
                        <div class="col-md-12">
                            <div class="form-group py-3">
                                <label class="bmd-label-floating">{{__('profile.course')}}</label>

                                <select class="custom-select mr-sm-2" name="course_id" class="form-control @error('course') is-invalid @enderror">
                                    @foreach($courses as $course)
                                    <option value="{{$course->id}}">{{$course->title}}</option>
                                    @endforeach
                                </select>

                                @error('course')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('course') }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="col-md-12 py-3">
                                <label for="file" class="d-block">{{__('profile.import_excel_description')}}</label>
                                <input type="file" id="file" name="file">
                                @error('file')
                                <strong class="text-danger">{{ $errors->first('file')  }}</strong>
                                @enderror
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary m-3 pull-right">{{__('profile.submit')}}</button>
                        <div class="clearfix"></div>
                </form>
                @endif
            </div>
        </div>
        {{--end of personal info section--}}
    </div>
</div>

@endsection

@section('footer')

<script>
    document.getElementById("view_name").innerHTML = "{{__('menu.import_excel')}}";
</script>
@endsection