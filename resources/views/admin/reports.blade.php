@extends('layouts.rtl-admin')

@section('content')

    {{--main--}}
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title ">{{__('profile.reports')}}</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive table-striped">
                        <table class="table table-hover overflow-auto text-right" id="searchTable">
                            <thead class=" text-primary">
                            <th>
                                #
                            </th>
                            <th>
                                {{__('profile.part')}}
                            </th>
                            <th>
                                {{__('profile.question_content')}}
                            </th>
                            <th>
                                {{__('profile.report_content')}}
                            </th>
                            <th>
                                {{__('profile.report_option')}}
                            </th>
                            <th>
                                {{__('profile.user')}}
                            </th>
                            <th>
                                {{__('profile.confirmed')}}
                            </th>
                            </thead>
                            <tbody>

                            @foreach($reports as $report)
                                <tr>
                                    <td>
                                        {{$report->id}}
                                    </td>
                                    <td>
                                        <a target="_blank"
                                           href="{{route('admin.profile-part',['id'=>$report->question->part->id])}}">
                                            {{$report->question->part->title}}
                                        </a>
                                    </td>
                                    <td>
                                        {!! substr($report->question->question_content , 0 ,100) !!}...
                                    </td>
                                    <td class="max-width-td">
                                        {{$report->content}}
                                    </td>
                                    <td>
                                        {{$report->report_option->title}}
                                    </td>
                                    <td>
                                        <a href="{{route('admin.profile-user', ['id'=>$report->user->id])}}">{{$report->user->email}}</a>
                                    </td>
                                    <td>
                                        <a class="btn {{($report->confirm)? "btn-success":"btn-danger"}}"
                                           href="{{route('admin.report-confirm',['id'=>$report->id])}}">
                                            {{($report->confirm)?__('profile.confirmed'):__('profile.not_confirmed')}}
                                        </a>
                                    </td>
                                <tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row d-flex justify-content-center mt-3">
                    {{ $reports->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script src="{{asset('js/seoa-script-search.js')}}"></script>
    <!--   searching filters  -->
    <script src="{{asset('js/filter-script.js')}}" type="text/javascript"></script>

    <script>
        document.getElementById("view_name").innerHTML = "{{__('menu.reports')}}";
    </script>
@endsection