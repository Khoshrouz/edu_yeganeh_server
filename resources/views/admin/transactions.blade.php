@extends('layouts.rtl-admin')

@section('content')

    {{--main--}}
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title ">{{__('profile.transactions')}}</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive table-striped">
                        <table class="table table-hover overflow-auto text-right" id="searchTable">
                            <thead class=" text-primary">
                            <th scope="col">#</th>
                            <th scope="col">{{__('profile.bank_data')}}</th>
                            <th scope="col">{{__('profile.user')}}</th>
                            <th scope="col">{{__('profile.status')}}</th>
                            </thead>
                            <tbody>
                            @foreach($transactions as $transaction)
                                <tr>
                                    <td>
                                        {{$transaction->id}}
                                    </td>

                                    <td>
                                        {{$transaction->bank_data}}
                                    </td>
                                    <td>
                                        <a href="{{route('admin.profile-user', ['id'=>$transaction->user->id])}}">{{$transaction->user->email}}</a>
                                    </td>
                                    <td>
                                        {{$transaction->status}}
                                    </td>
                                <tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row d-flex justify-content-center mt-3">
                    {{ $transactions->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script src="{{asset('js/seoa-script-search.js')}}"></script>
    <!--   searching filters  -->
    <script src="{{asset('js/filter-script.js')}}" type="text/javascript"></script>

    <script>
        document.getElementById("view_name").innerHTML = "{{__('menu.transactions')}}";
    </script>
@endsection