@extends('layouts.rtl-admin')

@section('header')
    <link href="{{asset('css/bootstrap-toggle.min.css')}}" rel="stylesheet"/>
    <script src="{{asset('js/bootstrap-toggle.min.js')}}" type="text/javascript"></script>
@endsection

@section('content')

    <div class="row">

        <div class="col-md-6 pt-5">
            <div class="card card-profile">
                <div class="card-avatar">
                    @if(isset($user->image->link))
                        <a target="_blank" href="{{route('user-image',['filename'=> $user->image->link])}}">
                            <img class="img" src="{{route('user-image',['filename'=> $user->image->link])}}"/>
                        </a>
                    @else
                        <img class="img" src="{{asset('assets/img/faces/marc.jpg')}}"/>
                    @endif
                </div>

                <div class="card-body">
                    <div class="row text-right d-flex justify-content-center">
                        <table>
                            <tbody>
                            @if(isset($user->image->link))
                                <tr>
                                    <td class="text-left">{{__('profile.delete_image')}}</td>

                                    <td>
                                        <a class="btn btn-danger"
                                        onclick="return ConfirmDelete();"
                                           href="{{route('admin.delete-image',['id'=>$user->image->id])}}">
                                            {{__('profile.delete')}}
                                        </a>
                                    </td>
                                <tr>
                            @else
                                <tr>
                                    <td colspan="2" class="text-center">
                                        <form method="POST" action='{{ route('admin.create-user-image') }}'
                                              aria-label="{{ __('forms.create_admin') }}"
                                              enctype="multipart/form-data"
                                              onsubmit="return ConfirmDelete();">
                                            @csrf
                                            <input type="hidden" value="{{$user->id}}" name="id">
                                            <label for="file">{{__('profile.select_file')}}</label>
                                            <input type="file" id="file" name="file">
                                            @error('file')
                                            <strong class="text-danger">{{ $errors->first('file')  }}</strong>
                                            @enderror

                                            <button type="submit"
                                                    class="btn btn-primary pull-center">{{__('profile.submit')}}</button>
                                            <div class="clearfix"></div>
                                        </form>
                                    </td>
                                <tr>
                            @endif

                            <tr>
                                <td colspan="2">
                                    <hr>
                                    <h5 class="card-category text-center text-dark">{{$user->name}}</h5>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div>
                        @if(Auth::guard('admin')->user()->hasRole('super-admin'))
                            <input type="checkbox"
                                   name="switch_user_activity"
                                   class="{{($user->active == 1)? "true_activity" : "false_activity"}}"
                                   data-toggle="toggle"
                                   data-on="{{__('profile.activated')}}"
                                   data-off="{{__('profile.deactivated')}}"
                                   data-onstyle="success" data-offstyle="danger"
                                   value="{{$user->id}}">
                        @else
                            @if($user->active)
                                <span class="text-white btn btn-success">
                                                                {{__("profile.activated")}}
                                                            </span>
                            @else
                                <span class="text-white btn btn-danger">
                                                                {{__("profile.deactivated")}}
                                                            </span>
                            @endif
                        @endif
                    </div>

                    @if(Auth::guard('admin')->user()->hasRole('super-admin'))

                        <hr>
                        <div class="stats">
                            <form method="POST"
                                  action='{{ route('admin.delete-user') }}'
                                  onsubmit="return ConfirmDelete();">
                                @csrf
                                <input type="hidden" name="id" value="{{$user->id}}">
                                <input type="submit" class="btn btn-warning"
                                       value="{{__('profile.delete_question')}}">
                            </form>
                        </div>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-md-6 pt-5">
            <div class="card card-profile">

                <div class="card-body">
                    <div class="row text-right">
                        <div class="col-md-12">
                            {{--personal info section--}}
                            <div class="card">
                                <div class="card-header card-header-primary">
                                    <h4 class="card-title">{{__('profile.edit')}}</h4>
                                </div>
                                <div class="card-body">
                                    <form method="POST" action='{{ route('admin.update-user') }}'
                                          aria-label="{{ __('forms.update_admin') }}"
                                          onsubmit="return ConfirmDelete();">
                                        @csrf
                                        <input type="hidden" name="id" value="{{$user->id}}">
                                        <div class="row mb-2">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">{{__('profile.first_name')}}</label>
                                                    <input id="name" type="text"
                                                           class="form-control @error('name') is-invalid @enderror"
                                                           name="name" value="{{$user->name}}" required
                                                           autocomplete="name" autofocus>
                                                    @error('name')
                                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">{{__('profile.email')}}</label>
                                                    <input type="email" name="email" autofocus id="email"
                                                           value="{{$user->email}}" required
                                                           class="form-control @error('email') is-invalid @enderror">
                                                    @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            {{--profile details--}}
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">{{__('profile.melliCode')}}</label>
                                                    <input type="number" name="melliCode" autofocus id="melliCode"
                                                           value="{{$user->melliCode}}" required
                                                           class="form-control @error('melliCode') is-invalid @enderror">
                                                    @error('melliCode')
                                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">{{__('profile.phone')}}</label>
                                                    <input type="number" name="phone" autofocus id="phone"
                                                           value="{{$user->phone}}" required
                                                           class="form-control @error('phone') is-invalid @enderror">
                                                    @error('phone')
                                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            {{--end of profile details--}}
                                        </div>

                                        <button type="submit"
                                                class="btn btn-primary pull-right">{{__('profile.submit')}}</button>
                                        <div class="clearfix"></div>
                                    </form>
                                </div>
                            </div>
                            {{--end of personal info section--}}
                        </div>
                    </div>


                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="card card-profile">

                <div class="card-body">
                    <div class="row text-right">
                        <div class="col-md-12">
                            {{--personal info section--}}
                            <div class="card">
                                <div class="card-header card-header-primary">
                                    <h4 class="card-title">{{__('profile.activities')}}</h4>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>{{__('profile.last_update')}}</td>
                                                <td>{{\Morilog\Jalali\Jalalian::fromDateTime($user->updated_at)}}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                            {{--end of personal info section--}}
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-md-12">
            <div class="card card-profile">

                <div class="card-body">
                    <div class="row text-right">
                        <div class="col-md-12">
                            {{--personal info section--}}
                            <div class="card">
                                <div class="card-header card-header-danger">
                                    <div class="d-flex justify-content-between">
                                        <h4 class="card-title">{{__('profile.reports')}} - ({{__('profile.not_confirmed')}}: {{$reports_count}})</h4>
                                        <button class="btn btn-white" data-toggle="collapse"
                                                href="#collapseReportsBody">
                                            <i class="material-icons">view_headline</i>
                                        </button>
                                    </div>
                                </div>
                                <div class="collapse hide" id="collapseReportsBody">
                                    <div class="card-body">
                                        <div class="table-responsive table-striped">
                                            <table class="table table-hover overflow-auto text-right"
                                                   id="searchTable">
                                                <thead class="thead-dark">
                                                <th scope="col">{{__('profile.report_content')}}</th>
                                                <th scope="col">{{__('profile.part')}}</th>
                                                <th scope="col">{{__('profile.report_option')}}</th>
                                                <th scope="col">{{__('profile.confirmed')}}</th>
                                                </thead>

                                                <tbody>
                                                @foreach($reports as $report)
                                                    <tr>
                                                        <td>
                                                            {{$report->content}}
                                                        </td>
                                                        <td>
                                                            <a target="_blank"
                                                               href="{{route('admin.profile-part',['id'=>$report->question->part_id])}}">
                                                                {{$report->question->question_content}}
                                                            </a>
                                                        </td>
                                                        <td>
                                                            {{$report->report_option->title}}
                                                        </td>
                                                        <td>
                                                            <a class="btn {{($report->confirm)? "btn-success":"btn-danger"}}"
                                                               href="{{route('admin.comment-confirm',['id'=>$report->id])}}">
                                                                {{($report->confirm)?__('profile.confirmed'):__('profile.not_confirmed')}}
                                                            </a>
                                                        </td>
                                                    <tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                            <div class="d-flex justify-content-center border-top pt-2">
                                                {{$reports->links()}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{--end of personal info section--}}
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-md-12">
            <div class="card card-profile">

                <div class="card-body">
                    <div class="row text-right">
                        <div class="col-md-12">
                            {{--personal info section--}}
                            <div class="card">
                                <div class="card-header card-header-warning">
                                    <div class="d-flex justify-content-between">
                                        <h4 class="card-title">{{__('profile.comments')}} - ({{__('profile.not_confirmed')}}: {{$comments_count}})</h4>
                                        <button class="btn btn-white" data-toggle="collapse"
                                                href="#collapseCommentsBody">
                                            <i class="material-icons">view_headline</i>
                                        </button>
                                    </div>
                                </div>
                                <div class="collapse hide" id="collapseCommentsBody">
                                    <div class="card-body">
                                        <div class="table-responsive table-striped">
                                            <table class="table table-hover overflow-auto text-right"
                                                   id="searchTable">
                                                <thead class="thead-dark">
                                                <th scope="col">{{__('profile.comment_content')}}</th>
                                                <th scope="col">{{__('profile.lesson')}}</th>
                                                <th scope="col">{{__('profile.stars')}}</th>
                                                <th scope="col">{{__('profile.confirmed')}}</th>
                                                </thead>

                                                <tbody>
                                                @foreach($comments as $comment)
                                                    <tr>
                                                        <td>
                                                            {{$comment->content}}
                                                        </td>
                                                        <td>
                                                            <a target="_blank"
                                                               href="{{route('admin.profile-lesson',['id'=>$comment->commentable_id])}}">
                                                                {{$comment->commentable->title}}
                                                            </a>
                                                        </td>
                                                        <td>
                                                            {{$comment->stars}}
                                                        </td>
                                                        <td>
                                                            <a class="btn {{($comment->confirm)? "btn-success":"btn-danger"}}"
                                                               href="{{route('admin.comment-confirm',['id'=>$comment->id])}}">
                                                                {{($comment->confirm)?__('profile.confirmed'):__('profile.not_confirmed')}}
                                                            </a>
                                                        </td>
                                                    <tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                            <div class="d-flex justify-content-center border-top pt-2">
                                                {{$comments->links()}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{--end of personal info section--}}
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-md-12">
            <div class="card card-profile">

                <div class="card-body">
                    <div class="row text-right">
                        <div class="col-md-12">
                            {{--personal info section--}}
                            <div class="card">
                                <div class="card-header card-header-primary">
                                    <div class="d-flex justify-content-between">
                                        <h4 class="card-title">{{__('profile.transactions')}}</h4>
                                        <button class="btn btn-white" data-toggle="collapse"
                                                href="#collapseTransactionsBody">
                                            <i class="material-icons">view_headline</i>
                                        </button>
                                    </div>
                                </div>
                                <div class="collapse hide" id="collapseTransactionsBody">
                                    <div class="card-body">
                                        <div class="table-responsive table-striped">
                                            <table class="table table-hover overflow-auto text-right"
                                                   id="searchTable">
                                                <thead class="thead-dark">
                                                <th scope="col">{{__('profile.chapter')}}</th>
                                                <th scope="col">{{__('profile.expires_at')}}</th>
                                                <th scope="col">{{__('profile.bank_data')}}</th>
                                                <th scope="col">{{__('profile.status')}}</th>
                                                </thead>

                                                <tbody>
                                                @foreach($purchases as $purchase)
                                                    <tr>
                                                        <td>
                                                            {{$purchase->chapter->title}}
                                                        </td>
                                                        <td>
                                                            {{\Morilog\Jalali\Jalalian::fromDateTime($purchase->expires_at)}}
                                                        </td>
                                                        <td>
                                                            {{$purchase->transaction->bank_data}}
                                                        </td>
                                                        <td>
                                                            {{$purchase->transaction->status}}
                                                        </td>
                                                    <tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                            <div class="d-flex justify-content-center border-top pt-2">
                                                {{$transactions->links()}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{--end of personal info section--}}
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-md-12">
            <div class="card card-profile">

                <div class="card-body">
                    <div class="row text-right">
                        <div class="col-md-12">
                            {{--personal info section--}}
                            <div class="card">
                                <div class="card-header card-header-primary">
                                    <div class="d-flex justify-content-between">
                                        <h4 class="card-title">{{__('profile.favorites')}}</h4>
                                        <button class="btn btn-white" data-toggle="collapse"
                                                href="#collapseFavoritesBody">
                                            <i class="material-icons">view_headline</i>
                                        </button>
                                    </div>
                                </div>
                                <div class="collapse hide" id="collapseFavoritesBody">
                                    <div class="card-body">
                                        <div class="table-responsive table-striped">
                                            <table class="table table-hover overflow-auto text-right"
                                                   id="searchTable">
                                                <thead class="thead-dark">
                                                <th scope="col">{{__('profile.question_content')}}</th>
                                                <th scope="col">{{__('profile.part')}}</th>
                                                </thead>

                                                <tbody>
                                                @foreach($favorites as $question)
                                                    <tr>
                                                        <td>
                                                            {{$question->question_content}}
                                                        </td>
                                                        <td>
                                                            <a target="_blank"
                                                               href="{{route('admin.profile-part',['id'=>$question->part_id])}}">
                                                                {{$question->part->title}}
                                                            </a>
                                                        </td>
                                                    <tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                            <div class="d-flex justify-content-center border-top pt-2">
                                                {{$favorites->links()}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{--end of personal info section--}}
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection

@section('footer')

    <script>
        document.getElementById("view_name").innerHTML = "{{__('menu.profile_user')}}";
    </script>

    <script src="{{asset('js/seoa-script.js')}}"></script>
@endsection