@extends('layouts.rtl-admin')

@section('header')
@endsection

@section('content')

    {{--search box--}}
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header card-header-rose">
                    <h4 class="card-title ">{{ __('filters.search_in_db') }}</h4>
                </div>

                <div class="card-body">
                    <form method="POST" action='{{ route('admin.search-lessons') }}'
                        aria-label="{{ __('forms.create_admin') }}" onsubmit="return ConfirmDelete();">
                        @csrf
                        <div class="row mb-2">
                            <div class="col-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">{{ __('profile.title') }}</label>
                                    <input id="title" type="text" class="form-control @error('title') is-invalid @enderror"
                                        name="title" value="{{ old('title') }}" autocomplete="title" autofocus>

                                    @error('title')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group col-6">
                                <select id="inputState" class="form-control" name="available">
                                    <option value="" selected>{{ __('filters.available_status') }}</option>
                                    <option value="1">{{ __('filters.activated') }}</option>
                                    <option value="0">{{ __('filters.deactivated') }}</option>
                                </select>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary pull-right col-12">{{ __('filters.search') }}</button>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>

        </div>
    </div>

    {{--main--}}
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title ">{{ __('profile.admin') }}</h4>
                    <p class="card-category"> {{ __('profile.admin_description') }}</p>
                </div>
                <div class="card-body">
                    <div class="row p-4">
                        <div class="col-6">
                            <input type="text" class=" searchText" id="searchTitleInput"
                                onkeyup="searchTableFunc('searchTitleInput',1)" placeholder="{{ __('filters.title') }}">

                        </div>
                        <div class="col-6">
                            <!-- Control buttons -->
                            <div id="myBtnContainer">
                                <button class="btn" onclick="filterSelection('',3)"> {{ __('filters.both') }}</button>
                                <button class="btn" onclick="filterSelection('{{ __('filters.activated') }}',3)">
                                    {{ __('filters.activated') }}</button>
                                <button class="btn" onclick="filterSelection('{{ __('filters.deactivated') }}',3)">
                                    {{ __('filters.deactivated') }}</button>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive table-striped">
                        <table class="table table-hover overflow-auto text-nowrap text-right" id="searchTable">
                            <thead class="thead-dark">
                                <th>
                                    #
                                </th>
                                {{--@if (Auth::guard('admin')->user()->is_super)
                                    <th>
                                        {{ __('profile.delete') }}
                                    </th>
                                @endif--}}
                                <th>
                                    {{ __('profile.title') }}
                                </th>
                                <th>
                                    {{ __('profile.course') }}
                                </th>
                                <th>
                                    {{ __('profile.chapters') }}
                                </th>
                                <th>
                                    {{ __('profile.actions') }}
                                </th>

                            </thead>
                            <tbody>
                                @foreach ($lessons as $lesson)
                                    <tr>
                                        <td class="text-primary">
                                            {{ $lesson->id }}
                                        </td>
                                        {{--@if (Auth::guard('admin')->user()->is_super)
                                            <td>
                                                <div class="stats">
                                                    <form method="POST" action='{{ route('admin.delete-admin') }}'
                                                        onsubmit="return ConfirmDelete();">
                                                        @csrf
                                                        <input type="hidden" name="id" value="{{ $admin->id }}">
                                                        <input type="submit" value="{{ __('profile.delete') }}">
                                                    </form>
                                                </div>
                                            </td>
                                        @endif--}}
                                        <td>
                                            <a href="{{ route('admin.profile-lesson', ['id' => $lesson->id]) }}">
                                                {{ $lesson->title }}
                                            </a>
                                        </td>
                                        <td>
                                            {{ $lesson->course->title }}
                                        </td>
                                        <td>
                                            {{ $lesson->chapters_count }}
                                        </td>
                                        <td>
                                            <a class="btn btn-primary" onclick="return ConfirmDelete();"
                                                href="{{ route('admin.profile-lesson', ['id' => $lesson->id]) }}">
                                                {{ __('profile.details') }}
                                            </a>
                                            <a class="btn btn-danger" onclick="return ConfirmDelete();"
                                                href="{{ route('admin.delete-lesson', ['id' => $lesson->id]) }}">
                                                {{ __('profile.delete') }}
                                            </a>
                                            <a class="btn btn-info" onclick="return ConfirmDelete();"
                                                href="{{ route('admin.update-lesson-index', ['id' => $lesson->id]) }}">
                                                {{ __('profile.update') }}
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row d-flex justify-content-center mt-3">
                    {{ $lessons->links() }}
                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer')
    <!--   searching filters  -->
    <script src="{{ asset('js/filter-script.js') }}" type="text/javascript"></script>

    <script>
        document.getElementById("view_name").innerHTML = "{{ __('menu.lessons') }}";

    </script>
@endsection
