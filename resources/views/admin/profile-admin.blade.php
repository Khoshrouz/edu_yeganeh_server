@extends('layouts.rtl-admin')

@section('header')
    <link href="{{asset('css/bootstrap-toggle.min.css')}}" rel="stylesheet"/>
    <script src="{{asset('js/bootstrap-toggle.min.js')}}" type="text/javascript"></script>
@endsection

@section('content')

    @if(Auth::guard('admin')->user()->hasRole('super-admin'))
        <div class="row">
            <div class="col-md-6 pt-5">
                <div class="card card-profile">
                    <div class="card-avatar">
                        @if(isset($admin->image->link))
                            <a target="_blank" href="{{route('admin-image',['filename'=> $admin->image->link])}}">
                                <img class="img" src="{{route('admin-image',['filename'=> $admin->image->link])}}"/>
                            </a>
                        @else
                            <img class="img" src="{{asset('assets/img/faces/marc.jpg')}}"/>
                        @endif
                    </div>

                    <div class="card-body">
                        <div class="row text-right d-flex justify-content-center">
                            <table class="w-100">
                                <tbody>
                                @if(isset($admin->image->link))
                                    <tr>
                                        <td colspan="2" class="text-center">
                                            <a class="btn btn-danger"
                                            onclick="return ConfirmDelete();"
                                               href="{{route('admin.delete-image',['id'=>$admin->image->id])}}">
                                                {{__('profile.delete_image')}}
                                            </a>
                                        </td>
                                    <tr>
                                @else
                                    <tr>
                                        <td colspan="2" class="text-center">
                                            <form method="POST" action='{{ route('admin.create-admin-image') }}'
                                                  aria-label="{{ __('forms.create_admin') }}"
                                                  enctype="multipart/form-data"
                                                  onsubmit="return ConfirmDelete();">
                                                @csrf
                                                <input type="hidden" value="{{$admin->id}}" name="id">
                                                <div class="row mb-2">
                                                    <div class="col-md-12">
                                                        <label for="file">{{__('profile.select_file')}}</label>
                                                        <input type="file" id="file" name="file">
                                                        @error('file')
                                                        <strong class="text-danger">{{ $errors->first('file')  }}</strong>
                                                        @enderror
                                                    </div>
                                                    {{--end of profile details--}}
                                                </div>

                                                <button type="submit"
                                                        class="btn btn-primary pull-center">{{__('profile.submit')}}</button>
                                                <div class="clearfix"></div>
                                            </form>
                                        </td>
                                    <tr>
                                @endif
                                <tr>
                                    <td colspan="2" class="text-center">
                                        <hr>
                                        @if(Auth::guard('admin')->user()->hasRole('super-admin') && $admin->id != Auth::guard('admin')->user()->id)
                                            <input type="checkbox"
                                                   name="switch_admin_activity"
                                                   class="{{($admin->active == 1)? "true_activity" : "false_activity"}}"
                                                   data-toggle="toggle"
                                                   data-on="{{__('profile.activated')}}"
                                                   data-off="{{__('profile.deactivated')}}"
                                                   data-onstyle="success" data-offstyle="danger"
                                                   value="{{$admin->id}}">
                                        @else
                                            @if($admin->active)
                                                <span class="text-white btn btn-success">
                                                                {{__("profile.activated")}}
                                                            </span>
                                            @else
                                                <span class="text-white btn btn-danger">
                                                                {{__("profile.deactivated")}}
                                                            </span>
                                            @endif
                                        @endif

                                    </td>
                                <tr>
                                    @if(Auth::guard('admin')->user()->hasRole('super-admin') && $admin->id != Auth::guard('admin')->user()->id)
                                        <td colspan="2">
                                            <hr>
                                            <div class="stats text-center">
                                                <form method="POST"
                                                      action='{{ route('admin.delete-admin') }}'
                                                      onsubmit="return ConfirmDelete();">
                                                    @csrf
                                                    <input type="hidden" name="id" value="{{$admin->id}}">
                                                    <input type="submit" class="btn btn-warning"
                                                           value="{{__('profile.delete_question')}}">
                                                </form>
                                            </div>
                                        </td>
                                    @endif
                                </tr>

                                <tr>
                                    <td colspan="2">
                                        <hr>
                                        <h5 class="card-category m-0 text-center text-dark">{{$admin->name}}</h5>
                                    </td>
                                </tr>


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6 pt-5">
                <div class="card-body">
                    <div class="row text-right">
                        <div class="col-md-12 col-sm-12">
                            {{--personal info section--}}
                            <div class="card">
                                <div class="card-header card-header-primary">
                                    <h4 class="card-title">{{__('profile.edit')}}</h4>
                                </div>
                                <div class="card-body">
                                    <form method="POST" action='{{ route('admin.update-admin') }}'
                                          aria-label="{{ __('forms.update_admin') }}"
                                          onsubmit="return ConfirmDelete();">
                                        @csrf
                                        <input type="hidden" name="id" value="{{$admin->id}}">
                                        <div class="row mb-2">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">{{__('profile.first_name')}}</label>
                                                    <input id="name" type="text"
                                                           class="form-control @error('name') is-invalid @enderror"
                                                           name="name" value="{{$admin->name}}" required
                                                           autocomplete="name" autofocus>
                                                    @error('name')
                                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">{{__('profile.email')}}</label>
                                                    <input type="email" name="email" autofocus id="email"
                                                           value="{{$admin->email}}" required
                                                           class="form-control @error('email') is-invalid @enderror">
                                                    @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <button type="submit"
                                                class="btn btn-primary pull-right">{{__('profile.submit')}}</button>
                                        <div class="clearfix"></div>
                                    </form>
                                </div>
                            </div>
                            {{--end of personal info section--}}

                        </div>

                        <div class="col-md-12 col-sm-12">
                            {{--reset password section--}}
                            <div class="card">
                                <div class="card-header card-header-warning">
                                    <h4 class="card-title">{{__('profile.password_reset')}}</h4>
                                </div>
                                <div class="card-body">
                                    <form method="POST" action='{{ route('admin.update-admin-password') }}'
                                          aria-label="{{ __('forms.update_admin') }}"
                                          onsubmit="return ConfirmDelete();">
                                        @csrf
                                        <input type="hidden" name="id" value="{{$admin->id}}">
                                        <div class="row mb-2">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">{{__('profile.password_old')}}</label>
                                                    <input type="password" name="old_password" required
                                                           class="form-control @error('old_password') is-invalid @enderror">
                                                    @error('old_password')
                                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">{{__('profile.password_new')}}</label>
                                                    <div>
                                                        <input id="password" type="password"
                                                               class="form-control @error('password') is-invalid @enderror"
                                                               name="password" required autocomplete="new-password">

                                                        @error('password')
                                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">{{__('profile.password_confirm')}}</label>
                                                    <input id="password-confirm" type="password"
                                                           class="form-control"
                                                           name="password_confirmation" required
                                                           autocomplete="new-password">
                                                </div>
                                            </div>
                                        </div>

                                        <button type="submit"
                                                class="btn btn-primary pull-right">{{__('profile.password_submit')}}</button>
                                        <div class="clearfix"></div>
                                    </form>
                                </div>
                            </div>
                            {{--end of reset password section--}}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    @else
        <div class="row">
            <div class="col-md-4 pt-5">
                <div class="card card-profile">
                    <div class="card-avatar">
                        @if(isset($admin->image->link))
                            <a target="_blank"
                               href="{{route('admin-image',['filename'=> $admin->image->link])}}">
                                <img class="img"
                                     src="{{route('admin-image',['filename'=> $admin->image->link])}}"/>
                            </a>
                        @else
                            <img class="img" src="{{asset('assets/img/faces/marc.jpg')}}"/>
                        @endif
                    </div>

                    <div class="card-body">
                        <div class="row text-right d-flex justify-content-center">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td colspan="2" class="text-center">
                                            <a class="text-white btn btn-{{ ($admin->active) ? "success" : "danger" }}"
                                               @if(Auth::guard('admin')->user()->hasRole('super-admin') && $admin->id != Auth::guard('admin')->user()->id)
                                               href="{{route('admin.update-admin-activation',['id'=>$admin->id])}}"@endif>
                                                {{($admin->active) ? __('profile.activated') : __('profile.deactivated') }}
                                            </a>
                                        </td>
                                    <tr>
                                        @if(Auth::guard('admin')->user()->hasRole('super-admin') && $admin->id != Auth::guard('admin')->user()->id)
                                            <td colspan="2">
                                                <div class="stats text-center">
                                                    <form method="POST"
                                                          action='{{ route('admin.delete-admin') }}'
                                                          onsubmit="return ConfirmDelete();">
                                                        @csrf
                                                        <input type="hidden" name="id" value="{{$admin->id}}">
                                                        <input type="submit" class="btn btn-warning"
                                                               value="{{__('profile.delete_question')}}">
                                                    </form>
                                                </div>
                                            </td>
                                        @endif
                                    </tr>

                                    <tr>
                                        <td colspan="2">
                                            <h5 class="card-category text-center text-dark">{{$admin->name}}</h5>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan="2">
                                            <h5 class="card-category text-center text-dark">{{$admin->email}}</h5>
                                        </td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

@endsection

@section('footer')
    <script>
        document.getElementById("view_name").innerHTML = "{{__('menu.profile')}}";
    </script>
    <script src="{{asset('js/seoa-script.js')}}"></script>

@endsection