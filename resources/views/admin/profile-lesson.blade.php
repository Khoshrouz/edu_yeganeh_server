@extends('layouts.rtl-admin')

@section('header')
<link href="{{asset('css/bootstrap-toggle.min.css')}}" rel="stylesheet" />
<script src="{{asset('js/bootstrap-toggle.min.js')}}" type="text/javascript"></script>
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">

                <span class="card-title">{{__('profile.lesson')}}: {{$lesson->title}}</span>
                --
                <span class="card-title">{{__('profile.course')}}: {{$lesson->course->title}}</span>
                <hr>
                <span class="card-title">{{__('profile.created_at')}}
                    : {{\Morilog\Jalali\Jalalian::fromDateTime($lesson->created_at)->format('Y-m-d H:i')}}</span>
                --
                <span class="card-title">{{__('profile.updated_at')}}
                    : {{\Morilog\Jalali\Jalalian::fromDateTime($lesson->updated_at)->format('Y-m-d H:i')}}</span>
            </div>
            <div class="card-body">
                <a class="btn btn-primary" href="{{route('admin.update-lesson-index', ['id'=>$lesson->id])}}">{{__('profile.update')}}</a>
            </div>
        </div>
    </div>

    {{--chapter section--}}
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">{{__('profile.chapters')}}</h4>

                <div class="d-flex justify-content-between">
                    <div>
                        <!-- <a class="btn btn-info" href="{{route('admin.create-chapter-index',['lesson_id'=>$lesson->id])}}">
                            {{__('menu.add-chapter')}}
                        </a> -->
                    </div>

                    <button class="btn btn-white" data-toggle="collapse" href="#collapseChaptersBody">
                        <i class="material-icons">view_headline</i>
                    </button>
                </div>

            </div>
            <div class="collapse show" id="collapseChaptersBody">
                <div class="card-body">
                    <div class="table-responsive table-striped">
                        <table class="table table-hover overflow-auto text-nowrap text-right" id="searchTable">
                            <thead class="thead-dark">
                                <th>{{__('profile.title')}}</th>
                                <th>{{__('profile.price')}}</th>
                                <th>{{__('profile.downloads_count')}}</th>
                                <th>{{__('profile.questions_count')}}</th>
                                <th>{{__('profile.comprehensive_questions_count')}}</th>
                                <th>{{__('profile.actions')}}</th>
                            </thead>

                            <tbody>
                                @foreach($lesson->chapters as $chapter)
                                <tr>
                                    <td>
                                        <a target="_blank" href="{{route('admin.profile-chapter',['id'=>$chapter->id])}}">
                                            {{$chapter->title}}
                                        </a>
                                    </td>
                                    <td>
                                        @foreach($chapter->packages as $package)
                                        <span class="font-weight-bold">
                                            {{$package->duration}}{{" "}}{{__("profile.duration_months")}}
                                        </span>
                                        <p class="price_number m-auto text-primary">
                                            {{$package->price}}
                                        </p>
                                        @endforeach
                                    </td>
                                    <td>
                                        {{$chapter->purchases_count}}
                                    </td>
                                    <td>
                                        {{$chapter->questions_count}}
                                    </td>
                                    <td>
                                        {{$chapter->questions_count}}
                                    </td>
                                    <td>
                                        <a target="_blank" href="{{route('admin.profile-chapter',['id'=>$chapter->id])}}" class="btn btn-primary">
                                            {{__("profile.details")}}
                                        </a>
                                        <a target="_blank" href="{{route('admin.update-chapter-index',['id'=>$chapter->id])}}" class="btn btn-info">
                                            {{__("profile.update")}}
                                        </a>
                                        <a href="{{route('admin.delete-chapter',['id'=>$chapter->id])}}" class="btn btn-danger">
                                            {{__("profile.delete")}}
                                        </a>

                                    </td>
                                <tr>
                                    @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection

@section('footer')
<script>
    document.getElementById("view_name").innerHTML = "{{__('menu.lesson')}}";
</script>
<script src="{{asset('js/seoa-script.js')}}"></script>
<!-- Numbers JS -->
<script src="{{asset('js/numbers.js')}}"></script>

@endsection