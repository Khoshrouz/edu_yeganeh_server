@extends('layouts.rtl-admin')

@section('content')

    <div class="row">
        <div class="col-md-12">
            {{--personal info section--}}
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">{{__('profile.create')}}</h4>
                </div>
                <div class="card-body">
                    <form method="POST" action='{{ route('admin.create-user') }}'
                          aria-label="{{ __('forms.create_admin') }}"
                          onsubmit="return ConfirmDelete();">
                        @csrf
                        <div class="row mb-2">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">{{__('profile.first_name')}}</label>
                                    <input id="name" type="text"
                                           class="form-control @error('name') is-invalid @enderror"
                                           name="name" value="{{ old('name') }}" required
                                           autocomplete="name" autofocus>
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">{{__('profile.email')}}</label>
                                    <input type="email" name="email" autofocus id="email"
                                           value="{{ old('email') }}" required
                                           class="form-control @error('email') is-invalid @enderror">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            {{--profile details--}}
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating">{{__('profile.melliCode')}}</label>
                                    <input type="number" name="melliCode" autofocus id="melliCode"
                                           value="{{ old('melliCode') }}" required
                                           class="form-control @error('melliCode') is-invalid @enderror">
                                    @error('melliCode')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating">{{__('profile.phone')}}</label>
                                    <input type="number" name="phone" autofocus id="phone"
                                           value="{{ old('phone') }}" required
                                           class="form-control @error('phone') is-invalid @enderror">
                                    @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating">{{__('profile.university_entrance')}}</label>
                                    <input type="number" name="university_entrance" autofocus id="university_entrance"
                                           value="{{ old('university_entrance') }}" required
                                           class="form-control @error('university_entrance') is-invalid @enderror">
                                    @error('university_entrance')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            {{--end of profile details--}}
                        </div>

                        <button type="submit"
                                class="btn btn-primary pull-right">{{__('profile.submit')}}</button>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
            {{--end of personal info section--}}


        </div>
    </div>

@endsection

@section('footer')
    <script src="{{asset('js/seoa-script.js')}}"></script>

    <script>
        document.getElementById("view_name").innerHTML = "{{__('menu.create_user')}}";
    </script>
@endsection
