@extends('layouts.rtl-admin')

@section('header')
<link href="{{asset('css/bootstrap-toggle.min.css')}}" rel="stylesheet" />
<script src="{{asset('js/bootstrap-toggle.min.js')}}" type="text/javascript"></script>
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <span class="card-title">{{__('profile.chapter')}}: {{$chapter->title}}</span>
                --
                <span class="card-title">{{__('profile.lesson')}}: {{$chapter->lesson->title}}</span>
            </div>
            <div class="card-body">

                <h5 class="card-title">
                    <p class="text-dark text-justify">{{__('profile.description')}}
                        : {!! $chapter->description !!}</p>
                </h5>
                <hr />
                <span class="card-title">{{__('profile.created_at')}}
                    : {{\Morilog\Jalali\Jalalian::fromDateTime($chapter->created_at)->format('Y-m-d H:i')}}</span>
                --
                <span class="card-title">{{__('profile.updated_at')}}
                    : {{\Morilog\Jalali\Jalalian::fromDateTime($chapter->updated_at)->format('Y-m-d H:i')}}</span>
                <hr />
                <span class="card-title">{{__('profile.downloads_count')}}
                    : {{$chapter->purchases_count}}
                </span>
                <hr />
                <span class="card-title">{{__('profile.stars')}}
                    : {{$chapter->stars}}
                </span>
                <hr />
                <div class="row">
                    @foreach($chapter->packages as $package)
                    <div class="col">
                        <span class="font-weight-bold">
                            {{$package->duration}}{{" "}}{{__("profile.duration_months")}}
                        </span>
                        <p class="price_number m-auto text-primary">
                            {{$package->price}}
                        </p>
                    </div>
                    @endforeach
                </div>
                <hr>
                <a class="btn btn-primary" href="{{route('admin.update-chapter-index', ['id'=>$chapter->id])}}">{{__('profile.update')}}</a>

            </div>

            <div class="card-body">
                <h5 class="card-title">{{__('profile.attached_images')}}</h5>
                <hr>
                <div class="d-flex flex-nowrap">
                    @if($chapter->images->count())
                    @foreach($chapter->images as $image)
                    <div class="card-avatar">
                        <a target="_blank" href="{{route('chapter-image',['image'=>$image->link])}}" class="col-2 p-1">
                            <img class="img img-thumbnail" width="80" height="80" src="{{route('chapter-image' ,['image'=> $image->link])}}" />
                        </a>
                    </div>
                    @endforeach
                    @endif
                    @if($chapter->icon)
                    <div class="card-avatar">
                        <a target="_blank" href="{{route('chapter-image',['image'=>$chapter->icon])}}" class="col-2 p-1 ml-4">
                            <img class="img img-thumbnail" width="80" height="80" src="{{route('chapter-image' ,['image'=> $chapter->icon])}}" />
                        </a>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    {{--confirmed comment section--}}
    <div class="col-md-6">
        <div class="card">
            <div class="card-header card-header-primary">
                <div class="d-flex justify-content-between">
                    <h4 class="card-title">{{__('profile.confirmed_comments')}}</h4>
                    <button class="btn btn-white" data-toggle="collapse" href="#collapseConfirmedCommentsBody">
                        <i class="material-icons">view_headline</i>
                    </button>
                </div>
            </div>
            <div class="collapse show" id="collapseConfirmedCommentsBody">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover overflow-auto text-right" id="searchTable">
                            <thead class="thead-dark">
                                <th scope="col">#</th>
                                <th scope="col">{{__('profile.comment_content')}}</th>
                                <th scope="col">{{__('profile.user')}}</th>
                                <th scope="col">{{__('profile.stars')}}</th>
                                <th scope="col">{{__('profile.confirmed')}}</th>
                            </thead>

                            <tbody>

                                @foreach($confirmed_comments as $comment)
                                <tr>
                                    <td>
                                        {{$comment->id}}
                                    </td>
                                    <td>
                                        {{$comment->content}}
                                    </td>
                                    <td>
                                        <a href="{{route('admin.profile-user', ['id'=>$comment->user->id])}}">{{$comment->user->email}}</a>
                                    </td>
                                    <td>
                                        {{$comment->stars}}
                                    </td>
                                    <td>
                                        <a class="btn {{($comment->confirm)? "btn-success":"btn-danger"}}" href="{{route('admin.comment-confirm',['id'=>$comment->id])}}">
                                            {{($comment->confirm)?__('profile.confirmed'):__('profile.not_confirmed')}}
                                        </a>
                                    </td>
                                <tr>
                                    @endforeach
                            </tbody>
                        </table>
                        <div class="d-flex justify-content-center border-top pt-2">
                            {{$confirmed_comments->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    {{--not confirmed comment section--}}
    <div class="col-md-6">
        <div class="card">
            <div class="card-header card-header-primary">
                <div class="d-flex justify-content-between">
                    <h4 class="card-title">{{__('profile.not_confirmed_comments')}}</h4>
                    <button class="btn btn-white" data-toggle="collapse" href="#collapseNotConfirmedCommentsBody">
                        <i class="material-icons">view_headline</i>
                    </button>
                </div>
            </div>
            <div class="collapse show" id="collapseNotConfirmedCommentsBody">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover overflow-auto text-right" id="searchTable">
                            <thead class="thead-dark">
                                <th scope="col">#</th>
                                <th scope="col">{{__('profile.comment_content')}}</th>
                                <th scope="col">{{__('profile.user')}}</th>
                                <th scope="col">{{__('profile.stars')}}</th>
                                <th scope="col">{{__('profile.confirmed')}}</th>
                            </thead>

                            <tbody>

                                @foreach($not_confirmed_comments as $comment)
                                <tr>
                                    <td>
                                        {{$comment->id}}
                                    </td>
                                    <td>
                                        {{$comment->content}}
                                    </td>
                                    <td>
                                        <a href="{{route('admin.profile-user', ['id'=>$comment->user->id])}}">{{$comment->user->email}}</a>
                                    </td>
                                    <td>
                                        {{$comment->stars}}
                                    </td>
                                    <td>
                                        <a class="btn {{($comment->confirm)? "btn-success":"btn-danger"}}" href="{{route('admin.comment-confirm',['id'=>$comment->id])}}">
                                            {{($comment->confirm)?__('profile.confirmed'):__('profile.not_confirmed')}}
                                        </a>
                                    </td>
                                <tr>
                                    @endforeach
                            </tbody>
                        </table>
                        <div class="d-flex justify-content-center border-top pt-2">
                            {{$not_confirmed_comments->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--chapter section--}}
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">{{__('profile.parts')}}</h4>
                <div class="d-flex justify-content-between">
                    <div>
                        <!-- <a class="btn btn-info" href="{{route('admin.create-part-index',['chapter_id'=>$chapter->id])}}">
                            {{__('menu.add-part')}}
                        </a> -->
                    </div>

                    <button class="btn btn-white" data-toggle="collapse" href="#collapseChaptersBody">
                        <i class="material-icons">view_headline</i>
                    </button>
                </div>
            </div>
            <div class="collapse show" id="collapseChaptersBody">
                <div class="card-body">
                    <div class="table-responsive table-striped">
                        <table class="table table-hover overflow-auto text-nowrap text-right" id="searchTable">
                            <thead class="thead-dark">
                                <th>{{__('profile.title')}}</th>
                                <th>{{__('profile.questions_count')}}</th>
                                <th>{{__('profile.actions')}}</th>
                            </thead>

                            <tbody>
                                @foreach($chapter->parts as $part)
                                <tr>
                                    <td>
                                        <a target="_blank" href="{{route('admin.profile-part',['id'=>$part->id])}}">
                                            {{$part->title}}
                                        </a>
                                    </td>
                                    <td>
                                        {{$part->questions_count}}
                                    </td>
                                    <td>
                                        <a target="_blank" href="{{route('admin.profile-part',['id'=>$part->id])}}" class="btn btn-primary">
                                            {{__("profile.details")}}
                                        </a>
                                        <a target="_blank" href="{{route('admin.update-part-index',['id'=>$part->id])}}" class="btn btn-info">
                                            {{__("profile.update")}}
                                        </a>
                                        <a href="{{route('admin.delete-part',['id'=>$part->id])}}" onclick="return ConfirmDelete();" class="btn btn-danger">
                                            {{__("profile.delete")}}
                                        </a>

                                    </td>
                                <tr>
                                    @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection

@section('footer')
<script>
    document.getElementById("view_name").innerHTML = "{{__('menu.chapter')}}";
</script>
<script src="{{asset('js/seoa-script.js')}}"></script>
<!-- Numbers JS -->
<script src="{{asset('js/numbers.js')}}"></script>

@endsection