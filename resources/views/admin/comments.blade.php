@extends('layouts.rtl-admin')

@section('content')

{{--main--}}
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title ">{{__('profile.reports')}}</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive table-striped">
                    <table class="table table-hover overflow-auto text-right" id="searchTable">
                        <thead class=" text-primary">
                            <th scope="col">#</th>
                            <th scope="col">{{__('profile.chapter')}}</th>
                            <th scope="col">{{__('profile.comment_content')}}</th>
                            <th scope="col">{{__('profile.user')}}</th>
                            <th scope="col">{{__('profile.stars')}} {{__('profile.comment')}}</th>
                            <th scope="col">{{__('profile.stars')}} {{__('profile.chapter')}}</th>
                            <th scope="col">{{__('profile.confirmed')}}</th>
                        </thead>
                        <tbody>
                            @foreach($comments as $comment)
                            <tr>
                                <td>
                                    {{$comment->id}}
                                </td>
                                <td>
                                    <a href="{{route('admin.profile-chapter',['id'=>$comment->commentable->id])}}">
                                        {{$comment->commentable->title}}
                                    </a>
                                </td>
                                <td>
                                    {{$comment->content}}
                                </td>
                                <td>
                                    <a href="{{route('admin.profile-user', ['id'=>$comment->user->id])}}">{{$comment->user->email}}</a>
                                </td>
                                <td>
                                    {{$comment->stars}}
                                </td>
                                <td>
                                    {{$comment->commentable->stars}}
                                </td>
                                <td>
                                    <a class="btn {{($comment->confirm)? "btn-success":"btn-danger"}}" href="{{route('admin.comment-confirm',['id'=>$comment->id])}}">
                                        {{($comment->confirm)?__('profile.confirmed'):__('profile.not_confirmed')}}
                                    </a>

                                    <a class="btn btn-warning" href="{{route('admin.comment-delete',['id'=>$comment->id])}}">
                                        {{__('profile.delete')}}
                                    </a>
                                </td>
                            <tr>
                                @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row d-flex justify-content-center mt-3">
                {{ $comments->links() }}
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
<script src="{{asset('js/seoa-script-search.js')}}"></script>
<!--   searching filters  -->
<script src="{{asset('js/filter-script.js')}}" type="text/javascript"></script>

<script>
    document.getElementById("view_name").innerHTML = "{{__('menu.reports')}}";
</script>
@endsection