@extends('layouts.rtl-admin')

@section('content')

    <div class="row">
        <div class="col-md-12">
            {{--personal info section--}}
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">{{ __('profile.update') }}</h4>
                </div>
                <div class="card-body">
                    <form method="POST" action='{{ route('admin.update-question') }}'
                        aria-label="{{ __('forms.create_admin') }}" enctype="multipart/form-data"
                        onsubmit="return ConfirmDelete();">
                        @csrf
                        <input type="hidden" name="id" value="{{ $question->id }}">
                        <div class="row mb-2 p-4">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">{{ __('profile.question_content') }}</label>

                                    <input type="text" name="question_content" value="{{ $question->question_content }}"
                                        class="form-control @error('question_content') is-invalid @enderror">

                                    @error('question_content')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('question_content') }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">{{ __('profile.answer_content') }}</label>

                                    <input type="text" name="answer_content" value="{{ $question->answer_content }}"
                                        class="form-control @error('answer_content') is-invalid @enderror">

                                    @error('answer_content')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('answer_content') }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>


                            <div class="col-md-12 mt-4">
                                <h5>{{ __('profile.options') }}</h5>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">{{ __('profile.first_option') }}</label>

                                    <input type="text" name="first_option" value="{{ $question->first_option }}"
                                        class="form-control @error('first_option') is-invalid @enderror">

                                    @error('first_option')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('first_option') }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">{{ __('profile.second_option') }}</label>

                                    <input type="text" name="second_option" value="{{ $question->second_option }}"
                                        class="form-control @error('second_option') is-invalid @enderror">

                                    @error('second_option')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('second_option') }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">{{ __('profile.third_option') }}</label>

                                    <input type="text" name="third_option" value="{{ $question->third_option }}"
                                        class="form-control @error('third_option') is-invalid @enderror">

                                    @error('third_option')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('third_option') }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">{{ __('profile.fourth_option') }}</label>

                                    <input type="text" name="fourth_option" value="{{ $question->fourth_option }}"
                                        class="form-control @error('fourth_option') is-invalid @enderror">

                                    @error('fourth_option')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('fourth_option') }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>


                            <div class="col-md-12 p-4">
                                <label class="bmd-label-floating">{{ __('profile.answer') }}</label>

                                <div class="form-group p-4">

                                    <div class="radio">
                                        <label><input value="1" type="radio" name="answer"
                                                {{ $question->answer == 1 ? 'checked' : '' }}> 1-
                                            {{ $question->first_option }}</label>
                                    </div>

                                    <div class="radio">
                                        <label><input value="2" type="radio" name="answer"
                                                {{ $question->answer == 2 ? 'checked' : '' }}> 2-
                                            {{ $question->second_option }}</label>
                                    </div>

                                    <div class="radio">
                                        <label><input value="3" type="radio" name="answer"
                                                {{ $question->answer == 3 ? 'checked' : '' }}> 3-
                                            {{ $question->third_option }}</label>
                                    </div>

                                    <div class="radio">
                                        <label><input value="4" type="radio" name="answer"
                                                {{ $question->answer == 4 ? 'checked' : '' }}> 4-
                                            {{ $question->fourth_option }}</label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 p-3">
                                <label for="file" class="d-block">{{ __('profile.question_image') }}</label>
                                <input type="file" id="file" name="question_image">
                                @error('file')
                                    <strong class="text-danger">{{ $errors->first('file') }}</strong>
                                @enderror
                            </div>


                            <div class="col-md-6 p-3">
                                <label for="file" class="d-block">{{ __('profile.answer_image') }}</label>
                                <input type="file" id="file" name="answer_image">
                                @error('file')
                                    <strong class="text-danger">{{ $errors->first('file') }}</strong>
                                @enderror
                            </div>




                            {{--end of profile details--}}
                        </div>

                        <button type="submit" class="btn btn-primary pull-right">{{ __('profile.submit') }}</button>
                        <div class="clearfix"></div>
                    </form>
                </div>

                <hr>

                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                        <div class="card-body">
                            <h5 class="card-title">{{ __('profile.question_image') }}</h5>
                            <hr>
                            @if (!is_null($question->question_image))
                                <div class="d-flex flex-nowrap">
                                    <div class="card-deck col-12 p-2">
                                        <div class="card">
                                            <a target="_blank"
                                                href="{{ route('question-image', ['image' => $question->question_image]) }}"
                                                class="p-1 m-auto">
                                                <img class="img img-thumbnail" alt="" width="100px" height="100px"
                                                    src="{{ route('question-image', ['image' => $question->question_image]) }}" />
                                            </a>

                                            <a href="{{ route('admin.delete-question-image', ['id' => $question->id]) }}"
                                                onclick="return ConfirmDelete();" class="btn btn-danger">
                                                {{ __('profile.delete') }}
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                        <div class="card-body">
                            <h5 class="card-title">{{ __('profile.answer_image') }}</h5>
                            <hr>
                            @if (!is_null($question->answer_image))
                                <div class="d-flex flex-nowrap">
                                    <div class="card-deck col-12 p-2">
                                        <div class="card">
                                            <a target="_blank"
                                                href="{{ route('question-image', ['image' => $question->answer_image]) }}"
                                                class="p-1 m-auto">
                                                <img class="img img-thumbnail" alt="" width="100px" height="100px"
                                                    src="{{ route('question-image', ['image' => $question->answer_image]) }}" />
                                            </a>

                                            <a href="{{ route('admin.delete-question-answer-image', ['id' => $question->id]) }}"
                                                onclick="return ConfirmDelete();" class="btn btn-danger">
                                                {{ __('profile.delete') }}
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>

            </div>
            {{--end of personal info section--}}


        </div>
    </div>

@endsection

@section('footer')

    <script>
        document.getElementById("view_name").innerHTML = "{{ __('menu.create_news') }}";

    </script>

@endsection
