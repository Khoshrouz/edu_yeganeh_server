@extends('layouts.rtl-admin')

@section('header')
<link href="{{asset('css/bootstrap-toggle.min.css')}}" rel="stylesheet" />
<script src="{{asset('js/bootstrap-toggle.min.js')}}" type="text/javascript"></script>
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <span class="card-title">{{__('profile.part')}}: {{$part->title}}</span>
                --
                <span class="card-title">{{__('profile.chapter')}}: {{$part->chapter->title}}</span>
                --
                <span class="card-title">{{__('profile.lesson')}}: {{$part->chapter->lesson->title}}</span>
            </div>
            <div class="card-body">
                <span class="card-title">{{__('profile.created_at')}}
                    : {{\Morilog\Jalali\Jalalian::fromDateTime($part->created_at)->format('Y-m-d H:i')}}</span>
                --
                <span class="card-title">{{__('profile.updated_at')}}
                    : {{\Morilog\Jalali\Jalalian::fromDateTime($part->updated_at)->format('Y-m-d H:i')}}</span>

                <hr>

                <a class="btn btn-primary" href="{{route('admin.update-part-index', ['id'=>$part->id])}}">{{__('profile.update')}}</a>
            </div>
        </div>
    </div>

    {{--Questions section--}}
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">{{__('profile.questions')}}</h4>
                <div class="d-flex justify-content-between">
                    <div>
                        <!-- <a class="btn btn-info" href="{{route('admin.create-question-index',['part_id'=>$part->id])}}">
                            {{__('menu.add-question')}}
                        </a> -->
                    </div>
                    <button class="btn btn-white" data-toggle="collapse" href="#collapseQuestionsBody">
                        <i class="material-icons">view_headline</i>
                    </button>
                </div>
            </div>

            <div class="collapse show" id="collapseQuestionsBody">
                <div class="card-body">
                    <div class="table-responsive table-striped">
                        <table class="table table-hover overflow-auto text-right" id="searchTable">
                            <thead class="thead-dark">
                                <th>
                                    {{__('profile.actions')}}
                                </th>
                                <th>
                                    {{__('profile.image')}}
                                </th>
                                <th>
                                    {{__('profile.question_content')}}
                                </th>
                                <th>
                                    {{__('profile.first_option')}}
                                </th>
                                <th>
                                    {{__('profile.second_option')}}
                                </th>
                                <th>
                                    {{__('profile.third_option')}}
                                </th>
                                <th>
                                    {{__('profile.fourth_option')}}
                                </th>
                                <th>
                                    {{__('profile.answer')}}
                                </th>
                                <th>
                                    {{__('profile.answer_content')}}
                                </th>
                                <th>
                                    {{__('profile.answer_image')}}
                                </th>
                            </thead>

                            @foreach($part->questions as $question)
                            <tr>
                                <td>
                                    <div>
                                        <a target="_blank" href="{{route('admin.update-question-index',['id'=>$question->id])}}" class="w-100 d-block btn btn-info">
                                            {{__("profile.update")}}
                                        </a>
                                        <a href="{{route('admin.delete-question',['id'=>$question->id])}}" 
                                            onclick="return ConfirmDelete();" class="w-100 btn btn-danger">
                                            {{__("profile.delete")}}
                                        </a>
                                    </div>
                                </td>
                                <td class="max-width-td">
                                    @if(empty($question->question_image))
                                    <img class="img img-thumbnail" width="60" height="60" src="{{asset('images/photo.svg')}}" />
                                    @else
                                    <a target="_blank" href="{{route('question-image' ,['image'=> $question->question_image])}}">
                                        <img class="img img-thumbnail" width="60" height="60" alt="{{$question->question_image}}" src="{{route('question-image' ,['image'=> $question->question_image])}}" />
                                    </a>
                                    @endif
                                </td>
                                <td class="max-width-td">
                                    {!! substr($question->question_content , 0 , 100) !!}
                                </td>
                                <td class="max-width-td">
                                    {{$question->first_option}}
                                </td>
                                <td class="max-width-td">
                                    {{$question->second_option}}
                                </td>
                                <td class="max-width-td">
                                    {{$question->third_option}}
                                </td>
                                <td class="max-width-td">
                                    {{$question->fourth_option}}
                                </td>
                                <td>
                                    {{$question->answer}}
                                </td>
                                <td class="max-width-td">
                                    {!! substr($question->answer_content , 0 , 100) !!}
                                </td>
                                <td class="max-width-td">
                                    @if(empty($question->answer_image))
                                    <img class="img img-thumbnail" width="60" height="60" src="{{asset('images/photo.svg')}}" />
                                    @else
                                    <a target="_blank" href="{{route('question-image' ,['image'=> $question->answer_image])}}">
                                        <img class="img img-thumbnail" width="60" height="60" alt="{{$question->answer_image}}" src="{{route('question-image' ,['image'=> $question->answer_image])}}" />
                                    </a>
                                    @endif
                                </td>
                            <tr>
                                @endforeach

                        </table>
                        <div class="d-flex justify-content-center border-top pt-2">
                            {{$questions->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    {{--reports section--}}
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <div class="d-flex justify-content-between">
                    <h4 class="card-title">{{__('profile.reports')}}</h4>
                    <button class="btn btn-white" data-toggle="collapse" href="#collapseReportsBody">
                        <i class="material-icons">view_headline</i>
                    </button>
                </div>

            </div>

            <div class="collapse hide" id="collapseReportsBody">
                <div class="card-body">
                    <div class="table-responsive table-striped">
                        <table class="table table-hover overflow-auto text-right" id="searchTable">
                            <thead class="thead-dark">
                                <th>
                                    {{__('profile.report_content')}}
                                </th>
                                <th>
                                    {{__('profile.report_option')}}
                                </th>
                                <th>
                                    {{__('profile.user')}}
                                </th>
                                <th>
                                    {{__('profile.confirmed')}}
                                </th>

                            </thead>

                            @foreach($reports as $report)
                            <tr>
                                <td class="max-width-td">
                                    {{$report->content}}
                                </td>
                                <td>
                                    {{$report->report_option->title}}
                                </td>
                                <td>
                                    <a href="{{route('admin.profile-user', ['id'=>$report->user->id])}}">{{$report->user->email}}</a>
                                </td>
                                <td>
                                    <a class="btn {{($report->confirm)? "btn-success":"btn-danger"}}" href="{{route('admin.report-confirm',['id'=>$report->id])}}">
                                        {{($report->confirm)?__('profile.confirmed'):__('profile.not_confirmed')}}
                                    </a>
                                </td>
                            <tr>
                                @endforeach

                        </table>

                        <div class="d-flex justify-content-center border-top pt-2">
                            {{$reports->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection

@section('footer')
<script>
    document.getElementById("view_name").innerHTML = "{{__('menu.part')}}";
</script>
<script src="{{asset('js/seoa-script.js')}}"></script>
<!-- Numbers JS -->
<script src="{{asset('js/numbers.js')}}"></script>

@endsection