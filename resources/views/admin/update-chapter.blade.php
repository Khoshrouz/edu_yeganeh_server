@extends('layouts.rtl-admin')

@section('content')

    <div class="row">
        <div class="col-md-12">
            {{--personal info section--}}
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">{{ __('profile.update') }}</h4>
                </div>
                <div class="card-body">
                    <form method="POST" action='{{ route('admin.update-chapter') }}'
                        aria-label="{{ __('forms.create_admin') }}" enctype="multipart/form-data"
                        onsubmit="return ConfirmDelete();">
                        @csrf
                        <input type="hidden" name="id" value="{{ $chapter->id }}">
                        <div class="row mb-2">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="bmd-label-floating">{{ __('profile.lesson') }}</label>

                                    <select class="custom-select mr-sm-2" name="lesson_id"
                                        class="form-control @error('course') is-invalid @enderror">
                                        @foreach ($lessons as $lesson)
                                            <option {{ $lesson->id == $chapter->lesson->id ? 'selected' : '' }}
                                                value="{{ $lesson->id }}">{{ $lesson->title }}</option>
                                        @endforeach
                                    </select>

                                    @error('lesson')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror

                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="bmd-label-floating">{{ __('profile.title') }}</label>
                                    <input id="title" type="text" class="form-control @error('title') is-invalid @enderror"
                                        name="title" required value="{{ $chapter->title }}" autocomplete="title" autofocus>
                                    @error('title')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="bmd-label-floating">{{ __('profile.description') }}</label>

                                    <textarea name="description" id="description" class="summernote">
                                    {{ $chapter->description }}
                                    </textarea>

                                    <input type="hidden" class="form-control @error('description') is-invalid @enderror">

                                    @error('description')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('description') }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6 p-3">
                                <label for="file" class="d-block">{{ __('profile.select_file_icon') }}</label>
                                <input type="file" id="file" name="file">
                                @error('file')
                                    <strong class="text-danger">{{ $errors->first('file') }}</strong>
                                @enderror
                            </div>

                            <div class="col-md-6 p-3">
                                <label for="files" class="d-block">{{ __('profile.select_files_screenshots') }}</label>
                                <input type="file" id="files" name="files[]" multiple>
                                @error('files.*')
                                    <strong class="text-danger">{{ $errors->first('files.*') }}</strong>
                                @enderror
                            </div>


                            {{--end of profile details--}}
                        </div>

                        <button type="submit" class="btn btn-primary pull-right">{{ __('profile.submit') }}</button>
                        <div class="clearfix"></div>
                    </form>
                </div>

                <hr>

                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                        <div class="card-body">
                            <h5 class="card-title">{{ __('profile.icon') }}</h5>
                            <hr>
                            @if (!is_null($chapter->icon))
                                <div class="d-flex flex-nowrap">
                                    <div class="card-deck col-12 p-2">
                                        <div class="card">
                                            <a target="_blank"
                                                href="{{ route('chapter-image', ['image' => $chapter->icon]) }}"
                                                class="p-1">
                                                <img class="img img-thumbnail" width="100%" height="100%"
                                                    src="{{ route('chapter-image', ['image' => $chapter->icon]) }}" />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12">
                        <div class="card-body">
                            <h5 class="card-title">{{ __('profile.images') }}</h5>
                            <hr>
                            <div class="d-flex flex-nowrap">
                                @foreach ($chapter->images as $image)

                                    <div class="card-deck col-3 p-2">
                                        <div class="card">
                                            <a target="_blank"
                                                href="{{ route('chapter-image', ['image' => $image->link]) }}" class="p-1">
                                                <img class="img img-thumbnail" width="100%" height="100%"
                                                    src="{{ route('chapter-image', ['image' => $image->link]) }}" />
                                            </a>
                                            <div class="card-footer justify-content-center">
                                                <a class="btn btn-danger" onclick="return ConfirmDelete();"
                                                    href="{{ route('chapter-delete-image', ['id' => $image->id]) }}">
                                                    {{ __('profile.delete') }}
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            {{--end of personal info section--}}


        </div>
    </div>

@endsection

@section('footer')

    <script>
        document.getElementById("view_name").innerHTML = "{{ __('menu.create_news') }}";

    </script>

@endsection
