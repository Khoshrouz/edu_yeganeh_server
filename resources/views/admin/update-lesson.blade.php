@extends('layouts.rtl-admin')

@section('content')

<div class="row">
    <div class="col-md-12">
        {{--personal info section--}}
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">{{__('profile.create')}}</h4>
            </div>
            <div class="card-body">
                <form method="POST" action='{{ route('admin.update-lesson') }}' aria-label="{{ __('forms.create_admin') }}" enctype="multipart/form-data" onsubmit="return ConfirmDelete();">
                    @csrf
                    <input type="hidden" name="id" value="{{$lesson->id}}">
                    <div class="row mb-2">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="bmd-label-floating">{{__('profile.lesson')}}</label>

                                <select class="custom-select mr-sm-2" name="course_id" class="form-control @error('lesson') is-invalid @enderror">
                                    @foreach($courses as $course)
                                    <option {{($course->id == $lesson->course_id)? 'selected':''}} value="{{$course->id}}">{{$course->title}}</option>
                                    @endforeach
                                </select>

                                @error('course')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror

                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="bmd-label-floating">{{__('profile.title')}}</label>
                                <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" required value="{{$lesson->title}}" autocomplete="title" autofocus>
                                @error('title')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12 p-3">
                            <label for="file" class="d-block">{{__('profile.select_file_icon')}}</label>
                            <input type="file" id="file" name="file">
                            @error('file')
                            <strong class="text-danger">{{ $errors->first('file')  }}</strong>
                            @enderror
                        </div>

                        {{--end of profile details--}}
                    </div>

                    <button type="submit" class="btn btn-primary pull-right">{{__('profile.submit')}}</button>
                    <div class="clearfix"></div>
                </form>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="card-body">
                        <h5 class="card-title">{{__('profile.icon')}}</h5>
                        <hr>
                        @if(!empty($lesson->image))
                        <a target="_blank" href="{{route('lesson-image',['image'=>$lesson->image->link])}}" class="p-1">
                            <img class="img img-thumbnail" width="100px" height="100px" src="{{route('lesson-image' ,['image'=> $lesson->image->link])}}" />
                        </a>
                        @endif
                    </div>
                </div>
            </div>

        </div>
        {{--end of personal info section--}}


    </div>
</div>

@endsection

@section('footer')

<script>
    document.getElementById("view_name").innerHTML = "{{__('menu.create_news')}}";
</script>

@endsection