@extends('layouts.rtl-admin')

@section('content')
    {{--search box--}}
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header card-header-rose">
                    <h4 class="card-title ">{{__('filters.search_in_db')}}</h4>
                </div>

                <div class="card-body">
                    <form method="POST" action='{{ route('admin.search-users') }}'
                          aria-label="{{ __('forms.create_admin') }}" onsubmit="return ConfirmDelete();">
                        @csrf
                        <div class="row mb-2">
                            <div class="col-6 col-md-3 col-sm-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating">{{__('profile.first_name')}}</label>
                                    <input id="name" type="text"
                                           class="form-control @error('name') is-invalid @enderror"
                                           name="name" value="{{ old('name') }}"
                                           autocomplete="name" autofocus>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-6 col-md-3 col-sm-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating">{{__('profile.email')}}</label>
                                    <input type="text" name="email" autofocus id="email" value="{{ old('email') }}"
                                           class="form-control @error('email') is-invalid @enderror">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group col-6 col-md-3 col-sm-4">
                                <select id="inputState" class="form-control" name="activation_status">
                                    <option value="" selected>{{__('filters.activation_status')}}</option>
                                    <option value="1">{{__('filters.activated')}}</option>
                                    <option value="0">{{__('filters.deactivated')}}</option>
                                </select>
                            </div>

                            {{--profile details--}}
                            <div class="col-6 col-md-3 col-sm-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating">{{__('profile.melliCode')}}</label>
                                    <input type="number" name="melliCode" autofocus id="melliCode"
                                           value="{{ old('melliCode') }}"
                                           class="form-control @error('melliCode') is-invalid @enderror">
                                    @error('melliCode')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>


                        </div>

                        <button type="submit"
                                class="btn btn-primary pull-right col-12">{{__('filters.search')}}</button>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>

        </div>
    </div>
    {{--main--}}
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title ">{{__('profile.users')}}</h4>
                    <p class="card-category"> {{__('profile.users_description')}}</p>
                </div>
                <div class="card-body">
                    <div class="row p-4">
                        <div class="col-md-4 col-sm-6">
                            <input type="text" class=" searchText" id="searchNamesInput"
                                   onkeyup="searchTableFunc('searchNamesInput',1)"
                                   placeholder="{{__('filters.name')}}">

                        </div>
                        <div class="col-md-4 col-sm-6">
                            <input type="text" class="searchText" id="searchMelliCodesInput"
                                   onkeyup="searchTableFunc('searchMelliCodesInput',4)"
                                   placeholder="{{__('filters.melliCode')}}">
                        </div>

                        <div class="col-md-4 col-sm-6">
                            <input type="text" class="searchText" id="searchPhonesInput"
                                   onkeyup="searchTableFunc('searchPhonesInput',3)"
                                   placeholder="{{__('filters.phone')}}">
                        </div>

                        <div class="col-md-4 col-sm-6">
                            <input type="text" class="searchText" id="searchEmailsInput"
                                   onkeyup="searchTableFunc('searchEmailsInput',5)"
                                   placeholder="{{__('filters.email')}}">
                        </div>


                        <div class="col-md-4 col-sm-6">
                            <!-- Control buttons -->
                            <div id="myBtnContainer">
                                <button class="btn"
                                        onclick="filterSelection('')"> {{__('filters.both')}}</button>
                                <button class="btn"
                                        onclick="filterSelection('{{__('filters.activated')}}')"> {{__('filters.activated')}}</button>
                                <button class="btn"
                                        onclick="filterSelection('{{__('filters.deactivated')}}')"> {{__('filters.deactivated')}}</button>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive table-striped">
                        <table class="table overflow-auto text-nowrap" id="searchTable">
                            <thead class=" text-primary">
                            <th>
                                #
                            </th>
                            <th>
                                {{__('profile.first_name')}}
                                {{__('profile.last_name')}}
                            </th>
                            <th>
                                {{__('profile.activation_status')}}
                            </th>

                            <th>
                                {{__('profile.phone')}}
                            </th>

                            <th>
                                {{__('profile.melliCode')}}
                            </th>

                            <th>
                                {{__('profile.email')}}
                            </th>
                            <th>
                                {{__('profile.created_at')}}
                            </th>
                            <th>
                                {{__('profile.actions')}}
                            </th>


                            </thead>
                            <tbody>
                            <?php
                            $counter = 0;
                            ?>
                            @foreach($users as $user)
                                <tr>
                                    <td class="text-primary">
                                        {{++$counter}}
                                    </td>
                                    <td>
                                        <a href="{{route('admin.profile-user', ['id'=>$user->id])}}">{{$user->name}}</a>
                                    </td>
                                    @if($user->active)
                                        <td class="text-success">
                                            {{__("filters.activated")}}
                                        </td>
                                    @else
                                        <td class="text-danger">
                                            {{__("filters.deactivated")}}
                                        </td>
                                    @endif

                                    <td>
                                        {{$user->phone}}
                                    </td>
                                    <td>
                                        {{$user->melliCode}}
                                    </td>
                                    <td>
                                        {{$user->email}}
                                    </td>
                                    <td>
                                        {{\Morilog\Jalali\Jalalian::fromDateTime($user->created_at)}}
                                    </td>
                                    <td>
                                        <a class="btn btn-info"
                                           href="{{route('admin.profile-user', ['id'=>$user->id])}}">{{__('profile.details')}}</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row d-flex justify-content-center mt-3">
                    {{ $users->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script src="{{asset('js/seoa-script-search.js')}}"></script>
    <!--   searching filters  -->
    <script src="{{asset('js/filter-script.js')}}" type="text/javascript"></script>

    <script>
        document.getElementById("view_name").innerHTML = "{{__('menu.users')}}";
    </script>
@endsection