@extends('layouts.rtl-admin')

@section('content')

    <div class="row">
        <div class="col-md-12">
            {{--personal info section--}}
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">{{__('profile.create')}}</h4>
                </div>
                <div class="card-body">
                    <form method="POST" action='{{ route('admin.create-admin') }}'
                          aria-label="{{ __('forms.create_admin') }}" onsubmit="return ConfirmDelete();">
                        @csrf
                        <div class="row mb-2">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">{{__('profile.first_name')}}</label>
                                    <input id="name" type="text"
                                           class="form-control @error('name') is-invalid @enderror"
                                           name="name" value="{{ old('name') }}"
                                           autocomplete="name" autofocus>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">{{__('profile.email')}}</label>
                                    <input type="email" name="email" autofocus  id="email" value="{{ old('email') }}"
                                           class="form-control @error('email') is-invalid @enderror">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">{{__('profile.password_new')}}</label>
                                    <input type="password" name="password" value="{{ old('password') }}"
                                           autofocus class="form-control @error('password') is-invalid @enderror">
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">{{__('profile.password_confirm')}}</label>
                                    <input type="password" name="password_confirmation" class="form-control">
                                </div>
                            </div>
                        </div>

                        <button type="submit"
                                class="btn btn-primary pull-right">{{__('profile.submit')}}</button>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
            {{--end of personal info section--}}
        </div>
    </div>

@endsection

@section('footer')

    <script>
        document.getElementById("view_name").innerHTML = "{{__('menu.create_admin')}}";
    </script>
@endsection

