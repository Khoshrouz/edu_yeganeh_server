@extends('layouts.auth')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard (User)</div>

                    <div class="card-body">
                        {{ __('messages.welcome')}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection