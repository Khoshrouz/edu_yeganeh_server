<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="rtl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!--     Fonts and icons     -->
    <link href="{{ asset('css/material-icons.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">

    <!-- CSS Files -->
    <link href="{{asset('assets/css/material-dashboard.css?v=2.1.0')}}" rel="stylesheet"/>
    <link href="{{asset('assets/css/material-dashboard-rtl.css?v=1.1')}}" rel="stylesheet"/>
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="{{asset('assets/demo/demo.css')}}" rel="stylesheet"/>

    <!-- include summernote css/js-->
    <link href="{{ asset('css/summernote.css') }}" rel="stylesheet">
    <!--   Core JS Files   -->
    <script src="{{asset('assets/js/core/jquery.min.js')}}" type="text/javascript"></script>

    <!--   Persian font and style   -->
    <link href="{{ asset('css/font-face.css') }}" rel="stylesheet">
    <!--   searching filters styles  -->
    <link href="{{ asset('css/filter-style.css') }}" rel="stylesheet">

    @yield('header')
</head>

<body>
<div class="wrapper ">
    <div class="sidebar" data-color="danger" data-background-color="white"
         data-image="{{asset('assets/img/sidebar-1.jpg')}}">
        <!--
          Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

          Tip 2: you can also add an image using data-image tag
      -->
        <div class="logo">
            <a href="#" class="simple-text logo-normal">
                {{ config('app.name', 'Laravel') }}
            </a>
        </div>
        <div class="sidebar-wrapper">
            <ul class="nav" id="menu-ul">

                <li class="nav-itemdropdown">

                    <a id="navbarDropdown nav-link" class="nav-link dropdown-toggle text-dark" href="#" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        <i class="material-icons">person</i>
                        {{__('header.hello')}}
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('admin-logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('header.logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('admin-logout') }}" method="POST"
                              style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>

                <li class="nav-item active">
                    <a class="nav-link" href="{{route('admin')}}">
                        <i class="material-icons">dashboard</i>
                        <p>{{__('menu.dashboard')}}</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"
                       href="{{route('admin.profile-admin',['id'=>\Illuminate\Support\Facades\Auth::guard('admin')->user()->id])}}">
                        <i class="material-icons">assignment_ind</i>
                        <p>{{__('menu.profile')}}</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('admin.admins')}}">
                        <i class="material-icons">group</i>
                        <p>{{__('menu.admins')}}</p>
                    </a>
                </li>
                @if(Auth::guard('admin')->user()->hasRole('super-admin'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('admin.create-admin-index')}}">
                            <i class="material-icons">person_add</i>
                            <p>{{__('menu.add-admin')}}</p>
                        </a>
                    </li>
                @endif

                <li class="nav-item">
                    <a class="nav-link" href="{{route('admin.users')}}">
                        <i class="material-icons">group</i>
                        <p>{{__('menu.users')}}</p>
                    </a>
                </li>

                @if(Auth::guard('admin')->user()->hasRole('super-admin'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('admin.create-user-index')}}">
                            <i class="material-icons">person_add</i>
                            <p>{{__('menu.add-user')}}</p>
                        </a>
                    </li>
                @endif

                <li class="nav-item">
                    <a class="nav-link" href="{{route('admin.lessons')}}">
                        <i class="material-icons">folder_open</i>
                        <p>{{__('menu.lessons')}}</p>
                    </a>
                </li>

                @if(Auth::guard('admin')->user()->hasRole('super-admin'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('admin.create-lesson-index')}}">
                            <i class="material-icons">create_new_folder</i>
                            <p>{{__('menu.add-lesson')}}</p>
                        </a>
                    </li>
                @endif


               {{-- <li class="nav-item">
                    <a class="nav-link" href="{{route('admin.chapters')}}">
                        <i class="material-icons">folder_open</i>
                        <p>{{__('menu.chapters')}}</p>
                    </a>
                </li>--}}

                {{--@if(Auth::guard('admin')->user()->hasRole('super-admin'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('admin.create-chapter-index')}}">
                            <i class="material-icons">create_new_folder</i>
                            <p>{{__('menu.add-chapter')}}</p>
                        </a>
                    </li>
                @endif--}}

                <li class="nav-item">
                    <a class="nav-link" href="{{route('admin.reports')}}">
                        <i class="material-icons">folder_open</i>
                        <p>{{__('menu.reports')}}</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{route('admin.comments')}}">
                        <i class="material-icons">folder_open</i>
                        <p>{{__('menu.comments')}}</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{route('admin.transactions')}}">
                        <i class="material-icons">folder_open</i>
                        <p>{{__('menu.transactions')}}</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{route('admin.import-question-index')}}">
                        <i class="material-icons">star</i>
                        <p>{{__('menu.import_excel')}}</p>
                    </a>
                </li>


            </ul>
        </div>
    </div>

    <div class="main-panel">
        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
            <div class="container-fluid">
                <div class="navbar-wrapper">
                    <a class="navbar-brand" id="view_name" href="#pablo"></a>
                </div>

                <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index"
                        aria-expanded="false"
                        aria-label="Toggle navigation">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="navbar-toggler-icon icon-bar"></span>
                    <span class="navbar-toggler-icon icon-bar"></span>
                    <span class="navbar-toggler-icon icon-bar"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end">

                </div>
            </div>
        </nav>
        <!-- End Navbar -->

        <div class="content">
            <div class="container-fluid">
                {{--alerts section--}}
                <div class="row">
                    <div class="col-12">
                        @if(session('alerts'))
                            @foreach(session('alerts') as $key => $value)
                                @if($key == 'error')
                                    <?php $key = 'danger'; ?>
                                @endif
                                <div class="alert alert-{{$key}} alert-with-icon" data-notify="container">
                                    <i class="material-icons" data-notify="icon">add_alert</i>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <i class="material-icons">close</i>
                                    </button>
                                    <span data-notify="پیام">{{$value}}</span>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
                {{--end of alerts section--}}

                @yield('content')
            </div>
        </div>


    </div>
</div>

@yield('footer')

<!--   Core JS Files   -->
<script src="{{asset('assets/js/core/popper.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/core/bootstrap-material-design.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/plugins/perfect-scrollbar.jquery.min.js')}}"></script>

<!-- Chartist JS -->
<script src="{{asset('assets/js/plugins/chartist.min.js')}}"></script>
<!--  Notifications Plugin    -->
<script src="{{asset('assets/js/plugins/bootstrap-notify.js')}}"></script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="{{asset('assets/js/material-dashboard.min.js?v=2.1.0')}}" type="text/javascript"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="{{asset('assets/demo/demo.js')}}"></script>
<script>
    $(document).ready(function () {
        // Javascript method's body can be found in assets/js/demos.js
        md.initDashboardPageCharts();

    });
</script>

<script>
    $(document).ready(function () {
        var url = window.location;
        $('.nav li.active').removeClass('active');
        // Will only work if string in href matches with location
        $('ul.nav a[href="' + url + '"]').parent().addClass('active');

        // Will also work for relative and absolute hrefs
        $('ul.nav a').filter(function () {
            return this.href == url;
        }).parent().addClass('active').parent().parent().addClass('active');
    });
</script>

{{--confirm any submit button--}}
<script>
    function ConfirmDelete() {
        var x = confirm("{{__('header.confirm')}}");
        if (x)
            return true;
        else
            return false;
    }

    function imgError(image) {
        image.onerror = "";
        image.src = "{{asset('assets/img/faces/marc.jpg')}}";
        return true;
    }

</script>
{{--end of confirm any submit button--}}

<!--summernote-->
<script src="{{asset('js/summernote.js')}}" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        $('.summernote').summernote(
            {
                width: null,                  // set editor width
                height: 150,                 // set editor height, ex) 300
                minHeight: null,              // set minimum height of editor
                maxHeight: null,              // set maximum height of editor
                disableLinkTarget: true,     // hide link Target Checkbox
                disableDragAndDrop: true,    // disable drag and drop event
                focus: true,                 // set focus to editable area after initializing summernote
                // toolbar
                toolbar: [
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['insert', ['hr']],
                    ['view', ['fullscreen', 'codeview']],
                ],
            }
        );
    });
</script>
<!--summernote-->

</body>

</html>
