<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">

    <!--to handle edge-ie-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- mobile responsive meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{-- <title>{{ config('app.name', 'Laravel') }}</title> --}}
    <title>254244</title>
    <meta name="enamad" content="784306" />

    <!-- ** Plugins Needed for the Project ** -->
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{asset('plugins/bootstrap/bootstrap.min.css')}}">
    <!-- FontAwesome -->
    <link rel="stylesheet" href="{{asset('plugins/fontawesome/font-awesome.min.css')}}">
    <!-- Animation -->
    <link rel="stylesheet" href="{{asset('plugins/animate.css')}}">
    <!-- Prettyphoto -->
    <link rel="stylesheet" href="{{asset('plugins/prettyPhoto.css')}}">
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="{{asset('plugins/owl/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/owl/owl.theme.css')}}">
    <!-- Flexslider -->
    <link rel="stylesheet" href="{{asset('plugins/flex-slider/flexslider.css')}}">
    <!-- Flexslider -->
    <link rel="stylesheet" href="{{asset('plugins/cd-hero/cd-hero.css')}}">
    <!-- Style Swicther -->
    <link id="style-switch" href="{{asset('css/presets/preset3.css')}}" media="screen" rel="stylesheet" type="text/css">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
<!--[if lt IE 9]>
    <script src="{{asset('plugins/html5shiv.js')}}"></script>
    <script src="{{asset('plugins/respond.min.js')}}"></script>
    <![endif]-->

    <!-- Main Stylesheet -->
    <link href="{{asset('css/style.css')}}" rel="stylesheet">

    <!--Favicon-->
    <link rel="icon" href="{{asset('images/code.svg')}}" type="image/x-icon"/>
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{asset('images/code.svg')}}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{asset('images/code.svg')}}">
    <link rel="apple-touch-icon-precomposed" href="{{asset('images/code.svg')}}">

    <link href="{{ asset('css/font-face.css') }}" rel="stylesheet">
</head>

<body>

<div class="body-inner">
    <!-- Header start -->
    <header id="header" class="fixed-top header d-flex justify-content-between" role="banner">
        <a class="navbar-bg" href="."><img class="img-fluid float-right" src="{{asset('images/logo.png')}}" width="165"
                                           alt="logo"></a>
        <div class="container mr-0">
            <nav class="navbar navbar-expand-lg navbar-dark">
                <button class="navbar-toggler ml-auto border-0 rounded-0 text-white" type="button"
                        data-toggle="collapse"
                        data-target="#navigation" aria-controls="navigation" aria-expanded="false"
                        aria-label="Toggle navigation">
                    <span class="fa fa-bars"></span>
                </button>

                <div class="collapse navbar-collapse" id="navigation">
                    <ul class="navbar-nav ml-auto">

                        @if(\Illuminate\Support\Facades\Auth::guard('admin')->check())
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('admin')}}">{{__('header.dashboard')}}</a>
                            </li>
                        @else
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="{{ route('admin.login') }}">{{__('header.login')}}</a>
                            </li>
                        @endif

                    </ul>
                </div>
            </nav>
        </div>
    </header>
    <!--/ Header end -->

    <!-- Slider start -->
    <section id="home" class="p-0">
        <div id="main-slide" class="cd-hero">
            <ul class="cd-hero-slider">
                <li class="selected">
                    <div class="image-container">
                    </div>
                    <div class="cutout_text">BMS</div>
                </li>
            </ul>
            <!--/ cd-hero-slider -->

            <div class="cd-slider-nav">
                <nav>
                    <span class="cd-marker item-1"></span>
                    <ul class="list-inline">
                        <li class="selected"><a href="#0"><i class="fa fa-hourglass"></i>مدیران</a></li>
                    </ul>
                </nav>
            </div> <!-- .cd-slider-nav -->

        </div>
        <!--/ Main slider end -->
    </section>
    <!--/ Slider end -->


@yield('content')


<!-- Copyright start -->
    <section id="copyright" class="copyright angle">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <ul class="footer-social unstyled">
                        <li>
                            <a title="linkedin" href="https://www.linkedin.com/in/navid-khosh-rouz/" target="_blank">
                                <span class="icon-pentagon wow bounceIn"><i class="fa fa-linkedin"></i></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div id="back-to-top" data-spy="affix" data-offset-top="10" class="back-to-top affix position-fixed">
                <button class="btn btn-primary" title="Back to Top"><i class="fa fa-angle-double-up"></i></button>
            </div>
        </div>
        <!--/ Container end -->
    </section>
    <!--/ Copyright end -->

</div><!-- Body inner end -->

<!-- jQuery -->
<script src="{{asset('plugins/jQuery/jquery.min.js')}}"></script>
<!-- Bootstrap JS -->
<script src="{{asset('plugins/bootstrap/bootstrap.min.js')}}"></script>
<!-- Style Switcher -->
<script type="text/javascript" src="{{asset('plugins/style-switcher.js')}}"></script>
<!-- Owl Carousel -->
<script type="text/javascript" src="{{asset('plugins/owl/owl.carousel.js')}}"></script>
<!-- PrettyPhoto -->
<script type="text/javascript" src="{{asset('plugins/jquery.prettyPhoto.js')}}"></script>
<!-- Bxslider -->
<script type="text/javascript" src="{{asset('plugins/flex-slider/jquery.flexslider.js')}}"></script>
<!-- CD Hero slider -->
<script type="text/javascript" src="{{asset('plugins/cd-hero/cd-hero.js')}}"></script>
<!-- Isotope -->
<script type="text/javascript" src="{{asset('plugins/isotope.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/ini.isotope.js')}}"></script>
<!-- Wow Animation -->
<script type="text/javascript" src="{{asset('plugins/wow.min.js')}}"></script>
<!-- Eeasing -->
<script type="text/javascript" src="{{asset('plugins/jquery.easing.1.3.js')}}"></script>
<!-- Counter -->
<script type="text/javascript" src="{{asset('plugins/jquery.counterup.min.js')}}"></script>
<!-- Waypoints -->
<script type="text/javascript" src="{{asset('plugins/waypoints.min.js')}}"></script>

<!-- Main Script -->
<script src="{{asset('js/script.js')}}"></script>

</body>

</html>