@extends('layouts.rtl-admin')

@section('content')

    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">{{__('profile.edit')}}</h4>
                    <p class="card-category">{{__('profile.edit_description')}}</p>
                </div>
                <div class="card-body">
                    <form>
                        <div class="row mb-2">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">{{__('profile.first_name')}}</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">{{__('profile.last_name')}}</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="row mb-2">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">{{__('profile.national_no')}}</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">{{__('profile.email')}}</label>
                                    <input type="email" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="row mb-2">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">{{__('profile.phone')}}</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">{{__('profile.licence_no')}}</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="row mb-2">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">{{__('profile.group')}}</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">{{__('profile.major')}}</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="row mb-2">
                            <div class="col-md-9">
                                <div class="form-group">
                                    <label class="bmd-label-floating">{{__('profile.address')}}</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="bmd-label-floating">{{__('profile.area')}}</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                        </div>

                        <button type="submit"
                                class="btn btn-primary pull-right">{{__('profile.submit')}}</button>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4 pt-5">
            <div class="card card-profile">
                <div class="card-avatar">
                    <a href="#pablo">
                        <img class="img" src="../assets/img/faces/marc.jpg"/>
                    </a>
                </div>
                <div class="card-body">
                    <h6 class="card-category text-gray">{{__('profile.user')}}</h6>
                    <a href="#pablo" class="btn btn-primary btn-round">{{__('profile.image_submit')}}</a>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card p-2">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">{{__('profile.mails_list')}}</h4>
                    <p class="card-category">{{__('profile.replies')}}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">{{__('profile.mail_at')}}</h5>
                    <hr>
                    <h4 class="card-title">{{__('profile.mail_title')}}: ...</h4>
                    <p class="card-category"><span class="text-dark">({{__('profile.contains_images')}})</span>
                        - {{__('profile.mail_content')}}: ...</p>
                    <hr>
                    <h5 class="card-title">{{__('profile.mail_at')}}</h5>
                    <hr>
                    <hp class="card-category"><span class="text-dark">({{__('profile.contains_images')}})</span>
                        - {{__('profile.reply_content')}}: ...
                    </hp>
                </div>
            </div>
        </div>
    </div>

@endsection