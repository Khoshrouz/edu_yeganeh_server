<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;

// Überschriften werden genau so übernommen wie sie in der Datei angegeben wurden
HeadingRowFormatter::default('none');

class UsersImport implements ToCollection, WithHeadingRow, WithEvents
{

    public $sheetNames;
    public $sheetData;

    public function __construct()
    {
        $this->sheetNames = [];
        $this->sheetData = [];
    }


    public function registerEvents(): array
    {
        return [
            BeforeSheet::class => function (BeforeSheet $event) {
                $this->sheetNames[] = $event->getSheet()->getTitle();
            }
        ];
    }

    public function chunkSize(): int
    {
        return 20;
    }


    public function collection(Collection $collection)
    {
        $this->sheetData[$this->sheetNames[count($this->sheetNames) - 1]] = $collection;
    }
}