<?php

namespace App\Providers;

use App\Account\Models\Admin;
use App\Account\Models\Chapter;
use App\Account\Models\Course;
use App\Account\Models\Lesson;
use App\Account\Models\Message;
use App\Account\Models\Purchase;
use App\Account\Models\Question;
use App\Observers\PurchaseObserver;
use App\User;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        JsonResource::withoutWrapping();

        Purchase::observe(PurchaseObserver::class);

        Relation::morphMap([
            'admins' => Admin::class,
            'users' => User::class,
            'messages' => Message::class,
            'chapters' => Chapter::class,
            'courses' => Course::class,
            'lessons' => Lesson::class,
            'questions' => Question::class
        ]);
    }
}
