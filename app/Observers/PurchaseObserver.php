<?php

namespace App\Observers;

use App\Account\Models\Chapter;
use App\Account\Models\Lesson;
use App\Account\Models\Purchase;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PurchaseObserver
{
    /**
     * Handle the purchase "created" event.
     *
     * @param \App\Account\Models\Purchase $purchase
     * @return void
     */
    public function created(Purchase $purchase)
    {
        DB::beginTransaction();
        try {
            // update the parents

            $lesson_id = Chapter::whereId($purchase->part->chapter_id)->select('lesson_id')->first();
            Chapter::whereId($purchase->part->chapter_id)->increment('downloads_count');
            Lesson::whereId($lesson_id->lesson_id)->increment('downloads_count');

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollback();
            Log::error('PurchaseObserver-create-error - purchase-id: ' .
                $purchase->id . ' - ' .
                ' exception: ' . $exception->getMessage() . ' -- Line: ' . $exception->getLine());
        }
    }

    /**
     * Handle the purchase "updated" event.
     *
     * @param \App\Account\Models\Purchase $purchase
     * @return void
     */
    public function updated(Purchase $purchase)
    {
        //
    }


    /**
     * Handle the post "deleted" event.
     *
     * @param \App\Account\Models\Purchase $purchase
     * @return void
     */
    public function deleted(Purchase $purchase)
    {
        DB::beginTransaction();
        try {
            // update the parents
            $lesson_id = Chapter::whereId($purchase->part->chapter_id)->select('lesson_id')->first();
            Chapter::whereId($purchase->part->chapter_id)->decrement('downloads_count');
            Lesson::whereId($lesson_id->lesson_id)->decrement('downloads_count');

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollback();
            Log::error('PurchaseObserver-create-error - purchase-id: ' .
                $purchase->id . ' - ' .
                ' exception: ' . $exception->getMessage() . ' -- Line: ' . $exception->getLine());
        }
    }
}
