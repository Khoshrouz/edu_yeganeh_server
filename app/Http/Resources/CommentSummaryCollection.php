<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CommentSummaryCollection extends ResourceCollection
{

    public $collects = CommentSummary::class;

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'comments' => $this->collection,
            'links' => [
                'first' => $this->onFirstPage(),
                'last' => $this->lastPage(),
                'prev' => $this->perPage(),
                'next' => $this->nextPageUrl(),
            ], 'meta' => [
                'current_page' => $this->currentPage(),
                'to' => $this->nextPageUrl(),
                'offset' => 10,
                'total' => $this->total(),
            ],
        ];
    }
}
