<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Morilog\Jalali\Jalalian;


class Transaction extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'bank_data' => $this->bank_data,
            'status' => $this->status,
            'purchase' => new Purchase($this->purchase),
            'created_at' => Carbon::parse($this->created_at)->format('Y-m-d H:i'),
        ];
    }
}
