<?php

namespace App\Http\Resources;

use App\Account\Models\Purchase;
use App\Http\Resources\Image as ImageResource;
use App\Http\Resources\Package as PackageResource;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;


class Chapter extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        if (\auth()->check()) {
            try {
                $is_purchased = Purchase::where('chapter_id', $this->id)
                    ->where('user_id', auth()->user()->id)
                    ->where('expires_at', ">", Carbon::now())
                    ->firstOrFail();
            } catch (Exception $e) {
                $is_purchased = false;
            }
        } else {
            $is_purchased = false;
        }

        return [
            'id' => $this->id,
            'title' => $this->title,
            'purchased' => $is_purchased ? true : false,
            'expires_at' => $is_purchased ? Carbon::parse($is_purchased->expires_at)->format('Y-m-d H:i') : null,
            'updated_at' => Carbon::parse($this->updated_at)->format('Y-m-d H:i'),
            'downloads_count' => $this->purchases_count,
            'questions_count' => $this->questions_count,
            'comprehensive_questions_count' => ($this->comprehensive_questions_count) ? $this->comprehensive_questions_count : 0,
            'stars' => ($this->stars) ? $this->stars : 5,
            'icon' => ($this->icon) ? __('website.server_address') . 'api/v1/icon/' . $this->icon : null,
            'description' => $this->description,
            'parts_count' => $this->parts_count,
            'favorites_count' => (Auth::check()) ? $this->favorites_count : 0,
            'favorites' => (Auth::check()) ? $this->favorites : [],
            'images' => ImageResource::collection($this->images),
            'packages' => PackageResource::collection($this->packages),
        ];
    }
}
