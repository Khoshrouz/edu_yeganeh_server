<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Image extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return __('website.server_address') . 'api/v1/image/' . $this->link;
//        return [
//            //'link' => __('website.server_address') . 'api/image/user/' . $this->link,
//            'link' => __('website.server_address') . 'api/v1/image/' . $this->link,
//        ];
    }
}
