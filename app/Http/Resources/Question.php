<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Question extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'question_image' => ($this->question_image) ? __('website.server_address') . 'api/v1/auth/question/' . $this->question_image : null,
            'question_content' => $this->question_content,
            'first_option' => $this->first_option,
            'second_option' => $this->second_option,
            'third_option' => $this->third_option,
            'fourth_option' => $this->fourth_option,
            'answer' => $this->answer,
            'answer_content' => $this->answer_content,
            'part_id' => $this->part_id,
            'answer_image' => ($this->answer_image) ? __('website.server_address') . 'api/v1/auth/question/' . $this->answer_image : null,
            'favorite' => ($this->favorites_count) ? true : false,
        ];
    }
}
