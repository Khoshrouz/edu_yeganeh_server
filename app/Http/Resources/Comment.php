<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Morilog\Jalali\Jalalian;

class Comment extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'chapter' => $this->commentable->title,
            'user' => explode("@" , $this->user->email),
            'content' => $this->content,
            'stars' => number_format($this->stars, 2),
            'created_at' => Carbon::parse($this->created_at)->format('Y-m-d H:i'),
        ];
    }
}
