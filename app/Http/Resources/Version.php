<?php

namespace App\Http\Resources;

use App\Http\Resources\MailImage as ImageResource;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class Version extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'device' => $this->device,
            'version' => $this->major . '.' . $this->minor . '.' . $this->patch,
        ];
    }
}
