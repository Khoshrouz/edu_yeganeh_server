<?php

namespace App\Http\Resources;

use App\Account\Models\Purchase;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Resources\Json\JsonResource;


class ChapterSummary extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        if (\auth()->check()) {
            try {
                $is_purchased = Purchase::where('chapter_id', $this->id)
                    ->where('user_id', auth()->user()->id)
                    ->where('expires_at', ">", Carbon::now())
                    ->firstOrFail();
            } catch (Exception $e) {
                $is_purchased = false;
            }
        } else {
            $is_purchased = false;
        }


        return [
            'id' => $this->id,
            'purchased' => $is_purchased ? true : false,
            'expires_at' => $is_purchased ? Carbon::parse($is_purchased->expires_at)->format('Y-m-d H:i') : null,
            'title' => $this->title,
            'icon' => ($this->icon) ? __('website.server_address') . 'api/v1/icon/' . $this->icon : null,
        ];
    }
}
