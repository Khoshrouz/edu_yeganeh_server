<?php

namespace App\Http\Resources;

use App\Http\Resources\UserImage as ImageResource;
use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        if (is_null($this->image))
            $image = null;
        else
            $image = $this->image->link;

        //$image = __('website.server_address') . 'api/v1/image/user/' . $this->image->link;

        return [
            'name' => $this->name,
            'melliCode' => $this->melliCode,
            'university_entrance' => $this->university_entrance,
            'email' => $this->email,
            'fcm_token' => $this->fcm_token,
            //'image' => $image,
        ];
    }
}
