<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class Lesson extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'title' => $this->title,
            'chapters_count' => $this->chapters_count,
            'chapters' => ChapterSummary::collection($this->chapters),
            'updated_at' => Carbon::parse($this->updated_at)->format('Y-m-d H:i'),
            'icon' => (isset($this->image->link)) ?
                __('website.server_address') . 'api/v1/image/' . $this->image->link : null,
        ];
    }
}
