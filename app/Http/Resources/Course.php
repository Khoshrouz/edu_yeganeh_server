<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\LessonSummary as LessonResource;

class Course extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'title' => $this->title,
            'icon' => (isset($this->image->link)) ? $this->image->link : null,
            'lessons' => LessonResource::collection($this->lessons),
        ];
    }
}
