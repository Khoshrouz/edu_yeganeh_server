<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserImage extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            //'link' => __('website.server_address') . 'api/v1/image/user/' . $this->link,
            'link' => $this->link,
        ];
    }
}
