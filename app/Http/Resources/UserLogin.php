<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserLogin extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        /*if (is_null($this->image))
            $image = null;
        else
            $image = $this->image->link;*/

        return [
            'name' => $this->name,
            'email' => $this->email,
            'university_entrance' => $this->university_entrance,
            // 'image' => $image,
        ];
    }
}
