<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;

class ApplicationController extends Controller
{
    public function downloadAndroid()
    {
        $path = storage_path() . '/app/applications/seoa.apk';
        if (!File::exists($path)) {
            return response(null, 404);
        }
        $file = File::get($path);
        $type = File::mimeType($path);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;
    }

    public function downloadIos()
    {
        $path = storage_path() . '/app/applications/seoa.ipa';
        if (!File::exists($path)) {
            return response(null, 404);
        }
        $file = File::get($path);
        $type = File::mimeType($path);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;
    }

    public function downloadTarefe()
    {
        $path = storage_path() . '/app/applications/Seoa_tarefe_99.apk';
        if (!File::exists($path)) {
            return response(null, 404);
        }

        return Storage::download(storage_path() . '/app/applications/Seoa_tarefe_99.apk');
    }
}
