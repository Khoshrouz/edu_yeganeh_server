<?php

namespace App\Http\Controllers;

use App\Account\Models\Comment;
use App\Account\Models\Favorite;
use App\Account\Models\Image;
use App\Account\Models\Purchase;
use App\Account\Models\Question;
use App\Account\Models\Report;
use App\Account\Models\Transaction;
use App\Http\Requests\CreateUserPost;
use App\Http\Requests\DeleteUserPost;
use App\Http\Requests\SearchUserPost;
use App\Http\Requests\UpdateUserPost;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin.auth');
    }

    public function getUser($id)
    {
        try {
            /*//avoid admins profiles to be seen by other admins
            if (!Auth::guard('admin')->user()->is_super && $id != Auth::guard('admin')->user()->is_super) {
                $alerts = collect(['warning' => __('alerts.403_super_admin')]);
                return redirect()->route('admin')->with(['alerts' => $alerts]);
            }*/
            $user = User::whereId($id)->with('image')->firstOrFail();
            $reports = Report::where('user_id', $user->id)->orderBy('created_at', 'DESC')
                ->paginate(5);
            $reports_count = Report::where('user_id', $user->id)->where('confirm', false)->count();

            $comments = Comment::where('user_id', $user->id)->orderBy('created_at', 'DESC')
                ->paginate(5);
            $comments_count = Comment::where('user_id', $user->id)->where('confirm', false)->count();

            $transactions = Transaction::where('user_id', $user->id)
                ->with('purchase')->orderBy('created_at', 'DESC')
                ->paginate(5);

            $purchases = Purchase::where('user_id', $user->id)
                ->with('transaction', 'chapter')
                ->orderBy('created_at', 'DESC')
                ->paginate(5);

            /*$d = Purchase::where('user_id', $user->id)->get();
            die(collect($d));*/

//            $favorites = Favorite::where('user_id', $user->id)->orderBy('created_at', 'DESC')
//                ->paginate(5);

            $questions_id = Favorite::where('user_id', $user->id)->pluck('id')->toArray();
            $favorite_questions = Question::whereIn('id', $questions_id)->with('part')->paginate(5);
            /*
             * $alerts = collect(['success' => 'success', 'warning' => 'shit and shit']);
             * then pass 'alerts' => $alerts to the view
            */
            return view('admin.profile-user')->with([
                'user' => $user,
                'reports' => $reports,
                'reports_count' => $reports_count,
                'comments' => $comments,
                'comments_count' => $comments_count,
                'transactions' => $transactions,
                'favorites' => $favorite_questions,
                'purchases' => $purchases,
            ]);
        } catch (\Exception $exception) {
            die($exception);
            $alerts = collect(['error' => __('alerts.load_error')]);
            return redirect()->back()->with(['alerts' => $alerts]);
        }

    }

    public function getUsers()
    {
        $users = User::orderBy('id', 'DESC')->paginate(20);
        return view('admin.users')->with(['users' => $users]);
    }

    public function searchUser(SearchUserPost $request)
    {
        $name = $request->input('name');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $activation_status = $request->input('activation_status');
        $melliCode = $request->input('melliCode');


        $users = User::where('name', 'LIKE', '%' . $name . '%')
            ->where('email', 'LIKE', '%' . $email . '%')
            ->where('phone', 'LIKE', '%' . $phone . '%')
            ->where('melliCode', 'LIKE', '%' . $melliCode . '%')
            ->where('active', 'LIKE', '%' . $activation_status . '%')
            ->orderBy('id', 'DESC')
            ->paginate(20);

        return view('admin.users')->with(['users' => $users]);
    }

    public function createUserIndex()
    {
        return view('admin.create-user');
    }

    public function createUser(CreateUserPost $request)
    {
        DB::beginTransaction();
        try {
            $request->request->add(['password' => Hash::make('password')]);
            $user = User::create($request->all());
            /*
             * all good.
             * save the result in database
             */
            DB::commit();
            $alerts = collect(['success' => __('alerts.create_successful')]);
            return redirect()->route('admin.profile-user', ['id' => $user->id])->with(['alerts' => $alerts]);
        } catch (\Exception $exception) {
            /*
             * database failed, rollback the data
             */
            DB::rollback();
            $alerts = collect(['error' => __('alerts.load_error')]);
            return redirect()->back()->with(['alerts' => $alerts]);
        }
    }

    public function deleteUser(DeleteUserPost $request)
    {
        $id = $request->input('id');

        DB::beginTransaction();
        try {
            $user = User::whereId($id)->withCount('comments', 'favorites', 'reports', 'transactions')->firstOrFail();

            if ($user->comments_count + $user->favorites_count + $user->reports_count + $user->transactions_count > 0) {
                $alerts = collect(['error' => __('alerts.delete_user_error')]);
                return redirect()->back()->with(['alerts' => $alerts]);
            }

            $result = $user->delete();
            if (!$result) {
                $alerts = collect(['error' => __('alerts.load_error')]);
                return redirect()->back()->with(['alerts' => $alerts]);
            }
            /*
             * all good.
             * save the result in database
             */
            DB::commit();
            $alerts = collect(['info' => __('alerts.delete_successful')]);
            return redirect()->route('admin.users')->with(['alerts' => $alerts]);
        } catch (\Exception $exception) {
            /*
             * database failed, rollback the data
             */
            DB::rollback();
            $alerts = collect(['error' => __('alerts.load_error')]);
            return redirect()->back()->with(['alerts' => $alerts]);
        }
    }

    public function updateUser(UpdateUserPost $request)
    {
        DB::beginTransaction();
        try {
            $id = $request->input('id');
            $inputs = $request->only(['name', 'email', 'melliCode', 'phone']);
            $result = User::whereId($id)->update($inputs);
            if (!$result) {
                $alerts = collect(['error' => __('alerts.load_error')]);
                return redirect()->back()->with(['alerts' => $alerts]);
            }
            /*
             * all good.
             * save the result in database
             */
            DB::commit();
            $alerts = collect(['success' => __('alerts.update_successful')]);
            return redirect()->back()->with(['alerts' => $alerts]);
        } catch (\Exception $exception) {
            /*
             * database failed, rollback the data
             */
            DB::rollback();
            $alerts = collect(['error' => __('alerts.load_error')]);
            return redirect()->back()->with(['alerts' => $alerts]);
        }
    }

    public function switchActivity($id)
    {
        DB::beginTransaction();
        try {
            $user = User::findOrFail($id);
            $user->active = $user->active ? 0 : 1;
            $user->save();
            /*
             * revoke the api accessToken to close the connection for the user
             */
            /*$accessToken = $user->token();
            $accessToken->revoke();*/
            DB::table('oauth_access_tokens')
                ->where('user_id', $user->id)->delete();
            /*
             * all good.
             * save the result in database
             */
            DB::commit();
            return response(['available' => $user->available], 200);

            /*$alerts = collect(['warning' => __('alerts.update_successful')]);
            return redirect()->back()->with(['alerts' => $alerts]);*/
        } catch (\Exception $exception) {
            /*
             * database failed, rollback the data
             */
            DB::rollback();
            return response(null, 500);

            /*$alerts = collect(['error' => __('alerts.load_error')]);
            return redirect()->back()->with(['alerts' => $alerts]);*/
        }
    }


    public function displayImage($filename)
    {
        $path = storage_path() . '/app/users/' . $filename;
        if (!File::exists($path)) {
            abort(404);
        }
        $file = File::get($path);
        $type = File::mimeType($path);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;
    }

    public function createUserImage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric|min:1'
        ]);

        if ($validator->fails()) {
            $alerts = collect(['error' => __('alerts.load_error')]);
            return redirect()->back()->with(['alerts' => $alerts]);
        }

        $id = $request->input('id');

        if (!$request->hasfile('file')) {
            $alerts = collect(['warning' => __('alerts.load_error')]);
            return redirect()->back()->with(['alerts' => $alerts]);
        }

        DB::beginTransaction();
        try {
            $file = $request->file('file');
            $hash = Carbon::now()->microsecond . '' . ($id + 268);
            $name = $hash . '.' . $file->extension();
            $file->storeAs('users', $name);
            Image::create([
                'link' => $name,
                'imageable_id' => $id,
                'imageable_type' => 'users',
            ]);
            /*
             * all good.
             * save the result in database
             */
            DB::commit();
            $alerts = collect(['success' => __('alerts.update_successful')]);
            return redirect()->back()->with(['alerts' => $alerts]);
        } catch (\Exception $exception) {
            /*
             * database failed, rollback the data
             */
            DB::rollback();
            $alerts = collect(['error' => __('alerts.load_error')]);
            return redirect()->back()->with(['alerts' => $alerts]);
        }
    }
}
