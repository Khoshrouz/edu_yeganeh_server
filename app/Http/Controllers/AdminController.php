<?php

namespace App\Http\Controllers;

use App\Account\Models\Admin;
use App\Account\Models\Comment;
use App\Account\Models\Question;
use App\Account\Models\Image;
use App\Account\Models\Report;
use App\Account\Models\Favorite;
use App\Account\Models\Lesson;
use App\Account\Models\Course;
use App\Account\Models\Chapter;
use App\Account\Models\Part;
use App\Http\Requests\CreateAdminPost;
use App\Http\Requests\DeleteAdminPost;
use App\Http\Requests\SearchAdminPost;
use App\Http\Requests\UpdateAdminPasswordPost;
use App\Http\Requests\UpdateAdminPost;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Schema;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin.auth:normal-admin');
    }

    public function index()
    {
        $not_confirmed_reports = Report::where('confirm', false)
            ->with('user', 'question')->orderBy('created_at', 'DESC')->paginate(10);
        $not_confirmed_reports_count = Report::where('confirm', false)->count();

        $not_confirmed_comments = Comment::with('user','commentable')
            ->where('confirm', false)
            ->orderBy('created_at', 'DESC')
            ->paginate(10);

        $not_confirmed_comments_count = Comment::where('confirm', false)->count();

        return view('admin')->with([
            'not_confirmed_reports' => $not_confirmed_reports,
            'not_confirmed_reports_count' => $not_confirmed_reports_count,
            'not_confirmed_comments' => $not_confirmed_comments,
            'not_confirmed_comments_count' => $not_confirmed_comments_count,
        ]);
    }

    public function getAdmin($id)
    {
        try {
            /*//avoid admins profiles to be seen by other admins
            if (!Auth::guard('admin')->user()->is_super && $id != Auth::guard('admin')->user()->is_super) {
                $alerts = collect(['warning' => __('alerts.403_super_admin')]);
                return redirect()->route('admin')->with(['alerts' => $alerts]);
            }*/
            $admin = Admin::whereId($id)->with('image')->firstOrFail();
            /*
             * $alerts = collect(['success' => 'success', 'warning' => 'shit and shit']);
             * then pass 'alerts' => $alerts to the view
            */
            return view('admin.profile-admin')->with(['admin' => $admin]);
        } catch (\Exception $exception) {
            $alerts = collect(['error' => __('alerts.load_error')]);
            return redirect()->back()->with(['alerts' => $alerts]);
        }
    }

    public function getAdmins()
    {
        $admins = Admin::where('id', '!=', Auth::guard('admin')->user()->id)
            ->orderBy('created_at', 'DESC')->paginate(20);
        return view('admin.admins')->with(['admins' => $admins]);
    }

    public function searchAdmin(SearchAdminPost $request)
    {
        $name = $request->input('name');
        $email = $request->input('email');
        $activation_status = $request->input('activation_status');

        $admins = Admin::where('id', '!=', Auth::guard('admin')->user()->id)
            ->where('name', 'LIKE', '%' . $name . '%')
            ->where('email', 'LIKE', '%' . $email . '%')
            ->where('active', 'LIKE', '%' . $activation_status . '%')
            ->orderBy('created_at', 'DESC')
            ->paginate(20);
        return view('admin.admins')->with(['admins' => $admins]);
    }

    public function createAdminIndex()
    {
        return view('admin.create-admin');
    }

    public function createAdmin(CreateAdminPost $request)
    {
        DB::beginTransaction();
        try {
            $request->request->add(['password' => Hash::make($request['password'])]);
            $admin = Admin::create($request->all());
            /*
             * all good.
             * save the result in database
             */
            DB::commit();
            $alerts = collect(['success' => __('alerts.create_successful')]);
            return redirect()->route('admin.profile-admin', ['id' => $admin->id])->with(['alerts' => $alerts]);
        } catch (\Exception $exception) {
            /*
             * database failed, rollback the data
             */
            DB::rollback();
            $alerts = collect(['error' => __('alerts.load_error')]);
            return redirect()->route('admin.admins')->with(['alerts' => $alerts]);
        }
    }

    public function deleteAdmin(DeleteAdminPost $request)
    {
        DB::beginTransaction();
        try {
            $id = $request->input('id');
            if ($id == Auth::guard('admin')->user()->id) {
                $alerts = collect(['error' => __('alerts.delete_error')]);
                return redirect()->back()->with(['alerts' => $alerts]);
            }

            $admin = Admin::whereId($id)->with('image')->firstOrFail();
            /*
             * check if the admin has image
             * image contains physical file, need to be the last one
             */
            if (!empty($admin->image)) {
                //delete image from storage folder
                $image = Image::find($admin->image->id);
            }

            //delete the main admin
            $result = $admin->delete();

            $image_result = true;
            if ($result && isset($image)) {
                //delete the image
                $image_result = $image->delete();
            }

            if ($result && $image_result) {
                /*
                 * all good.
                 * save the result in database
                 */
                DB::commit();
                $alerts = collect(['info' => __('alerts.delete_successful')]);
                return redirect()->route('admin.admins')->with(['alerts' => $alerts]);
            } else {
                $alerts = collect(['error' => __('alerts.load_error')]);
                return redirect()->route('admin.admins')->with(['alerts' => $alerts]);
            }
        } catch (\Exception $exception) {
            /*
             * database failed, rollback the data
             */
            DB::rollback();
            $alerts = collect(['error' => __('alerts.load_error')]);
            return redirect()->route('admin.admins')->with(['alerts' => $alerts]);
        }
    }

    public function updateAdmin(UpdateAdminPost $request)
    {
        $id = $request->input('id');
        $inputs = $request->only(['name', 'email']);

        DB::beginTransaction();
        try {
            $result = Admin::whereId($id)->update($inputs);
            /*
             * all good.
             * save the result in database
             */
            DB::commit();
            if ($result) {
                $alerts = collect(['success' => __('alerts.update_successful')]);
                return redirect()->back()->with(['alerts' => $alerts]);
            }
        } catch (\Exception $exception) {
            /*
             * database failed, rollback the data
             */
            DB::rollback();
            $alerts = collect(['error' => __('alerts.load_error')]);
            return redirect()->back()->with(['alerts' => $alerts]);
        }
    }

    public function switchActivity($id)
    {

        DB::beginTransaction();
        try {
            $admin = Admin::findOrFail($id);
            $admin->active = $admin->active ? 0 : 1;
            $admin->save();
            /*
             * all good.
             * save the result in database
             */
            DB::commit();
            return response(['available' => $admin->active], 200);

            /*$alerts = collect(['warning' => __('alerts.update_successful')]);
            return redirect()->back()->with(['alerts' => $alerts]);*/
        } catch (\Exception $exception) {
            /*
             * database failed, rollback the data
             */
            DB::rollback();
            return response(null, 500);

            /*$alerts = collect(['error' => __('alerts.load_error')]);
            return redirect()->back()->with(['alerts' => $alerts]);*/

        }
    }

    public function updateAdminPassword(UpdateAdminPasswordPost $request)
    {
        $id = $request->input('id');
        $password = $request->input('password');
        $old_password = $request->input('old_password');

        DB::beginTransaction();
        try {
            $admin = Admin::findOrFail($id);
            if (!Hash::check($old_password, $admin->password)) {
                $alerts = collect(['error' => __('alerts.update_old_password_error')]);
                return redirect()->back()->with(['alerts' => $alerts]);
            }

            $admin->fill(['password' => Hash::make($password)])->save();
            /*
             * all good.
             * save the result in database
             */
            DB::commit();
            $alerts = collect(['success' => __('alerts.update_successful')]);
            return redirect()->back()->with(['alerts' => $alerts]);
        } catch (\Exception $exception) {
            /*
             * database failed, rollback the data
             */
            DB::rollback();
            $alerts = collect(['error' => __('alerts.load_error')]);
            return redirect()->back()->with(['alerts' => $alerts]);
        }
    }

    public function displayImage($filename)
    {
        $path = storage_path() . '/app/admins/' . $filename;
        if (!File::exists($path)) {
            abort(404);
        }
        $file = File::get($path);
        $type = File::mimeType($path);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;
    }

    public function createAdminImage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric|min:1'
        ]);

        if ($validator->fails()) {
            $alerts = collect(['error' => __('alerts.load_error')]);
            return redirect()->back()->with(['alerts' => $alerts]);
        }
        $id = $request->input('id');

        if ($request->hasfile('file')) {
            DB::beginTransaction();
            try {
                $file = $request->file('file');
                $hash = Carbon::now()->microsecond . '' . ($id + 268);
                $name = $hash . '.' . $file->extension();
                $file->storeAs('admins', $name);
                Image::create([
                    'link' => $name,
                    'imageable_id' => $id,
                    'imageable_type' => 'admins',
                ]);
                //$data[] = $name;
                /*
                 * all good.
                 * save the result in database
                 */
                DB::commit();
                $alerts = collect(['success' => __('alerts.update_successful')]);
                return redirect()->back()->with(['alerts' => $alerts]);
            } catch (\Exception $exception) {
                /*
                 * database failed, rollback the data
                 */
                DB::rollback();
                $alerts = collect(['warning' => __('alerts.load_error')]);
                return redirect()->back()->with(['alerts' => $alerts]);
            }
        } else {
            $alerts = collect(['warning' => __('alerts.load_error')]);
            return redirect()->back()->with(['alerts' => $alerts]);
        }
    }


    public function deleteDB() {
        DB::beginTransaction();
        try {
            Schema::disableForeignKeyConstraints();
            
            Report::truncate();
            Part::truncate();
            Chapter::truncate();
            Lesson::truncate();
            Favorite::truncate();
            Comment::truncate();
            Question::truncate();
            Image::truncate();
            Course::truncate();


            $medical = Course::create([
                'title' => "پزشکی"
            ]);
            Image::create([
                'link' => 'medical.jpg',
                'imageable_id' => $medical->id,
                'imageable_type' => 'courses',
            ]);
    
            $dental = Course::create([
                'title' => "داندان پزشکی"
            ]);
            Image::create([
                'link' => 'dental.jpg',
                'imageable_id' => $dental->id,
                'imageable_type' => 'courses',
            ]);

            DB::commit();
            die("Databse deleted.");
        } catch (\Exception $exception) {
            /*
             * database failed, rollback the data
             */
            DB::rollback();
            dd($exception);
            die("Ask Navid!");
        }


    }
}
