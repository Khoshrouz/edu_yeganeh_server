<?php

namespace App\Http\Controllers\Api;

use App\Account\Models\Version;
use App\Http\Controllers\Controller;
use App\Http\Resources\Version as VersionResource;
use Jenssegers\Agent\Agent;

class VersionController extends Controller
{
    /*
     *   Major version numbers change whenever there is some significant change
     *   being introduced. For example, a large or potentially backward-incompatible change to a software package.
     *
     *   Minor version numbers change when a new, minor feature is introduced or
     *   when a set of smaller features is rolled out.
     *
     *   Patch numbers change when a new build of the software is released to customers.
     *   This is normally for small bug-fixes or the like.
     */

    public function index()
    {
        $versions = Version::where('device', 'ios')
            ->orderBy('major', 'DESC')
            ->orderBy('minor', 'DESC')
            ->orderBy('patch', 'DESC')
            ->get();
        return response(VersionResource::collection($versions));
    }

    public function getStatus($version)
    {
        $agent = new Agent();
        $exploded_version = explode('.', $version);
        if ($agent->isAndroidOS()) {
            $device = 'android';
            $extension = '.apk';
        } else {
            $device = 'ios';
            $extension = '.ipa';
        }

        /*
         * check if any new major version exists
         */
        try {
            $last_version = Version::where('device', $device)->where('major', '>', $exploded_version[0])->orderBy('major', 'DESC')->firstOrFail();
            $data = [
                'status' => 'major',
                'link' => __('website.server_address') . 'download/bmc' . $extension,
                'new' => new VersionResource($last_version),
            ];
            return response(['data' => $data]);

        } catch (\Exception $exception) {
        }


        /*
         * check if any new minor version exists
         */
        try {
            $last_version = Version::where('device', $device)
                ->where('major', $exploded_version[0])
                ->where('minor', '>', $exploded_version[1])->
                orderBy('minor', 'DESC')->firstOrFail();
            $data = [
                'status' => 'minor',
                'link' => __('website.server_address') . 'download/bmc' . $extension,
                'new' => new VersionResource($last_version),
            ];
            return response(['data' => $data]);

        } catch (\Exception $exception) {
        }

        /*
         * check if any new patch version exists
         */
        try {
            $last_version = Version::where('device', $device)
                ->where('major', $exploded_version[0])
                ->where('minor', $exploded_version[1])
                ->where('patch', '>', $exploded_version[2])->
                orderBy('patch', 'DESC')->firstOrFail();
            $data = [
                'status' => 'patch',
                'link' => __('website.server_address') . 'download/bmc' . $extension,
                'new' => new VersionResource($last_version),
            ];
            return response(['data' => $data]);

        } catch (\Exception $exception) {
        }


        /*
         * i you are already updated
         */
        return response()->json(null, 204);
    }

}
