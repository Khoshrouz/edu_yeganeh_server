<?php

namespace App\Http\Controllers\Api;

use App\Account\Models\Chapter;
use App\Account\Models\Comment;
use App\Account\Models\Course;
use App\Account\Models\Favorite;
use App\Account\Models\Image;
use App\Account\Models\Lesson;
use App\Account\Models\Note;
use App\Account\Models\Package;
use App\Account\Models\Part;
use App\Account\Models\Purchase;
use App\Account\Models\Question;
use App\Account\Models\Report;
use App\Account\Models\ReportOption;
use App\Account\Models\Transaction;
use App\Http\Controllers\Controller;
use App\Http\Resources\Note as NoteResource;
use App\Http\Resources\Chapter as ChapterResource;
use App\Http\Resources\Comment as CommentResource;
use App\Http\Resources\CommentCollection;
use App\Http\Resources\CommentSummary;
use App\Http\Resources\CommentSummaryCollection;
use App\Http\Resources\Course as CourseResource;
use App\Http\Resources\Favorite as FavoriteResource;
use App\Http\Resources\Lesson as LessonResource;
use App\Http\Resources\Part as PartResource;
use App\Http\Resources\Question as QuestionResource;
use App\Http\Resources\ReportOptions as ReportOptionsResource;
use App\Http\Resources\ReportCollection;
use App\Http\Resources\TransactionCollection;
use App\Http\Resources\User as UserResource;
use App\Traits\ApiTrait;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;


class ApiController extends Controller
{
    use ApiTrait;

    public function getProfile()
    {
        $data = [
            'user' => new UserResource(auth()->user()),
        ];
        return response(['data' => $data]);
    }

    public function getProfileComments()
    {
        $comments = Comment::where('user_id', auth()->user()->id)
            ->orderBy('created_at', 'DESC')
            ->paginate(10);

        return response(['data' => new CommentCollection($comments)]);
    }

    public function getProfileReports()
    {
        $reports = Report::where('user_id', auth()->user()->id)->where('confirm', true)
            ->orderBy('created_at', 'DESC')
            ->paginate(10);
        $data = [
            'reports' => new ReportCollection($reports),
        ];
        return response(['data' => $data]);
    }

    public function getProfileTransactions()
    {
        $transactions = Transaction::where('user_id', auth()->user()->id)
            ->with('user', 'purchase')
            ->orderBy('created_at', 'DESC')
            ->paginate(10);
        $data = [
            'transactions' => new TransactionCollection($transactions),
        ];
        return response(['data' => $data]);
    }

    public function getCourses()
    {
        $courses = Course::Where('available', true)->with(['image', 'lessons' => function ($query) {
            $query->select('id', 'course_id', 'title');
            $query->with('image');
            $query->orderBy('id', 'DESC');
        }])->get();

        $data = [
            'courses' => CourseResource::collection($courses),
        ];
        return response(['data' => $data]);
    }

    public function getLesson($id)
    {
        try {
            $lesson = Lesson::whereId($id)
                ->with('chapters')
                ->withCount('chapters')->firstOrFail();

            $favorites = null;
            if (Auth::check())
                $favorites = Favorite::where('user_id', Auth::user()->id)->get();

            $data = [
                'lesson' => new LessonResource($lesson),
                "favorites" => ($favorites) ? FavoriteResource::collection($favorites) : []
            ];
            return response(['data' => $data]);
        } catch (\Exception $exception) {
            Log::warning('ApiController-getLesson-warning 404 - lesson: ' . $id .
                ' exception: ' . $exception->getMessage() . ' -- Line: ' . $exception->getLine());
            return response(['message' => $this->restErrorCodes(404)], 404);
        }
    }

    public function getComments($id)
    {
        $yours = Comment::where('commentable_type', 'chapters')
            ->where('commentable_id', $id)
            ->where('user_id', Auth::user()->id)
            ->where('confirm', true)->first();

        $comments = Comment::where('commentable_type', 'chapters')
            ->where('commentable_id', $id)
            ->where('user_id', '!=', Auth::user()->id)
            ->where('confirm', true)
            ->orderBy('created_at', 'DESC')
            ->paginate(10);

        $data = [
            'yours' => (is_null($yours) ? null : new CommentSummary($yours)),
            'others' => new CommentSummaryCollection($comments)
        ];

        return response(['data' => $data]);
    }

    public function getFreeComments($id)
    {
        $comments = Comment::where('commentable_type', 'chapters')
            ->where('commentable_id', $id)
            ->where('confirm', true)
            ->orderBy('created_at', 'DESC')
            ->paginate(10);

        return response(['data' => new CommentSummaryCollection($comments)]);
    }


    public function postComment(Request $request)
    {
        $request->validate([
            'chapter_id' => 'required|numeric|min:1',
            'content' => 'required|string|min:4|max:255',
            'stars' => 'required|numeric|between:1.00,5.00',
        ]);

        $chapter_id = $request->input('chapter_id');
        $content = $request->input('content');
        $stars = $request->input('stars');

        DB::beginTransaction();
        try {
            if (!Chapter::where('id', $chapter_id)->exists()) {
                return response(['message' => $this->restErrorCodes(404)], 404);
            }

            $comment = Comment::where('commentable_id', $chapter_id)->where('commentable_type', 'chapters')->where('user_id', \auth()->user()->id)->first();
            if (is_null($comment)) {
                // no record has found, create new one
                $comment = Comment::create([
                    'content' => $content,
                    'stars' => $stars,
                    'commentable_id' => $chapter_id,
                    'commentable_type' => 'chapters',
                    'user_id' => Auth::user()->id,
                ]);
                DB::commit();
                //************ return the response *****************
                $data = [
                    'comment' => new CommentResource($comment),
                ];
                return response(['data' => $data]);
            } else {
                /*$data = [
                    'comment' => new CommentResource($comment),
                ];*/
                return response(['message' => $this->restErrorCodes(409)], 409);
            }
        } catch (\Exception $exception) {
            Log::warning('ApiController-postComment-warning 404 - chapter: ' . $chapter_id .
                ' exception: ' . $exception->getMessage() . ' -- Line: ' . $exception->getLine());
            /*
             * database failed, rollback the data
             */
            DB::rollback();
            return response(['message' => $this->restErrorCodes(500)], 500);
        }
    }

    public function getChapter($id)
    {
        DB::beginTransaction();
        try {
            $chapter = Chapter::whereId($id)
                ->with(['parts', 'images', 'packages', 'favorites'])
                ->withCount('parts', 'favorites', 'purchases')->firstOrFail();

            $data = [
                'chapter' => new ChapterResource($chapter),
            ];
            return response(['data' => $data]);
        } catch (\Exception $exception) {
            Log::warning('ApiController-getChapter-warning 404 - lesson: ' . $id .
                ' exception: ' . $exception->getMessage() . ' -- Line: ' . $exception->getLine());
            return response(['message' => $this->restErrorCodes(404)], 404);
        }
    }

    //    public function getChapters($lesson_id)
    //    {
    //        if (Auth::check()) {
    //            $withCount = [['parts', 'favorites' => function ($query) {
    //                $query->where('user_id', \auth()->user()->id);
    //            }]];
    //        } else {
    //            $withCount = "parts";
    //        }
    //
    //        $chapters = Chapter::where('available', true)
    //            ->where('lesson_id', $lesson_id)
    //            ->withCount($withCount)
    //            ->orderBy('id', 'DESC')
    //            ->get();
    //
    //        $data = [
    //            'chapters' => ChapterResource::collection($chapters),
    //        ];
    //        return response(['data' => $data]);
    //    }

    public function getParts($chapter_id)
    {
        try {

            $purchased = Purchase::where("chapter_id", $chapter_id)
                ->where('expires_at', ">", Carbon::now())
                ->where("user_id", auth()->user()->id)->exists();

            if (!$purchased)
                return response(['message' => $this->restErrorCodes(403)], 403);

            $part = Part::Where('available', true)
                ->where('chapter_id', $chapter_id)
                ->withCount('questions')
                ->orderBy('id', 'DESC')
                ->get();

            //            $favorites = Favorite::where('user_id', \auth()->user()->id)
            //                ->where('chapter_id', $chapter_id)->pluck('question_id')->count();

            $data = [
                //                'favorites' => $favorites,
                'parts' => PartResource::collection($part),
            ];
            return response(['data' => $data]);
        } catch (\Exception $exception) {
            return response(['message' => $this->restErrorCodes(500)], 500);
        }
    }

    public function getChapterQuestions($chapter_id)
    {
        DB::beginTransaction();
        try {
            $purchased = Purchase::where("chapter_id", $chapter_id)
                ->where('expires_at', ">", Carbon::now())
                ->where("user_id", \auth()->user()->id)->exists();

            if (!$purchased)
                return response(['message' => $this->restErrorCodes(403)], 403);

            $parts = Part::where('chapter_id', $chapter_id)->pluck('id')->toArray();
            $questions = Question::Where('available', true)
                ->whereIn('part_id', $parts)
                ->withCount(['favorites' => function ($query) {
                    $query->where('user_id', Auth::user()->id);
                }])
                ->get();

            $data = [
                'count' => count($questions),
                'questions' => QuestionResource::collection($questions),
            ];
            return response(['data' => $data]);
        } catch (\Exception $exception) {
            Log::warning('ApiController-getChapterQuestions-warning 404 - chapter: ' . $chapter_id .
                ' exception: ' . $exception->getMessage() . ' -- Line: ' . $exception->getLine());
            return response(['message' => $this->restErrorCodes(404)], 404);
        }
    }

    public function getQuestions(Request $request)
    {
        $request->validate([
            'parts.*' => 'required|numeric|min:1'
        ]);

        // $package = ($request->input('package')) ? $request->input('package') : 20;

        //        //fetch the array of parts
        $parts = $request->input('parts');
        $chapters_id = Part::whereIn('id', $parts)->pluck('chapter_id')->toArray();
        $purchased_count = Purchase::whereIn('chapter_id', $chapters_id)
            ->where('expires_at', ">", Carbon::now())
            ->where('user_id', auth()->user()->id)->count();
        if ($purchased_count != sizeof($chapters_id)) {
            return response(['message' => $this->restErrorCodes(403)], 403);
        }

        $questions = Question::Where('available', true)
            ->whereIn('part_id', $parts)
            ->withCount(['favorites' => function ($query) {
                $query->where('user_id', Auth::user()->id);
            }])
            ->get();

        $data = [
            'count' => count($questions),
            'questions' => QuestionResource::collection($questions),
        ];
        return response(['data' => $data]);
    }

    public function setFavorite(Request $request)
    {
        $request->validate([
            'question_id' => 'required|numeric|min:1',
        ]);

        $question_id = $request->input('question_id');

        if (Favorite::where('question_id', $question_id)->where('user_id', Auth::user()->id)->exists()) {
            return response(['message' => $this->restErrorCodes(409)], 409);
        }

        DB::beginTransaction();
        try {
            //check if question is available
            $question = Question::findOrFail($question_id);
            $part_id = $question->part_id;
            $part_id = $question->part_id;

            Favorite::create([
                'user_id' => \auth()->user()->id,
                'question_id' => $question_id,
                'part_id' => $question->part_id,
                'chapter_id' => $question->part->chapter_id,
                'lesson_id' => $question->part->chapter->lesson_id
            ]);

            DB::commit();
            //************ return the response *****************
            return response()->json(null, 204);
        } catch (\Exception $exception) {
            /*
             * database failed, rollback the data
             */
            DB::rollback();
            return response(['message' => $this->restErrorCodes(404)], 404);
        }
    }

    public function deleteFavorite($id)
    {
        if (Favorite::where('question_id', $id)->where('user_id', Auth::user()->id)->exists()) {
            Favorite::where("question_id", $id)->where('user_id', Auth::user()->id)->delete();
        } else {
            return response(['message' => $this->restErrorCodes(404)], 404);
        }
    }

    public function getFavorites($chapter_id)
    {
        try {
            $purchased = Purchase::where("chapter_id", $chapter_id)
                ->where('expires_at', ">", Carbon::now())
                ->where("user_id", auth()->user()->id)->exists();

            if (!$purchased) {
                return response(['message' => $this->restErrorCodes(404)], 404);
            }

            $questions_id = Favorite::where('user_id', \auth()->user()->id)
                ->where('chapter_id', $chapter_id)->pluck('question_id')->toArray();

            $questions = Question::Where('available', true)
                ->whereIn('id', $questions_id)->get();

            if (!count($questions))
                return response(['message' => $this->restErrorCodes(404)], 404);

            $data = [
                'count' => count($questions),
                'questions' => QuestionResource::collection($questions),
            ];
            return response(['data' => $data]);
        } catch (\Exception $exception) {
            return response(['message' => $this->restErrorCodes(404)], 404);
        }
    }

    public function getLessonFavorites($lesson_id)
    {
        try {
            $lesson = Lesson::whereId($lesson_id)->with('chapters')->firstOrFail();
            $chapters = $lesson->chapters;

            $merged_arrays = null;
            foreach ($chapters as $chapter) {
                $purchased = Purchase::where("chapter_id", $chapter->id)
                    ->where('expires_at', ">", Carbon::now())
                    ->where("user_id", auth()->user()->id)->exists();

                if ($purchased) {
                    $questions_ids = Favorite::where('user_id', \auth()->user()->id)
                        ->where('chapter_id', $chapter->id)->pluck('question_id')->toArray();

                    $questions = Question::Where('available', true)
                        ->whereIn('id', $questions_ids)->get();

                    $merged_arrays = ($merged_arrays)
                        ? $merged_arrays->merge($questions)
                        : $questions;
                }
            }

            if (!$merged_arrays)
                return response(['message' => $this->restErrorCodes(404)], 404);

            $data = [
                'questions' => QuestionResource::collection($merged_arrays),
            ];
            return response(['data' => $data]);
        } catch (\Exception $exception) {
            return response(['message' => $this->restErrorCodes(404)], 404);
        }
    }

    public function getChapterIcon($icon)
    {
        $path = storage_path() . '/app/chapters/' . $icon;
        if (!File::exists($path)) {
            abort(404);
        }
        $file = File::get($path);
        $type = File::mimeType($path);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;
    }

    public function getImage($image)
    {
        try {
            $image_file = Image::where('link', $image)->firstOrFail();
        } catch (\Exception $exception) {
            abort(404);
        }

        $path = storage_path() . '/app/' . $image_file->imageable_type . '/' . $image;
        if (!File::exists($path)) {
            abort(404);
        }
        $file = File::get($path);
        $type = File::mimeType($path);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;
    }


    public function getQuestionImage($image)
    {
        $path = storage_path() . '/app/questions/' . $image;
        if (!File::exists($path)) {
            abort(404);
        }
        $file = File::get($path);
        $type = File::mimeType($path);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;
    }

    public function purchaseChapter($id, $package_id)
    {
        $package = Package::where('id', $package_id)->where('chapter_id', $id)->first();
        if (!$package) {
            return response(['message' => $this->restErrorCodes(404)], 404);
        }

        //already have it
        if (Purchase::where('user_id', Auth::user()->id)
            ->where('chapter_id', $id)
            ->where('expires_at', ">", Carbon::now())
            ->exists()
        ) {
            return response(['message' => $this->restErrorCodes(409)], 409);
        }

        if (!Chapter::whereId($id)->exists()) {
            return response(['message' => $this->restErrorCodes(404)], 404);
        }

        DB::beginTransaction();
        try {
            $transaction = Transaction::create([
                'user_id' => Auth::user()->id,
                'bank_data' => "Bank data here...",
                'status' => "paid",
            ]);

            // for test we set the duration to minutes
            Purchase::create([
                'user_id' => Auth::user()->id,
                'expires_at' => Carbon::now()->addMinutes($package->duration),
                'chapter_id' => $id,
                'transaction_id' => $transaction->id,
            ]);

            DB::commit();
            //************ return the response *****************
            return response()->json(null, 204);
        } catch (\Exception $exception) {
            /*
             * database failed, rollback the data
             */
            DB::rollback();
            return response(['message' => $this->restErrorCodes(404)], 404);
        }
    }

    public function deletePurchase($id)
    {
        DB::beginTransaction();
        try {
            $purchase = Purchase::where('user_id', Auth::user()->id)->where('chapter_id', $id)->first();
            Transaction::whereId($purchase->transaction_id)->delete();
            $purchase->delete();

            DB::commit();
            //************ return the response *****************
            return response()->json(null, 204);
        } catch (\Exception $exception) {
            /*
             * database failed, rollback the data
             */
            DB::rollback();
            return response(['message' => $this->restErrorCodes(404)], 404);
        }
    }

    public function getReportOptions()
    {
        $report_options = ReportOption::all();
        $data = [
            'report_options' => ReportOptionsResource::collection($report_options),
        ];
        return response(['data' => $data]);
    }

    public function setReport(Request $request)
    {
        $request->validate([
            'report_option_id' => 'required|numeric|min:1',
            'question_id' => 'required|numeric|min:1',
            'content' => 'required|string|min:1|max:500',
        ]);

        $question_id = $request->input('question_id');
        $report_option_id = $request->input('report_option_id');
        $content = $request->input('content');

        if (!ReportOption::where('id', $report_option_id)->exists()) {
            return response(['message' => $this->restErrorCodes(409)], 409);
        }

        try {
            // find the question
            $question = Question::where('id', $question_id)->with('part')->firstOrFail();

            // check if the question is owned by the user
            $exists = Purchase::where('chapter_id', $question->part->chapter_id)
                ->where('user_id', auth()->user()->id)
                ->where('expires_at', ">", Carbon::now())
                ->exists();

            // if it is expired, then return false
            if (!$exists)
                return response(['message' => $this->restErrorCodes(403)], 403);
        } catch (\Exception $exception) {
            return response(['message' => $this->restErrorCodes(404)], 404);
        }

        DB::beginTransaction();
        try {
            Report::create([
                'user_id' => \auth()->user()->id,
                'question_id' => $question_id,
                'content' => $content,
                'report_option_id' => $report_option_id
            ]);

            DB::commit();
            //************ return the response *****************
            return response()->json(null, 204);
        } catch (\Exception $exception) {
            /*
             * database failed, rollback the data
             */
            DB::rollback();
            return response(['message' => $this->restErrorCodes(500)], 500);
        }
    }

    public function getNote($id)
    {
        $purchased = Purchase::where("chapter_id", $id)
            ->where('expires_at', ">", Carbon::now())
            ->where("user_id", auth()->user()->id)->exists();

        if (!$purchased)
            return response(['message' => $this->restErrorCodes(403)], 403);

        $note = Note::where('notable_id', $id)
            ->where('notable_type', 'chapters')->first();

        $data = [
            'note' => $note ? new NoteResource($note) : null,
        ];
        return response(['data' => $data]);
    }

    public function setNote(Request $request)
    {
        $request->validate([
            'chapter_id' => 'required|numeric|min:1',
            'content' => 'required|string|min:1|max:65000',
        ]);
        $chapter_id = $request->input('chapter_id');
        $content = $request->input('content');

        // check if the chapter is purchased and exists
        $purchased = Purchase::where("chapter_id", $chapter_id)
            ->where('expires_at', ">", Carbon::now())
            ->where("user_id", auth()->user()->id)->exists();

        if (!$purchased)
            return response(['message' => $this->restErrorCodes(403)], 403);

        DB::beginTransaction();
        try {
            $note = Note::where('notable_id', $chapter_id)->where('notable_type', 'chapters')->firstOrFail();

            // it exists, let's update it
            $note->content = $content;
            $note->save();

            DB::commit();
            //************ return the response *****************
            return response()->json(null, 204);
        } catch (\Exception $exception) {
        }

        try {
            // first time, let's create new one
            Note::create([
                'content' => $content,
                'user_id' => auth()->user()->id,
                'notable_id' => $chapter_id,
                'notable_type' => 'chapters',
            ]);

            DB::commit();
            //************ return the response *****************
            return response()->json(null, 204);
        } catch (\Exception $exception) {
            /*
             * database failed, rollback the data
             */
            DB::rollback();
            return response(['message' => $this->restErrorCodes(500)], 500);
        }
    }
}
