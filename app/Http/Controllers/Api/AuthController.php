<?php

namespace App\Http\Controllers\Api;

use App\Account\Models\Message;
use App\Http\Controllers\Controller;
use App\Traits\ApiTrait;
use App\Traits\NotificationTrait;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\User as UserResource;
use App\Http\Resources\UserLogin as UserLoginResource;
use Illuminate\Validation\Rule;
use Morilog\Jalali\Jalalian;


class AuthController extends Controller
{
    use ApiTrait, NotificationTrait;

    public function register(Request $request)
    {
        $validatedData = $request->validate([
            'email' => 'required|string|email|max:255|' . Rule::unique('users'),
            'university_entrance' => 'required|numeric|regex:/^[0-9]+$/|min:' .
                Jalalian::now()->subYears(3)->getYear() . '|max:' . Jalalian::now()->getYear(),
            'phone' => 'required|alpha_num|' . Rule::unique('users'),
        ]);

        DB::beginTransaction();
        try {
            // save the phone as default password for future usage
            $validatedData['password'] = Hash::make("password");
            $user = User::create($validatedData);

            // //we automatically attempt to login
            // $loginData = [
            //     'email' => $user->email,
            //     'password' => 'password'
            // ];
            // if (!Auth::attempt($loginData)) {
            //     return response(['message' => $this->restErrorCodes(500)], 500);
            // }

            // /*
            //  * creating the accessToken so that the user can have communication
            //  *  with the server for the next 30 with this accessToken
            //  */
            // $accessToken = auth()->user()->createToken('authTokenBms')->accessToken;
            // $data = ['user' => new UserLoginResource(auth()->user()), 'access_token' => $accessToken];
            // /*
            // * all good.
            // * save the result in database
            // */
            DB::commit();
            //************ return the response *****************
            return response()->json(null, 204);
        } catch (\Exception $exception) {
            /*
             * database failed, rollback the data
             */
            DB::rollback();
            return response(['message' => $this->restErrorCodes(500)], 500);
        }
    }


    public function authentication(Request $request)
    {
        $request->validate([
            'phone' => 'required',
        ]);


        try {
            /*
             * check if the user exists
             */
            $phone = $request->input('phone');
            $user = User::where('phone', $phone)->firstOrFail();
        } catch (\Exception $exception) {
            /*
             * returning 404 as the response
             */
            return response(['message' => $this->restErrorCodes(404)], 404);
        }

        DB::beginTransaction();
        try {
            /*
             * Deleting previous auth-messages
             */
            Message::where('messageable_id', $user->id)->where('messageable_type', 'users')->delete();

            /*
            * creating and sending sms to User
            */
            $message = Message::create([
                'expires_at' => Carbon::now()->addMinutes(2)->format('Y-m-d H:i:s'),
                'content' => rand(15876, 98256),
                'messageable_type' => 'users',
                'messageable_id' => $user->id,
            ]);
            //sending the sms to user
            $this->sendSMS($phone);

            /*
             * all good.
             * save the result in database
             */
            DB::commit();
            //************ return the response *****************
            $data = [
                'code' => $message->only(['expires_at', 'content']),
            ];
            return response(['data' => $data]);
        } catch (\Exception $exception) {
            /*
             * database failed, rollback the data
             */
            DB::rollback();
            return response(['message' => $this->restErrorCodes(500)], 500);
        }
    }

    public function login(Request $request)
    {
        $request->validate([
            'code' => 'required|numeric',
            'phone' => 'required',
        ]);


        /*
         * validating the user
         */
        try {
            /*
             * check if the user exists and the code belongs to the user
             */
            $phone = $request->input('phone');
            $user = User::where('phone', $phone)->firstOrFail();
        } catch (\Exception $exception) {
            /*
             * returning 404 as the response
             */
            return response(['message' => $this->restErrorCodes(404)], 404);
        }

        DB::beginTransaction();
        try {
            /*
            * validating the auth-code in message
            */
            try {
                /*
                 * check if the code belongs to the user
                 */
                $code = $request->input('code');
                //also check if the message is not expired and exist
                $message = Message::where('content', $code)->where('messageable_id', $user->id)
                    ->where('messageable_type', 'users')
                    ->where('expires_at', '>=', Carbon::now()->format('Y-m-d H:i:s'))
                    ->firstOrFail();

                //reset the attempts
                $user->attempts = 0;
                $user->save();
            } catch (\Exception $exception) {
                //check the attempts
                if ($user->attempts == 4) {
                    $user->attempts = 0;
                    //before sending respond, need to delete the expired messages in table
                    Message::where('messageable_id', $user->id)->where('messageable_type', 'users')->delete();
                } else {
                    //increase the attempts
                    $user->attempts++;
                }
                $user->save();

                /*
                 * returning 404 as the response
                 */
                return response(['message' => $this->restErrorCodes(404)], 404);
            }

            /*
             * check if the user is forbidden
             */
            if (!$user->active)
                return response(['message' => $this->restErrorCodes(403)], 403);

            /*
             * user is authorized
             */
            //we automatically attempt to login
            $loginData = [
                'email' => $user->email,
                'password' => 'password'
            ];
            if (!Auth::attempt($loginData)) {
                return response(['message' => $this->restErrorCodes(500)], 500);
            }


            /*
             * login was successful message need to be deleted
             */
            $message->delete();

            /*
             * Make sure the client is not in application anymore, before creating new accessToken
             */

            DB::table('oauth_access_tokens')
                ->where('user_id', $user->id)->delete();

            /*
             * creating the accessToken so that the user can have communication
             *  with the server for the next 30 with this accessToken
             */
            $accessToken = auth()->user()->createToken('authTokenBms')->accessToken;
            $data = ['user' => new UserLoginResource(auth()->user()), 'access_token' => $accessToken];
            /*
                 * all good.
                 * save the result in database
                 */
            DB::commit();
            //************ return the response *****************
            return response(['data' => $data]);
        } catch (\Exception $exception) {

            /*
             * database failed, rollback the data
             */
            DB::rollback();
            return response(['message' => $this->restErrorCodes(500)], 500);
        }
    }

    public function logout()
    {
        DB::beginTransaction();
        try {
            DB::table('oauth_access_tokens')
                ->where('user_id', Auth::user()->id)->delete();

            $user = auth()->user();
            $user->fcm_token = null;
            $user->save();

            DB::commit();
            //************ return the response *****************
            return response()->json(null, 204);
        } catch (\Exception $exception) {
            /*
             * database failed, rollback the data
             */
            DB::rollback();
            return response(['message' => $this->restErrorCodes(500)], 500);
        }
    }

    public function updateFcmToken(Request $request)
    {
        $request->validate([
            'fcm_token' => 'required|string',
        ]);

        DB::beginTransaction();
        try {
            /*
             * updating the fcm token
             */
            $user = Auth::user();
            $user->fcm_token = $request->input('fcm_token');
            $user->save();

            DB::commit();
            //************ return the response *****************
            return response()->json(null, 204);
        } catch (\Exception $exception) {
            /*
             * database failed, rollback the data
             */
            DB::rollback();
            return response(['message' => $this->restErrorCodes(500)], 500);
        }
    }
}
