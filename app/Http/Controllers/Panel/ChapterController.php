<?php

namespace App\Http\Controllers\Panel;

use App\Account\Models\Chapter;
use App\Account\Models\Comment;
use App\Account\Models\Image;
use App\Account\Models\Lesson;
use App\Account\Models\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ChapterController extends PanelController
{
    public function __construct()
    {
        $this->middleware('admin.auth:normal-admin');
    }

    public function index()
    {
        return view('chapter');
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|min:3',
            'description' => 'required|string|min:1|max:500',
            'lesson_id' => 'required|numeric|min:1',
            'files.*' => 'nullable|max:10000|mimes:jpeg,jpg,png',
            'file' => 'required|max:1024|mimes:jpeg,jpg,png'
        ]);

        if ($validator->fails()) {
            $alerts = collect(['error' => __('alerts.load_error')]);
            return redirect()->back()->with(['alerts' => $alerts])
                ->withInput($request->input())
                ->withErrors($validator->errors());
        }

        DB::beginTransaction();
        try {
            $file = $request->file('file');
            $hash = Carbon::now()->microsecond . '' . rand(123, 897);
            $icon = $hash . '.' . $file->extension();
            $file->storeAs('chapters', $icon);
            $request->merge(['icon' => $icon]);

            $chapter = Chapter::create($request->all());

            if ($request->hasfile('files')) {
                foreach ($request->file('files') as $file) {
                    $hash = Carbon::now()->microsecond . '' . ($chapter->id + 216);
                    $name = $hash . '.' . $file->extension();
                    $file->storeAs('chapters', $name);
                    Image::create([
                        'link' => $name,
                        'imageable_id' => $chapter->id,
                        'imageable_type' => 'chapters',
                    ]);
                }
            }
            /*
             * all good.
             * save the result in database
             */
            DB::commit();
            $alerts = collect(['info' => __('alerts.create_successful')]);
            Log::info('ChapterController-create-successful - admin: ' .
                auth('admin')->user()->name);
            return redirect()->back()->with(['alerts' => $alerts]);
        } catch (\Exception $exception) {
            /*
             * database failed, rollback the data
             */
            DB::rollback();
            Log::error('ChapterController-create-error - admin: ' .
                auth('admin')->user()->name .
                ' exception: ' . $exception->getMessage() . ' -- Line: ' . $exception->getLine());
            $alerts = collect(['error' => __('alerts.load_error')]);
            return redirect()->back()->with(['alerts' => $alerts]);
        }
    }

    public function createIndex($lesson_id = 0)
    {
        $alerts = collect(['error' => __('alerts.load_error')]);
        return redirect()->back()->with(['alerts' => $alerts]);
    }

    public function update(Request $request)
    {
        $id = $request->input('id');

        $request->validate([
            'id' => 'required|numeric|min:1',
            'title' => 'required|string|min:3',
            'description' => 'required|string|min:10|max:1000',
            'lesson_id' => 'required|numeric|min:1',
            'files.*' => 'nullable|max:10000|mimes:jpeg,jpg,png',
            'file' => 'nullable|max:1024|mimes:jpeg,jpg,png'
        ]);

        DB::beginTransaction();
        try {
            $chapter = Chapter::whereId($id)->firstOrFail();

            if ($request->hasfile('files')) {
                foreach ($request->file('files') as $file) {
                    $hash = Carbon::now()->microsecond . '' . ($id + 216);
                    $name = $hash . '.' . $file->extension();
                    $file->storeAs('chapters', $name);
                    Image::create([
                        'link' => $name,
                        'imageable_id' => $id,
                        'imageable_type' => 'chapters',
                    ]);
                }
            }

            if ($request->hasfile('file')) {
                $file = $request->file('file');
                $hash = Carbon::now()->microsecond . '' . ($id + 216);
                $new_icon = $hash . '.' . $file->extension();
                $file->storeAs('chapters', $new_icon);

                //delete the previous image
                Storage::delete('chapters/' . $chapter->icon);

                //add the new icon to request
                $chapter->icon = $new_icon;
            }

            $chapter->title = $request->input('title');
            $chapter->description = $request->input('description');
            $chapter->lesson_id = $request->input('lesson_id');

            $chapter->save();

            $chapter->lesson->touch();
            $chapter->lesson->course->touch();


            /*
             * all good.
             * save the result in database
             */
            DB::commit();
            $alerts = collect(['info' => __('alerts.update_successful')]);
            Log::info('ChapterController-update-successful - admin: ' .
                auth('admin')->user()->name . ' - chapter: ' . $id);
            return redirect()->back()->with(['alerts' => $alerts]);
        } catch (\Exception $exception) {
            /*
             * database failed, rollback the data
             */
            DB::rollback();
            Log::error('ChapterController-update-error - admin: ' .
                auth('admin')->user()->name . ' - chapter: ' . $id .
                ' exception: ' . $exception->getMessage() . ' -- Line: ' . $exception->getLine());
            $alerts = collect(['error' => __('alerts.load_error')]);
            return redirect()->back()->with(['alerts' => $alerts]);
        }
    }

    public function updateIndex($id)
    {
        try {
            $chapter = Chapter::whereId($id)->with('lesson', 'images')->firstOrFail();
            $lessons = Lesson::get();
            return view('admin.update-chapter')->with([
                'lessons' => $lessons,
                'chapter' => $chapter
            ]);
        } catch (\Exception $exception) {
            $alerts = collect(['error' => __('alerts.load_error')]);
            return redirect()->back()->with(['alerts' => $alerts]);
        }
    }

    public function delete($id)
    {
        DB::beginTransaction();
        try {
            $chapter = Chapter::whereId($id)->withCount('parts', 'images')->firstOrFail();
            $hasChildren = ($chapter->parts_count > 0) ? true : false;
            if ($hasChildren) {
                $alerts = collect(['warning' => __('alerts.delete_item_error')]);
                return redirect()->back()->with(['alerts' => $alerts]);
            }
            //delete if there are any images
            $images = $chapter->images;
            foreach ($images as $image) {
                Storage::delete('chapters/' . $image->link);
            }

            //delete the previous image
            Storage::delete('chapters/' . $chapter->icon);
            $chapter->delete();
            /*
             * all good.
             * save the result in database
             */
            DB::commit();
            $alerts = collect(['info' => __('alerts.delete_successful')]);
            Log::info('ChapterController-delete-successful - admin: ' .
                auth('admin')->user()->name . ' - chapter: ' . $id);
            return redirect()->back()->with(['alerts' => $alerts]);
        } catch (\Exception $exception) {
            /*
             * database failed, rollback the data
             */
            DB::rollback();
            Log::error('ChapterController-delete-error - admin: ' .
                auth('admin')->user()->name . ' - chapter: ' . $id .
                ' exception: ' . $exception->getMessage() . ' -- Line: ' . $exception->getLine());
            $alerts = collect(['error' => __('alerts.load_error')]);
            return redirect()->back()->with(['alerts' => $alerts]);
        }
    }

    public function get($id)
    {
        try {
            $chapter = Chapter::whereId($id)
                ->withCount('parts', 'images', 'purchases')
                ->with(['parts' => function ($query) {
                    $query->orderBy('id', 'DESC');
                    $query->withCount('questions');
                }], 'images', 'lesson', 'packages')
                ->firstOrFail();

            $confirmed_comments = Comment::where('commentable_id', $chapter->id)
                ->where('commentable_type', 'chapters')
                ->with('user')
                ->where('confirm', true)
                ->orderBy('created_at', 'DESC')
                ->paginate(5);

            $not_confirmed_comments = Comment::where('commentable_id', $chapter->id)
                ->where('commentable_type', 'chapters')
                ->with('user')
                ->where('confirm', false)
                ->orderBy('created_at', 'DESC')
                ->paginate(5);

            /*
             * all good.
             * save the result in database
             */
            return view('admin.profile-chapter')->with([
                'chapter' => $chapter,
                'confirmed_comments' => $confirmed_comments,
                'not_confirmed_comments' => $not_confirmed_comments,
            ]);
        } catch (\Exception $exception) {
            /*
             * database failed, rollback the data
             */
            Log::error('ChapterController-get-error - admin: ' .
                auth('admin')->user()->name . ' - chapter: ' . $id .
                ' exception: ' . $exception->getMessage() . ' -- Line: ' . $exception->getLine());
            $alerts = collect(['error' => __('alerts.load_error')]);
            return redirect()->back()->with(['alerts' => $alerts]);
        }
    }

    public function getItems()
    {
        $chapters = Chapter::orderBy('id', 'DESC')->withCount('parts')->paginate(30);
        return view('admin.chapters')->with(['chapters' => $chapters]);
    }

    public function searchItems(Request $request)
    {
        $title = $request->input('title');
        $available = $request->input('available');

        $chapters = Chapter::where('title', 'LIKE', '%' . $title . '%')
            ->where('available', 'LIKE', '%' . $available . '%')
            ->orderBy('id', 'DESC')->withCount('chapters')->paginate(30);
        return view('admin.chapters')->with(['chapters' => $chapters]);
    }

    public function getImage($image)
    {
        $path = storage_path() . '/app/chapters/' . $image;
        if (!File::exists($path)) {
            abort(404);
        }
        $file = File::get($path);
        $type = File::mimeType($path);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;
    }

    public function deleteImage($id)
    {
        DB::beginTransaction();
        try {
            $image = Image::find($id);
            $image->delete();
            /*
             * all good.
             * save the result in database
             */
            DB::commit();
            $alerts = collect(['info' => __('alerts.delete_successful')]);
            Log::info('ChapterController-delete-image-successful - admin: ' .
                auth('admin')->user()->name . ' - chapter: ' . $id);
            return redirect()->back()->with(['alerts' => $alerts]);
        } catch (\Exception $exception) {
            /*
             * database failed, rollback the data
             */
            DB::rollback();
            Log::error('ChapterController-delete-image-error - admin: ' .
                auth('admin')->user()->name . ' - chapter: ' . $id .
                ' exception: ' . $exception->getMessage() . ' -- Line: ' . $exception->getLine());
            $alerts = collect(['error' => __('alerts.load_error')]);
            return redirect()->back()->with(['alerts' => $alerts]);
        }
    }
}
