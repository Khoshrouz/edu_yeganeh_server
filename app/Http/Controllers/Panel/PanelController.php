<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

abstract class PanelController extends Controller
{
    public abstract function __construct();
    public abstract function index();
    public abstract function create(Request $request);
    public abstract function createIndex();
    public abstract function update(Request $request);
    public abstract function updateIndex($id);
    public abstract function delete($id);
    public abstract function get($id);
    public abstract function getItems();
    public abstract function searchItems(Request $request);
    public abstract function getImage($image);
    public abstract function deleteImage($id);
}