<?php

namespace App\Http\Controllers\Panel;

use App\Account\Models\Report;
use App\Account\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class TransactionController extends PanelController
{

    public function __construct()
    {
    }

    public function index()
    {
        // TODO: Implement index() method.
    }

    public function create(Request $request)
    {
        // TODO: Implement create() method.
    }

    public function createIndex()
    {
        // TODO: Implement createIndex() method.
    }

    public function update(Request $request)
    {
        // TODO: Implement update() method.
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    public function get($id)
    {
        // TODO: Implement get() method.
    }

    public function getItems()
    {
        // return all the reports
        $transactions = Transaction::orderBy('created_at', 'DESC')->with('user')->paginate(30);
        return view('admin.transactions')->with(['transactions' => $transactions]);
    }

    public function searchItems(Request $request)
    {
        // TODO: Implement searchItems() method.
    }

    public function getImage($image)
    {
        // TODO: Implement getImage() method.
    }

    public function deleteImage($id)
    {
        // TODO: Implement deleteImage() method.
    }

    public function updateIndex($id)
    {
        // TODO: Implement updateIndex() method.
    }
}
