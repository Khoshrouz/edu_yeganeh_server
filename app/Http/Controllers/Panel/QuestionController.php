<?php

namespace App\Http\Controllers\Panel;

use App\Account\Models\Chapter;
use App\Account\Models\Comment;
use App\Account\Models\Course;
use App\Account\Models\Image;
use App\Account\Models\Lesson;
use App\Account\Models\Package;
use App\Account\Models\Part;
use App\Account\Models\Question;
use App\Account\Models\Report;
use App\Imports\UsersImport;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class QuestionController extends PanelController
{

    public function __construct()
    {
        // nothing
    }

    public function importQuestionIndex()
    {
        $courses = Course::all();
        return view('admin.import-question')->with(['courses' => $courses]);
    }

    public function importQuestion(Request $request)
    {
        $request->validate([
            'file' => 'required|max:1024|mimes:xlsx,xls,xlx',
            'course_id' => 'required|alpha_num|min:1',
        ]);

        $course_id = $request->input('course_id');

        // $path = $request->file('file')->getRealPath();

        $Import = new UsersImport();
        Excel::import($Import, $request->file('file'));

        $course = Course::whereId($course_id)->first();

        DB::beginTransaction();
        try {
            $org_file_name = $request->file('file')->getClientOriginalName();
            $filename = trim(substr($org_file_name, 0, strrpos($org_file_name, ".")));
            $exploded_file_title = explode('-', $filename);
            $lesson_title = trim($exploded_file_title[0]);
            $chapter_title = trim($exploded_file_title[1]);


            $lesson = Lesson::firstOrCreate([
                "title" => $lesson_title,
                "course_id" => $course->id
            ]);

            $chapter = Chapter::firstOrCreate([
                "title" => $chapter_title,
                "icon" => "no_icon",
                "description" => "برای این قسمت توضیحاتی در نظر گرفنه نشده است.",
                "lesson_id" => $lesson->id,
            ]);

            $perm_to_create_package = Package::where('chapter_id', $chapter->id)->exists();
            if (!$perm_to_create_package) {
                Package::create([
                    "price" => "6000",
                    "duration" => 6,
                    "chapter_id" => $chapter->id
                ]);

                Package::create([
                    "price" => "12000",
                    "duration" => 12,
                    "chapter_id" => $chapter->id
                ]);

                Package::create([
                    "price" => "18000",
                    "duration" => 24,
                    "chapter_id" => $chapter->id
                ]);
            }

            foreach ($Import->sheetData as $key => $value) {
                // sheet_title ==> $key
                // data in value ==> $value

                // $exploded_sheet_title = explode('-', $key);
                // $lesson_title = trim($exploded_sheet_title[0]);
                // $chapter_title = trim($exploded_sheet_title[1]);
                // $part_title = trim($exploded_sheet_title[2]);

                $part_title = trim($key);

                $part = Part::firstOrCreate([
                    "title" => $part_title,
                    "chapter_id" => $chapter->id,
                ]);

                $questions_count = 0;
                foreach ($value as $question) {
                    $temp = array();
                    foreach ($question as $column) {
                        array_push($temp, $column);
                    }

                    if ($temp[1]) {
                        $questions_count++;

                        Question::create([
                            "question_content" => $temp[1] ? $temp[1] : "-",
                            "question_image" => $temp[2] ? $temp[2] : null,
                            "first_option" => $temp[3] ? $temp[3] : "-",
                            "second_option" => $temp[4] ? $temp[4] : "-",
                            "third_option" => $temp[5] ? $temp[5] : "-",
                            "fourth_option" => $temp[6] ? $temp[6] : "-",
                            "answer" => intval($temp[7]) ? intval($temp[7]) : 1,
                            "answer_content" => $temp[8] ? $temp[8] : "-",
                            "answer_image" => $temp[9] ? $temp[9] : null,
                            "part_id" => $part->id
                        ]);
                    }
                }

                $chapter->questions_count = $chapter->questions_count + $questions_count;
                $chapter->save();
            }

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollback();
            die($exception);
        }

        return redirect()->back();
    }

    public function index()
    {
        // TODO: Implement index() method.
    }

    public function create(Request $request)
    {
        // TODO: Implement create() method.
    }

    public function createIndex()
    {
        // TODO: Implement createIndex() method.
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric|min:1',
            'answer_content' => 'required|string|min:1|max:1000',
            'question_content' => 'required|string|min:1|max:1000',
            'first_option' => 'required|string|min:1|max:1000',
            'second_option' => 'required|string|min:1|max:1000',
            'third_option' => 'required|string|min:1|max:1000',
            'fourth_option' => 'required|string|min:1|max:1000',
            'answer' => 'required|numeric|min:1|max:4',
            'question_image' => 'nullable|max:1024|mimes:jpeg,jpg,png',
            'answer_image' => 'nullable|max:1024|mimes:jpeg,jpg,png'
        ]);

        $id = $request->input('id');

        if ($validator->fails()) {
            $alerts = collect(['error' => __('alerts.load_error')]);
            return redirect()->back()->with(['alerts' => $alerts])
                ->withInput($request->input())
                ->withErrors($validator->errors());
        }



        $id = $request->input('id');

        DB::beginTransaction();
        try {
            $question = Question::whereId($id)->firstOrFail();

            if ($request->hasfile('question_image')) {
                $question_image = $request->file('question_image');
                $hash = Carbon::now()->microsecond . '' . ($id + 216);
                $new_icon = $hash . '.' . $question_image->extension();
                $question_image->storeAs('questions', $new_icon);

                //delete the previous image
                Storage::delete('questions/' . $question->question_image);

                //add the new icon to request
                $question->question_image = $new_icon;
            }

            if ($request->hasfile('answer_image')) {
                $answer_image = $request->file('answer_image');
                $hash = Carbon::now()->microsecond . '' . ($id + 216);
                $new_icon = $hash . '.' . $answer_image->extension();
                $answer_image->storeAs('questions', $new_icon);

                //delete the previous image
                Storage::delete('questions/' . $question->answer_image);

                //add the new icon to request
                $question->answer_image = $new_icon;
            }

            $question->answer_content = $request->input('answer_content');
            $question->question_content = $request->input('question_content');
            $question->answer = $request->input('answer');

            $question->first_option = $request->input('first_option');
            $question->second_option = $request->input('second_option');
            $question->third_option = $request->input('third_option');
            $question->fourth_option = $request->input('fourth_option');

            $question->save();
            $question->part->touch();
            $question->part->chapter->touch();
            $question->part->chapter->lesson->touch();
            $question->part->chapter->lesson->course->touch();


            /*
             * all good.
             * save the result in database
             */
            DB::commit();
            $alerts = collect(['info' => __('alerts.update_successful')]);
            Log::info('QuestionController-update-successful - admin: ' .
                auth('admin')->user()->name . ' - question: ' . $id);
            return redirect()->back()->with(['alerts' => $alerts]);
        } catch (\Exception $exception) {
            /*
             * database failed, rollback the data
             */

            die($exception);
            DB::rollback();
            Log::error('QuestionController-update-error - admin: ' .
                auth('admin')->user()->name . ' - question: ' . $id .
                ' exception: ' . $exception->getMessage() . ' -- Line: ' . $exception->getLine());
            $alerts = collect(['error' => __('alerts.load_error')]);
            return redirect()->back()->with(['alerts' => $alerts]);
        }
    }

    public function updateIndex($id)
    {
        try {
            $question = Question::whereId($id)->with('part')->firstOrFail();
            return view('admin.update-question')->with([
                'question' => $question
            ]);
        } catch (\Exception $exception) {
            $alerts = collect(['error' => __('alerts.load_error')]);
            return redirect()->back()->with(['alerts' => $alerts]);
        }
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    public function get($id)
    {
        // TODO: Implement get() method.
    }

    public function getItems()
    {
        // TODO: Implement getItems() method.
    }

    public function searchItems(Request $request)
    {
        // TODO: Implement searchItems() method.
    }

    public function getImage($image)
    {
        $path = storage_path() . '/app/questions/' . $image;
        if (!File::exists($path)) {
            abort(404);
        }
        $file = File::get($path);
        $type = File::mimeType($path);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;
    }

    public function deleteImage($id)
    {
        DB::beginTransaction();
        try {
            $question = Question::whereId($id)->firstOrFail();

            Storage::delete('questions/' . $question->question_image);
            //add the new icon to request
            $question->question_image = null;
            $question->save();
            /*
             * all good.
             * save the result in database
             */
            DB::commit();
            $alerts = collect(['info' => __('alerts.delete_successful')]);
            Log::info('QuestionController-delete-image-successful - admin: ' .
                auth('admin')->user()->name . ' - question: ' . $id);
            return redirect()->back()->with(['alerts' => $alerts]);
        } catch (\Exception $exception) {
            /*
             * database failed, rollback the data
             */
            DB::rollback();
            Log::error('QuestionController-delete-image-error - admin: ' .
                auth('admin')->user()->name . ' - question: ' . $id .
                ' exception: ' . $exception->getMessage() . ' -- Line: ' . $exception->getLine());
            $alerts = collect(['error' => __('alerts.load_error')]);
            return redirect()->back()->with(['alerts' => $alerts]);
        }
    }

    public function deleteAnswerImage($id)
    {
        DB::beginTransaction();
        try {
            $question = Question::whereId($id)->firstOrFail();

            Storage::delete('questions/' . $question->answer_image);
            //add the new icon to request
            $question->answer_image = null;
            $question->save();

            /*
             * all good.
             * save the result in database
             */
            DB::commit();
            $alerts = collect(['info' => __('alerts.delete_successful')]);
            Log::info('QuestionController-delete-answer-image-successful - admin: ' .
                auth('admin')->user()->name . ' - question: ' . $id);
            return redirect()->back()->with(['alerts' => $alerts]);
        } catch (\Exception $exception) {
            /*
             * database failed, rollback the data
             */
            DB::rollback();
            Log::error('QuestionController-delete-answer-image-error - admin: ' .
                auth('admin')->user()->name . ' - question: ' . $id .
                ' exception: ' . $exception->getMessage() . ' -- Line: ' . $exception->getLine());
            $alerts = collect(['error' => __('alerts.load_error')]);
            return redirect()->back()->with(['alerts' => $alerts]);
        }
    }
}
