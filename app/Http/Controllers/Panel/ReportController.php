<?php

namespace App\Http\Controllers\Panel;

use App\Account\Models\Report;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ReportController extends PanelController
{

    public function confirm($id)
    {
        DB::beginTransaction();
        try {
            $report = Report::findOrFail($id);
            $report->confirm = !$report->confirm;
            $report->save();
            /*
            * all good.
            * save the result in database
            */
            DB::commit();
            $alerts = collect(['info' => __('alerts.update_successful')]);
            Log::info('ReportController-update-confirm-successful - admin: ' .
                auth('admin')->user()->name . ' - report: ' . $id);
            return redirect()->back()->with(['alerts' => $alerts]);

        } catch (\Exception $exception) {
            /*
            * database failed, rollback the data
            */
            DB::rollback();
            Log::error('ReportController-confirm-error - admin: ' .
                auth('admin')->user()->name . ' - report: ' . $id .
                ' exception: ' . $exception->getMessage() . ' -- Line: ' . $exception->getLine());
            $alerts = collect(['error' => __('alerts.load_error')]);
            return redirect()->back()->with(['alerts' => $alerts]);
        }
    }

    public function __construct()
    {
    }

    public function index()
    {
        // TODO: Implement index() method.
    }

    public function create(Request $request)
    {
        // TODO: Implement create() method.
    }

    public function createIndex()
    {
        // TODO: Implement createIndex() method.
    }

    public function update(Request $request)
    {
        // TODO: Implement update() method.
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    public function get($id)
    {
        // TODO: Implement get() method.
    }

    public function getItems()
    {
        // return all the reports
        $reports = Report::orderBy('created_at', 'DESC')->with('user', 'question')->paginate(30);
        return view('admin.reports')->with(['reports' => $reports]);
    }

    public function searchItems(Request $request)
    {
        // TODO: Implement searchItems() method.
    }

    public function getImage($image)
    {
        // TODO: Implement getImage() method.
    }

    public function deleteImage($id)
    {
        // TODO: Implement deleteImage() method.
    }

    public function updateIndex($id)
    {
        // TODO: Implement updateIndex() method.
    }
}
