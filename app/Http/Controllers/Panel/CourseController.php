<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CourseController extends PanelController
{

    public function __construct()
    {
        // nothing
    }

    public function index()
    {
        // TODO: Implement index() method.
    }

    public function create(Request $request)
    {
        // TODO: Implement create() method.
    }

    public function createIndex()
    {
        // TODO: Implement createIndex() method.
    }

    public function update(Request $request)
    {
        // TODO: Implement update() method.
    }

    public function updateIndex($id)
    {
        // TODO: Implement updateIndex() method.
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    public function get($id)
    {
        // TODO: Implement get() method.
    }

    public function getItems()
    {
        // TODO: Implement getItems() method.
    }

    public function searchItems(Request $request)
    {
        // TODO: Implement searchItems() method.
    }

    public function getImage($image)
    {
        // TODO: Implement getImage() method.
    }

    public function deleteImage($id)
    {
        // TODO: Implement deleteImage() method.
    }
}
