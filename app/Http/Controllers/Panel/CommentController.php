<?php

namespace App\Http\Controllers\Panel;

use App\Account\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx\Comments;

class CommentController extends PanelController
{

    public function confirm($id)
    {
        DB::beginTransaction();
        try {
            $comment = Comment::whereId($id)->with('commentable')->firstOrFail();
            $comment->confirm = !$comment->confirm;
            $comment->save();

            // comments are for lessons
            $comments = Comment::where('commentable_id', $comment->commentable_id)->where('confirm', true)
                ->select('stars')->get();
            $count = $comments->count();
            $sum = $comments->sum('stars');

            //echo $comments . ' --- ' . $count . ' --- ' . $sum . ' --- ' . $comment->commentable->stars;
            $comment->commentable->stars = (float)($sum / $count);
            $comment->commentable->save();
            //die(' **** ' . $comment->commentable->stars);
            /*
            * all good.
            * save the result in database
            */
            DB::commit();
            $alerts = collect(['info' => __('alerts.update_successful')]);
            Log::info('CommentController-update-confirm-successful - admin: ' .
                auth('admin')->user()->name . ' - Comment: ' . $id);
            return redirect()->back()->with(['alerts' => $alerts]);
        } catch (\Exception $exception) {
            die($exception);
            /*
            * database failed, rollback the data
            */
            DB::rollback();
            Log::error('CommentController-confirm-error - admin: ' .
                auth('admin')->user()->name . ' - Comment: ' . $id .
                ' exception: ' . $exception->getMessage() . ' -- Line: ' . $exception->getLine());
            $alerts = collect(['error' => __('alerts.load_error')]);
            return redirect()->back()->with(['alerts' => $alerts]);
        }
    }

    public function __construct()
    {
    }

    public function index()
    {
        // TODO: Implement index() method.
    }

    public function create(Request $request)
    {
        // TODO: Implement create() method.
    }

    public function createIndex()
    {
        // TODO: Implement createIndex() method.
    }

    public function update(Request $request)
    {
        // TODO: Implement update() method.
    }

    public function updateIndex($id)
    {
        // TODO: Implement updateIndex() method.
    }

    public function delete($id)
    {
        DB::beginTransaction();
        try {
            $comment = Comment::whereId($id)->with('commentable')->firstOrFail();
            $comment->delete();
            /*
            * all good.
            * save the result in database
            */
            DB::commit();
            $alerts = collect(['info' => __('alerts.delete_successful')]);
            Log::info('CommentController-delete-successful - admin: ' .
                auth('admin')->user()->name . ' - Comment: ' . $id);
            return redirect()->back()->with(['alerts' => $alerts]);
        } catch (\Exception $exception) {
            /*
            * database failed, rollback the data
            */
            DB::rollback();
            Log::error('CommentController-delete-error - admin: ' .
                auth('admin')->user()->name . ' - Comment: ' . $id .
                ' exception: ' . $exception->getMessage() . ' -- Line: ' . $exception->getLine());
            $alerts = collect(['error' => __('alerts.load_error')]);
            return redirect()->back()->with(['alerts' => $alerts]);
        }
    }

    public function get($id)
    {
        // TODO: Implement get() method.
    }

    public function getItems()
    {
        // return all the reports
        $comments = Comment::with('user', 'commentable')
            ->orderBy('created_at', 'DESC')
            ->where('confirm', false)
            ->paginate(30);
        return view('admin.comments')->with(['comments' => $comments]);
    }

    public function searchItems(Request $request)
    {
        // TODO: Implement searchItems() method.
    }

    public function getImage($image)
    {
        // TODO: Implement getImage() method.
    }

    public function deleteImage($id)
    {
        // TODO: Implement deleteImage() method.
    }
}
