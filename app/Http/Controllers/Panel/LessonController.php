<?php

namespace App\Http\Controllers\Panel;

use App\Account\Models\Course;
use App\Account\Models\Image;
use App\Account\Models\Lesson;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class LessonController extends PanelController
{
    //
    public function __construct()
    {
        $this->middleware('admin.auth:normal-admin');
    }

    public function index()
    {
        return view('lesson');
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|min:3',
            'course_id' => 'required|alpha_num|min:1',
            'file' => 'required|max:1024|mimes:jpeg,jpg,png'
        ]);

        if ($validator->fails()) {
            $alerts = collect(['error' => __('alerts.load_error')/*. ' -- ' .
                $validator->errors()*/]);
            return redirect()->back()->with(['alerts' => $alerts])
                ->withInput($request->input())
                ->withErrors($validator->errors());
        }

        DB::beginTransaction();
        try {
            $lesson = Lesson::create($request->all());

            //saving the icon
            $file = $request->file('file');
            $hash = Carbon::now()->microsecond . '' . rand(258, 369);
            $icon = $hash . '.' . $file->extension();
            $file->storeAs('lessons', $icon);
            Image::create([
                'link' => $icon,
                'imageable_id' => $lesson->id,
                'imageable_type' => 'lessons',
            ]);

            /*
             * all good.
             * save the result in database
             */
            DB::commit();
            $alerts = collect(['info' => __('alerts.create_successful')]);
            Log::info('LessonController-create-successful - admin: ' .
                auth('admin')->user()->name);
            return redirect()->back()->with(['alerts' => $alerts]);
        } catch (\Exception $exception) {
            /*
             * database failed, rollback the data
             */
            DB::rollback();
            Log::error('LessonController-create-error - admin: ' .
                auth('admin')->user()->name . ' - ' .
                ' exception: ' . $exception->getMessage() . ' -- Line: ' . $exception->getLine());
            $alerts = collect(['error' => __('alerts.load_error')]);
            return redirect()->back()->with(['alerts' => $alerts]);
        }
    }

    public function createIndex()
    {
        $lessons = Lesson::all();
        $courses = Course::all();
        return view('admin.create-lesson')->with(['lessons' => $lessons, 'courses' => $courses]);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric|min:1',
            'title' => 'required|string|min:3',
            'course_id' => 'required|alpha_num|min:1',
            'file' => 'nullable|max:1024|mimes:jpeg,jpg,png'
        ]);

        $id = $request->input('id');

        if ($validator->fails()) {
            $alerts = collect(['error' => __('alerts.load_error')]);
            return redirect()->back()->with(['alerts' => $alerts])
                ->withInput($request->input())
                ->withErrors($validator->errors());
        }

        DB::beginTransaction();
        try {
            $lesson = Lesson::whereId($id)->with('image')->firstOrFail();

            if ($request->hasfile('file')) {
                $file = $request->file('file');
                $hash = Carbon::now()->microsecond . '' . ($id + 216);
                $new_icon = $hash . '.' . $file->extension();
                $file->storeAs('lessons', $new_icon);

                //delete the previous image
                if (!empty($lesson->image)) {
                    $lesson->image->delete();
                }

                //add the new icon to request
                Image::create([
                    'link' => $new_icon,
                    'imageable_id' => $lesson->id,
                    'imageable_type' => 'lessons',
                ]);
            }

            $lesson->title = $request->input('title');
            $lesson->course_id = $request->input('course_id');

            $lesson->save();
            $lesson->course->touch();

            /*
             * all good.
             * save the result in database
             */
            DB::commit();
            $alerts = collect(['info' => __('alerts.update_successful')]);
            Log::info('LessonController-update-successful - admin: ' .
                auth('admin')->user()->name . ' - lesson: ' . $id);
            return redirect()->back()->with(['alerts' => $alerts]);
        } catch (\Exception $exception) {
            /*
             * database failed, rollback the data
             */
            DB::rollback();

            Log::error('LessonController-update-error - admin: ' .
                auth('admin')->user()->name . ' - lesson: ' . $id .
                ' exception: ' . $exception->getMessage() . ' -- Line: ' . $exception->getLine());
            $alerts = collect(['error' => __('alerts.load_error')]);
            return redirect()->back()->with(['alerts' => $alerts]);
        }
    }

    public function updateIndex($id)
    {
        try {
            $lesson = Lesson::whereId($id)->with('chapters', 'course', 'image')->firstOrFail();

            $lessons = Lesson::where('course_id', $lesson->course_id)->orderBy('id', 'DESC')->get();
            $courses = Course::all();

            return view('admin.update-lesson')->with([
                'course' => $lesson->course,
                'lessons' => $lessons,
                'courses' => $courses,
                'lesson' => $lesson
            ]);
        } catch (\Exception $exception) {
            die($exception);
            $alerts = collect(['error' => __('alerts.load_error')]);
            return redirect()->back()->with(['alerts' => $alerts]);
        }
    }

    public function delete($id)
    {
        DB::beginTransaction();
        try {
            $lesson = Lesson::whereId($id)->with('image')->withCount("chapters")->firstOrFail();
            $hasChildren = ($lesson->chapters_count > 0) ? true : false;
            if ($hasChildren) {
                $alerts = collect(['warning' => __('alerts.delete_item_error')]);
                return redirect()->back()->with(['alerts' => $alerts]);
            }

            if (!empty($lesson->image)) {
                //delete the icon
                Storage::delete('lessons/' . $lesson->image->link);
            }

            $lesson->delete();
            /*
             * all good.
             * save the result in database
             */
            DB::commit();
            $alerts = collect(['info' => __('alerts.delete_successful')]);
            Log::info('LessonController-delete-successful - admin: ' .
                auth('admin')->user()->name . ' - lesson: ' . $id);
            return redirect()->back()->with(['alerts' => $alerts]);
        } catch (\Exception $exception) {
            /*
             * database failed, rollback the data
             */
            DB::rollback();
            Log::error('LessonController-delete-error - admin: ' .
                auth('admin')->user()->name . ' - lesson: ' . $id .
                ' exception: ' . $exception->getMessage() . ' -- Line: ' . $exception->getLine());
            $alerts = collect(['error' => __('alerts.load_error')]);
            return redirect()->back()->with(['alerts' => $alerts]);
        }
    }

    public function get($id)
    {
        try {
            $lesson = Lesson::whereId($id)
                ->withCount('chapters')
                ->with(['chapters' => function ($query) {
                    $query->with(['packages']);
                    $query->withCount('purchases');
                    $query->orderBy('id', 'DESC');
                }], 'image', 'course')
                ->firstOrFail();
            /*
             * all good.
             * save the result in database
             */
            return view('admin.profile-lesson')->with(['lesson' => $lesson]);
        } catch (\Exception $exception) {
            /*
             * database failed, rollback the data
             */
            Log::error('LessonController-get-error - admin: ' .
                auth('admin')->user()->name . ' - lesson: ' . $id .
                ' exception: ' . $exception->getMessage() . ' -- Line: ' . $exception->getLine());
            $alerts = collect(['error' => __('alerts.load_error')]);
            return redirect()->back()->with(['alerts' => $alerts]);
        }
    }

    public function getItems()
    {
        $lessons = Lesson::orderBy('course_id', 'DESC')
            ->withCount('chapters', 'course')->paginate(50);
        return view('admin.lessons')->with(['lessons' => $lessons]);
    }

    public function searchItems(Request $request)
    {
        $course_id = $request->input('course_id');
        $title = $request->input('title');
        $available = $request->input('available');

        if (is_null($course_id)) {
            $lessons = Lesson::where('title', 'LIKE', '%' . $title . '%')
                ->where('available', 'LIKE', '%' . $available . '%')
                ->orderBy('id', 'DESC')
                ->orderBy('course_id', 'DESC')
                ->withCount('lessons')->paginate(30);
        } else {
            $lessons = Lesson::where('title', 'LIKE', '%' . $title . '%')
                ->where('available', 'LIKE', '%' . $available . '%')
                ->where('course_id', $course_id)
                ->orderBy('course_id', 'DESC')
                ->orderBy('id', 'DESC')->withCount('lessons')->paginate(30);
        }
        return view('admin.lessons')->with(['lessons' => $lessons]);
    }

    public function getImage($image)
    {
        $path = storage_path() . '/app/lessons/' . $image;
        if (!File::exists($path)) {
            abort(404);
        }
        $file = File::get($path);
        $type = File::mimeType($path);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;
    }

    public function deleteImage($id)
    {
        DB::beginTransaction();
        try {
            $image = Image::find($id);
            $image->delete();
            /*
             * all good.
             * save the result in database
             */
            DB::commit();
            $alerts = collect(['info' => __('alerts.delete_successful')]);
            Log::info('LessonController-delete-image-successful - admin: ' .
                auth('admin')->user()->name . ' - lesson: ' . $id);
            return redirect()->back()->with(['alerts' => $alerts]);
        } catch (\Exception $exception) {
            /*
             * database failed, rollback the data
             */
            DB::rollback();
            Log::error('LessonController-delete-image-error - admin: ' .
                auth('admin')->user()->name . ' - lesson: ' . $id .
                ' exception: ' . $exception->getMessage() . ' -- Line: ' . $exception->getLine());
            $alerts = collect(['error' => __('alerts.load_error')]);
            return redirect()->back()->with(['alerts' => $alerts]);
        }
    }
}
