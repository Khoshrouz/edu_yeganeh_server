<?php

namespace App\Http\Controllers\Panel;

use App\Account\Models\Chapter;
use App\Account\Models\Part;
use App\Account\Models\Question;
use App\Account\Models\Report;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PartController extends PanelController
{

    public function __construct()
    {
    }

    public function index()
    {

        // TODO: Implement index() method.
    }

    public function create(Request $request)
    {
        // TODO: Implement create() method.
    }

    public function createIndex()
    {
        $alerts = collect(['error' => __('alerts.load_error')]);
        return redirect()->back()->with(['alerts' => $alerts]);
    }

    public function update(Request $request)
    {
        // TODO: Implement update() method.
    }

    public function updateIndex($id)
    {
        $alerts = collect(['error' => __('alerts.load_error')]);
        return redirect()->back()->with(['alerts' => $alerts]);
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    public function get($id)
    {

        DB::beginTransaction();
        try {
            $part = Part::whereId($id)
                ->with('chapter')
                ->firstOrFail();

            $questions = Question::where('part_id', $part->id)
                ->orderBy('id', 'DESC')->paginate(10);
            $questions_id = Question::where('part_id', $part->id)->pluck('id')->toArray();
            $reports = Report::whereIn('question_id', $questions_id)
                ->with('user')
                ->orderBy('created_at', 'DESC')->paginate(5);

            /*
             * all good.
             * save the result in database
             */
            DB::commit();
            return view('admin.profile-part')->with([
                'part' => $part,
                'questions' => $questions,
                'reports' => $reports,
            ]);
        } catch (\Exception $exception) {
            /*
             * database failed, rollback the data
             */
            die($exception);
            DB::rollback();
            Log::error('PartController-get-error - admin: ' .
                auth('admin')->user()->name . ' - part: ' . $id .
                ' exception: ' . $exception->getMessage() . ' -- Line: ' . $exception->getLine());
            $alerts = collect(['error' => __('alerts.load_error')]);
            return redirect()->back()->with(['alerts' => $alerts]);
        }
    }

    public function getItems()
    {
        // TODO: Implement getItems() method.
    }

    public function searchItems(Request $request)
    {
        // TODO: Implement searchItems() method.
    }

    public function getImage($image)
    {
        // TODO: Implement getImage() method.
    }

    public function deleteImage($id)
    {
        // TODO: Implement deleteImage() method.
    }
}
