<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateUserPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|numeric|min:1',
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|'.Rule::unique('users')->ignore($this->id),
            'melliCode' => 'required|numeric|regex:/^[0-9]+$/|'.Rule::unique('users')->ignore($this->id),
            'phone' => 'required|numeric|regex:/^[0-9]+$/|'.Rule::unique('users')->ignore($this->id),
        ];
    }
}
