<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SearchUserPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'nullable|string|max:255',
            'email' => 'nullable|string|email|max:255',
            'melliCode' => 'nullable|numeric',
            'phone' => 'nullable|string|max:50',
            'licence_no' => 'nullable|string|max:50',
            'group' => 'nullable|numeric|min:0',
            'major' => 'nullable|numeric|min:0',
            'province' => 'nullable|string|max:50',
            'area' => 'nullable|string|max:50',
            'address' => 'nullable|string|max:255',
            'activation_status' => 'nullable|boolean',
        ];
    }
}
