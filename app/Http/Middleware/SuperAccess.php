<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class SuperAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('admin')->user()->hasRole('super-admin')) {
            return $next($request);
        }
        $alerts = collect(['warning' => __('alerts.403_super_admin')]);
        return redirect()->route('admin')->with(['alerts' => $alerts]);
    }
}
