<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use NavidKhoshRouz\AdminRolePermission\models\Role;

class AdminAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role = null, $permission = null)
    {
        if (Auth::guard('admin')->check()) {
            //die(DB::table('admins_roles')->get() . ' -- ' . Auth::guard('admin')->user()->id);
            if (!Auth::guard('admin')->user()->hasRole('super-admin')) {
                if (!is_null($role))
                    if (!Auth::guard('admin')->user()->hasRole($role)) {
                        abort(403);
                    }

                if (!is_null($permission))
                    if (!Auth::guard('admin')->user()->hasPermissionTo($permission)) {
                        abort(403);
                    }
            }
            return $next($request);
        }

        return redirect('login/admin');
    }
}
