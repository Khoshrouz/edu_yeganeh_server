<?php

namespace App\Account\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    public $timestamps = true;

    protected $fillable = [
        'price',
        'duration',
        'chapter_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function chapter()
    {
        return $this->belongsTo(Chapter::class, 'chapter_id');
    }
}
