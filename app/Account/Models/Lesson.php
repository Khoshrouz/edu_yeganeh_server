<?php

namespace App\Account\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Lesson extends Model
{
    public $timestamps = true;

    protected $fillable = [
        'title',
        'available',
        'course_id',
    ];

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    public function course()
    {
        return $this->belongsTo(Course::class, 'course_id');
    }

    public function chapters()
    {
        return $this->hasMany(Chapter::class, 'lesson_id');
    }
}
