<?php

namespace App\Account\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    public $timestamps = true;

    protected $fillable = [
        'expires_at', 'content', 'messageable_id', 'messageable_type',
    ];

    public function messageable()
    {
        return $this->morphTo();
    }
}
