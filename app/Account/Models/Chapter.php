<?php

namespace App\Account\Models;

use Illuminate\Database\Eloquent\Model;

class Chapter extends Model
{
    public $timestamps = true;

    protected $fillable = [
        'title',
        'stars',
        'icon',
        'description',
        'downloads_count',
        'questions_count',
        'comprehensive_questions_count',
        'available',
        'lesson_id'
    ];

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function purchases()
    {
        return $this->hasMany(Purchase::class, 'chapter_id');
    }

    public function packages()
    {
        return $this->hasMany(Package::class, 'chapter_id');
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function parts()
    {
        return $this->hasMany(Part::class, 'chapter_id');
    }

    public function favorites()
    {
        return $this->hasMany(Favorite::class, 'chapter_id');
    }

    public function lesson()
    {
        return $this->belongsTo(Lesson::class, 'lesson_id');
    }


}
