<?php

namespace App\Account\Models;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    public $timestamps = true;

    protected $fillable = [
        'content',
        'notable_id',
        'notable_type',
        'user_id',
    ];

    public function user()
    {
        $this->belongsTo(User::class, 'user_id');
    }

    public function notable()
    {
        return $this->morphTo();
    }
}
