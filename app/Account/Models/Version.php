<?php

namespace App\Account\Models;

use Illuminate\Database\Eloquent\Model;

class Version extends Model
{
    public $timestamps = true;

    protected $fillable = [
        'device',
        'major',
        'minor',
        'patch',
    ];
}
