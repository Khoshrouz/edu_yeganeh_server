<?php

namespace App\Account\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use NavidKhoshRouz\AdminRolePermission\traits\HasPermissionsTrait;

class Admin extends Authenticatable
{
    use Notifiable, HasPermissionsTrait;

    protected $guard = 'admin';
    public $timestamps = true;

    protected $fillable = [
        'name', 'email', 'password', 'active'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    protected static function boot()
    {
        parent::boot();

        /*static::deleted(function ($admin){
            $admin->mails()->delete();
        });*/
    }
}
