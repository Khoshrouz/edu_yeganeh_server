<?php

namespace App\Account\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    public $timestamps = true;

    protected $fillable = [
        'user_id',
        'question_id',
        'part_id',
        'chapter_id',
        'lesson_id'
    ];

    public function user()
    {
        $this->belongsTo(User::class, 'user_id');
    }

    public function part()
    {
        $this->belongsTo(Part::class, 'part_id');
    }

    public function question()
    {
        $this->belongsTo(Question::class, 'question_id');
    }

    public function chapter()
    {
        $this->belongsTo(Chapter::class, 'chapter_id');
    }

    public function lesson()
    {
        $this->belongsTo(Lesson::class, 'lesson_id');
    }
}
