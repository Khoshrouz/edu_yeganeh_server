<?php

namespace App\Account\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    public $timestamps = true;

    protected $fillable = [
        'title',
        'available',
    ];

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    public function lessons()
    {
        return $this->hasMany(Lesson::class, 'course_id');
    }
}
