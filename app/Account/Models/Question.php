<?php

namespace App\Account\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    public $timestamps = true;

    protected $fillable = [
        'question_image',
        'question_content',
        'first_option',
        'second_option',
        'third_option',
        'fourth_option',
        'answer',
        'answer_content',
        'answer_image',
        'part_id',
        'available',
    ];

    public function part()
    {
        return $this->belongsTo(Part::class, 'part_id');
    }

    public function favorites()
    {
        return $this->hasMany(Favorite::class, "question_id");
    }


}
