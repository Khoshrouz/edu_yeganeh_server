<?php

namespace App\Account\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    public $timestamps = true;

    protected $fillable = [
        'content',
        'report_option_id',
        'question_id',
        'user_id',
        'confirm',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function question()
    {
        return $this->belongsTo(Question::class, 'question_id');
    }

    public function report_option()
    {
        return $this->belongsTo(ReportOption::class, 'report_option_id');
    }
}
