<?php

namespace App\Account\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Image extends Model
{
    //
    public $timestamps = true;

    protected $fillable = [
        'link', 'available', 'imageable_id', 'imageable_type',
    ];

    public function imageble()
    {
        return $this->morphTo();
    }

    protected static function boot()
    {
        parent::boot();

        static::deleted(function ($image) {
            Storage::delete($image->imageable_type . '/' . $image->link);
        });
    }


}
