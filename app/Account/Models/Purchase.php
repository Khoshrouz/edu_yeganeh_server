<?php

namespace App\Account\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    public $timestamps = true;

    protected $fillable = [
        'user_id',
        'expires_at',
        'chapter_id',
        'transaction_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function chapter()
    {
        return $this->belongsTo(Chapter::class, 'chapter_id');
    }

    public function transaction()
    {
        return $this->belongsTo(Transaction::class, 'transaction_id');
    }
}
