<?php

namespace App\Account\Models;

use Illuminate\Database\Eloquent\Model;

class ReportOption extends Model
{
    public $timestamps = true;

    protected $fillable = [
        'title',
    ];
}
