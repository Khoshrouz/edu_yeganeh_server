<?php

namespace App\Account\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public $timestamps = true;

    protected $fillable = [
        'user_id',
        'bank_data',
        'status',
    ];

    public function user()
    {
        return $this->belongsTo(User::class , 'user_id');
    }

    public function purchase()
    {
        return $this->hasOne(Purchase::class , 'transaction_id');
    }
}
