<?php

namespace App\Account\Models;

use Illuminate\Database\Eloquent\Model;

class Part extends Model
{
    public $timestamps = true;

    protected $fillable = [
        'title',
        'available',
        'chapter_id'
    ];

    public function chapter()
    {
        return $this->belongsTo(Chapter::class, 'chapter_id');
    }

    public function questions()
    {
        return $this->hasMany(Question::class, 'part_id');
    }
}
