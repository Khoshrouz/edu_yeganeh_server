<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::prefix('v1/')->group(function () {
    Route::post('/register', 'Api\AuthController@register');
    Route::post('/login', 'Api\AuthController@login');
    Route::post('/authentication', 'Api\AuthController@authentication');
    Route::get('/courses', 'Api\ApiController@getCourses');
    Route::get('/lesson/{id}', 'Api\ApiController@getLesson');
    Route::get('/chapter/{id}', 'Api\ApiController@getChapter');
    Route::get('/image/{image}/', 'Api\ApiController@getImage');
    Route::get('/icon/{icon}/', 'Api\ApiController@getChapterIcon');
    // Route::get('/lesson/{lesson_id}/chapters','Api\ApiController@getChapters');
    Route::get('/chapter/{id}/comments', 'Api\ApiController@getFreeComments');
});

Route::prefix('v1/auth/')->middleware(['auth:api'])->group(function () {
    Route::get('/logout', 'Api\AuthController@logout');
    Route::get('/profile', 'Api\ApiController@getProfile');
    Route::get('/profile/comments', 'Api\ApiController@getProfileComments');
    Route::get('/profile/transactions', 'Api\ApiController@getProfileTransactions');
    Route::get('/chapter/{id}', 'Api\ApiController@getChapter');
    Route::get('/image/user/{filename}', 'Api\ApiController@displayUserImage');
    Route::post('/edit/user/token', 'Api\AuthController@updateFcmToken');

    /*
    * Reports
    */
    Route::get('/reports/options', 'Api\ApiController@getReportOptions');
    Route::get('/profile/reports', 'Api\ApiController@getProfileReports');
    Route::post('/report', 'Api\ApiController@setReport');

    /*
    * Notes
    */
    Route::get('/chapter/{id}/note', 'Api\ApiController@getNote');
    Route::post('/chapter/note', 'Api\ApiController@setNote');

    /*
     * Lessons
     */
    Route::get('/lesson/{id}', 'Api\ApiController@getLesson');

    /*
     * Courses / chapters / parts / questions
     */

    Route::get('/chapter/{chapter_id}/parts', 'Api\ApiController@getParts');
    Route::post('/questions', 'Api\ApiController@getQuestions');
    Route::get('/chapter/{chapter_id}/questions', 'Api\ApiController@getChapterQuestions');
    Route::post('/favorite', 'Api\ApiController@setFavorite');
    Route::get('/chapter/{chapter_id}/favorites', 'Api\ApiController@getFavorites');
    Route::get('/lesson/{lesson_id}/favorites', 'Api\ApiController@getLessonFavorites');

    // comment part
    Route::post('/comment', 'Api\ApiController@postComment');
    Route::get('/chapter/{id}/comments', 'Api\ApiController@getComments');

    // purchase a chapter
    Route::get('/chapter/{id}/purchase/{package_id}', 'Api\ApiController@purchaseChapter');
    Route::delete('/chapter/{id}/purchase', 'Api\ApiController@deletePurchase');
    Route::delete('/favorite/question/{id}/delete', 'Api\ApiController@deleteFavorite');

    // question-image
    Route::get('/question/{image}/', 'Api\ApiController@getQuestionImage');
});


Route::get('v1/version/{status}', 'Api\VersionController@getStatus');
//Route::get('/index','Api\VersionController@index');
