<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});


/*Auth::routes([
    'register' => false, // Registration Routes...
    'reset' => false, // Password Reset Routes...
    'verify' => false, // Email Verification Routes...
]);*/

Route::get('lang/{lang}', ['as' => 'lang.switch', 'uses' => 'LanguageController@switchLang']);

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/login/admin', 'Auth\LoginController@showAdminLoginForm')->name('admin.login');
//Route::get('/login/writer', 'Auth\LoginController@showWriterLoginForm');
//Route::get('/register/admin', 'Auth\RegisterController@showAdminRegisterForm');
//Route::get('/register/writer', 'Auth\RegisterController@showWriterRegisterForm');

Route::post('/login/admin', 'Auth\LoginController@adminLogin');
//Route::post('/login/writer', 'Auth\LoginController@writerLogin');
//Route::post('/register/admin', 'Auth\RegisterController@createAdmin');
//Route::post('/register/writer', 'Auth\RegisterController@createWriter');

Route::view('/home', 'home')->middleware('auth');

// *********** admin links ***********************
//crud actions with super access limitation
Route::prefix('/admin')->middleware(['super.access'])->group(function () {
    Route::get('/db/delete', 'AdminController@deleteDB');

    /*
     * Admin section
     */
    Route::get('/admin/new', 'AdminController@createAdminIndex')->name('admin.create-admin-index');
    Route::post('/admin/delete', 'AdminController@deleteAdmin')->name('admin.delete-admin');
    Route::post('/admin/create/image', 'AdminController@createAdminImage')->name('admin.create-admin-image');
    Route::post('/admin/create', 'AdminController@createAdmin')->name('admin.create-admin');
    Route::post('/admin/edit', 'AdminController@updateAdmin')->name('admin.update-admin');
    Route::get('/admin/{id}/activity', 'AdminController@switchActivity')->name('admin.update-admin-activation');
    Route::post('/admin/edit/password', 'AdminController@updateAdminPassword')->name('admin.update-admin-password');

    /*
     * User section
     */
    Route::get('/user/new', 'UserController@createUserIndex')->name('admin.create-user-index');
    Route::post('/user/create/image', 'UserController@createUserImage')->name('admin.create-user-image');
    Route::post('/user/create', 'UserController@createUser')->name('admin.create-user');
    Route::post('/user/edit', 'UserController@updateUser')->name('admin.update-user');
    Route::get('/user/{id}/activity', 'UserController@switchActivity')->name('admin.update-user-activation');
    Route::post('/user/delete', 'UserController@deleteUser')->name('admin.delete-user');

    /*
     * Lesson section
     */
    Route::get('/chapter/{lesson_id}/create', 'Panel\ChapterController@createIndex')->name('admin.create-chapter-index');
    Route::post('/chapter/create', 'Panel\ChapterController@create')->name('admin.create-chapter');
    Route::get('/chapter/{id}/delete', 'Panel\ChapterController@delete')->name('admin.delete-chapter');
    Route::post('/chapter/update', 'Panel\ChapterController@update')->name('admin.update-chapter');
    Route::get('/chapter/{id}/update', 'Panel\ChapterController@updateIndex')->name('admin.update-chapter-index');

    /*
     * Chapter section
     */
    Route::get('/lesson/create', 'Panel\LessonController@createIndex')->name('admin.create-lesson-index');
    Route::post('/lesson/create', 'Panel\LessonController@create')->name('admin.create-lesson');
    Route::get('/lesson/{id}/delete', 'Panel\LessonController@delete')->name('admin.delete-lesson');
    Route::post('/lesson/update', 'Panel\LessonController@update')->name('admin.update-lesson');
    Route::get('/lesson/{id}/update', 'Panel\LessonController@updateIndex')->name('admin.update-lesson-index');

    /*
     * Part section
     */
    Route::get('/part/{chapter_id}/create', 'Panel\PartController@createIndex')->name('admin.create-part-index');
    Route::post('/part/create', 'Panel\PartController@create')->name('admin.create-part');
    Route::get('/part/{id}/delete', 'Panel\PartController@delete')->name('admin.delete-part');
    Route::post('/part/update', 'Panel\PartController@update')->name('admin.update-part');
    Route::get('/part/{id}/update', 'Panel\PartController@updateIndex')->name('admin.update-part-index');


    /*
     * Question section
     */
    Route::get('/question/{part_id}/create', 'Panel\QuestionController@createIndex')->name('admin.create-question-index');
    Route::post('/question/create', 'Panel\QuestionController@create')->name('admin.create-question');
    Route::get('/question/{id}/delete', 'Panel\QuestionController@delete')->name('admin.delete-question');
    Route::post('/question/update', 'Panel\QuestionController@update')->name('admin.update-question');
    Route::get('/question/{id}/update', 'Panel\QuestionController@updateIndex')->name('admin.update-question-index');


    /*
     * Comment section
     */
    Route::get('/comment/{id}/confirm', 'Panel\CommentController@confirm')->name('admin.comment-confirm');
    Route::get('/comment/{id}/delete', 'Panel\CommentController@delete')->name('admin.comment-delete');


    /*
     * Report section
     */
    Route::get('/report/{id}/confirm', 'Panel\ReportController@confirm')->name('admin.report-confirm');


    /*
     * Question import
     */
    //import_excel
    Route::get('/question/import', 'Panel\QuestionController@importQuestionIndex')->name('admin.import-question-index');
    Route::post('/question/import', 'Panel\QuestionController@importQuestion')->name('admin.import-question');

});

Route::prefix('/admin')->group(function () {
    /*
     * Admin section
     */
    Route::get('', 'AdminController@index')->name('admin');
    Route::get('/admin/{id}', 'AdminController@getAdmin')->name('admin.profile-admin');
    Route::get('admin/image/{filename}', 'AdminController@displayImage')->name('admin-image');
    Route::get('/admins', 'AdminController@getAdmins')->name('admin.admins');
    Route::post('/admins', 'AdminController@searchAdmin')->name('admin.search-admins');

    /*
     * User section
     */
    //Route::get('/mails/user/{id}', 'UserController@getUserMails')->name('admin.user-mails');
    Route::get('/user/{id}', 'UserController@getUser')->name('admin.profile-user');
    Route::get('user/image/{filename}', 'UserController@displayImage')->name('user-image');
    Route::get('/users', 'UserController@getUsers')->name('admin.users');
    Route::post('/users', 'UserController@searchUser')->name('admin.search-users');

    /*
     * Lesson section
     */
    Route::get('/chapter/{id}', 'Panel\ChapterController@get')->name('admin.profile-chapter');
    //Route::get('/chapters', 'Panel\ChapterController@getItems')->name('admin.chapters');
    Route::post('/chapters', 'Panel\ChapterController@searchItems')->name('admin.search-chapters');
    Route::get('/chapters/image/{image}', 'Panel\ChapterController@getImage')->name('chapter-image');
    Route::get('/chapter/{id}/image/delete', 'Panel\ChapterController@deleteImage')->name('chapter-delete-image');

    /*
     * Chapter section
     */
    Route::get('/lesson/{id}', 'Panel\LessonController@get')->name('admin.profile-lesson');
    Route::get('/lessons', 'Panel\LessonController@getItems')->name('admin.lessons');
    Route::post('/lessons', 'Panel\LessonController@searchItems')->name('admin.search-lessons');
    Route::get('/lessons/image/{image}', 'Panel\LessonController@getImage')->name('lesson-image');
    Route::get('/lesson/{id}/image/delete', 'Panel\LessonController@deleteImage')->name('delete-lesson-image');

    /*
     * Part section
     */
    Route::get('/parts/{id}', 'Panel\PartController@get')->name('admin.profile-part');
    Route::post('/parts', 'Panel\PartController@searchItems')->name('admin.search-parts');
    Route::get('/parts/image/{image}', 'Panel\PartController@getImage')->name('part-image');

    /*
    * Question section
    */
    Route::get('/questions/{id}', 'Panel\QuestionController@get')->name('admin.profile-question');
    Route::post('/questions', 'Panel\QuestionController@searchItems')->name('admin.search-questions');
    Route::get('/questions/image/{image}', 'Panel\QuestionController@getImage')->name('question-image');
    Route::get('/questions/{id}/question_image/delete', 'Panel\QuestionController@deleteImage')->name('admin.delete-question-image');
    Route::get('/questions/{id}/answer_image/delete', 'Panel\QuestionController@deleteAnswerImage')->name('admin.delete-question-answer-image');

    /*
     * reports
     */
    Route::get('/reports', 'Panel\ReportController@getItems')->name('admin.reports');

    /*
     * comments
     */
    Route::get('/comments', 'Panel\CommentController@getItems')->name('admin.comments');

    /*
     * comments
     */
    Route::get('/transactions', 'Panel\TransactionController@getItems')->name('admin.transactions');

});
//************ logout links for admins ************

// *********** writer links ***********************
//Route::view('/writer', 'writer')->middleware('writer.auth')->name('writer');
//************ logout links for writers ************

Route::post('/admin/logout', function () {
    if (Auth::guard('admin')->check())
        Auth::guard('admin')->logout();
    return redirect()->intended('');
})->name('admin-logout');

/*Route::post('/writer/logout', function () {
    if (Auth::guard('writer')->check())
        Auth::guard('writer')->logout();
    return redirect()->intended('');
})->name('writer-logout');*/

//************ end of logout part ************

