<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('price');
            $table->integer('duration')->default(6); // 6,12 or 24 months
            $table->bigInteger('chapter_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('packages', function (Blueprint $table) {
            $table->foreign('chapter_id')->references('id')
                ->on('chapters')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
