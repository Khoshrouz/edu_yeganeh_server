<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('question_image')->nullable();
            $table->text('question_content');

            $table->text('first_option');
            $table->text('second_option');
            $table->text('third_option');
            $table->text('fourth_option');

            $table->integer('answer')->default(1);
            $table->text('answer_content');
            $table->string('answer_image')->nullable();

            $table->bigInteger('part_id')->unsigned();

            $table->boolean('available')->default(true);
            $table->timestamps();
        });


        Schema::table('questions', function (Blueprint $table) {
            $table->foreign('part_id')->references('id')
                ->on('parts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
