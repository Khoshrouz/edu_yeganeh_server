<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->text('content');

            $table->bigInteger('report_option_id')->unsigned();

            $table->bigInteger('question_id')->unsigned();

            $table->bigInteger('user_id')->unsigned();

            $table->boolean('confirm')->default(false);


            $table->timestamps();
        });

        Schema::table('reports', function (Blueprint $table) {
            $table->foreign('report_option_id')->references('id')
                ->on('report_options')->onDelete('cascade');

            $table->foreign('question_id')->references('id')
                ->on('questions')->onDelete('cascade');

            $table->foreign('user_id')->references('id')
                ->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
