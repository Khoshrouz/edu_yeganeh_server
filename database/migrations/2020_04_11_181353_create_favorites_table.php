<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFavoritesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('favorites', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('question_id')->unsigned()->nullable();
            $table->bigInteger('part_id')->unsigned();
            $table->bigInteger('chapter_id')->unsigned();
            $table->bigInteger('lesson_id')->unsigned();

            $table->timestamps();
        });

        Schema::table('favorites', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')
                ->on('users')->onDelete('cascade');

            $table->foreign('part_id')->references('id')
                ->on('parts')->onDelete('cascade');

            $table->foreign('chapter_id')->references('id')
                ->on('chapters')->onDelete('cascade');

            $table->foreign('lesson_id')->references('id')
                ->on('lessons')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('favorites');
    }
}
