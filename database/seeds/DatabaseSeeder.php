<?php

use App\Account\Models\Version;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('users')->insert([
            'name' => 'نوید خوش روز',
            'email' => 'navid.khoshoruz@gmail.com',
            'phone' => '09334112809',
            'melliCode' => '2710149826',
            'university_entrance' => '90',
            'password' => Hash::make('password'),
        ]);

        DB::table('users')->insert([
            'name' => 'ماکان شعبانی',
            'email' => 'm.shabani@gmail.com',
            'phone' => '09370472468',
            'melliCode' => '1440639060',
            'university_entrance' => '91',
            'password' => Hash::make('password'),
        ]);



        factory(Version::class, 6)->create();
        $this->call([
            RolesPermissionsSeeder::class,
            CoursesSeeder::class
        ]);

        Model::reguard();
    }
}
