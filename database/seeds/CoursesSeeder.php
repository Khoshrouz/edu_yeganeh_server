<?php

use App\Account\Models\Chapter;
use App\Account\Models\Comment;
use App\Account\Models\Course;
use App\Account\Models\Image;
use App\Account\Models\Lesson;
use App\Account\Models\Part;
use App\Account\Models\Question;
use App\Account\Models\ReportOption;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class CoursesSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        //        $medical = Course::create([
        //            'title' => "پزشکی"
        //        ]);
        //        Image::create([
        //            'link' => 'medical.jpg',
        //            'imageable_id' => $medical->id,
        //            'imageable_type' => 'courses',
        //        ]);
        //
        //        $dental = Course::create([
        //            'title' => "داندان پزشکی"
        //        ]);
        //        Image::create([
        //            'link' => 'dental.jpg',
        //            'imageable_id' => $dental->id,
        //            'imageable_type' => 'courses',
        //        ]);
        //
        //        factory(Image::class, 4)->create();
        //
        //        factory(Lesson::class, 4)->create();

        // factory(User::class, 200)->create()->each(function ($user) {
        //     $user->comments()->saveMany(factory(Comment::class, 1)->make());
        // });

        //        factory(Chapter::class, 40)->create();
        //        factory(Part::class, 200)->create()->each(function ($part) {
        //            $part->questions()->saveMany(factory(Question::class, 10)->make());
        //        });


        /*
         * creating the options and reports
         */
        ReportOption::create([
            'title' => 'اشتباه تایپی'
        ]);
        ReportOption::create([
            'title' => 'ایراد علمی'
        ]);
        ReportOption::create([
            'title' => 'ایراد در نرم افزار'
        ]);


        Model::reguard();
    }
}
