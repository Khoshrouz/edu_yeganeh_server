<?php

use App\Account\Models\Admin;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use NavidKhoshRouz\AdminRolePermission\models\Permission;
use NavidKhoshRouz\AdminRolePermission\models\Role;

class RolesPermissionsSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $super_admin_permission = Permission::where('slug', 'crud')->first();
        $normal_admin_permission = Permission::where('slug', 'ru')->first();

        //RoleTableSeeder.php
        $super_admin_role = new Role();
        $super_admin_role->slug = 'super-admin';
        $super_admin_role->name = 'Full access admin';
        $super_admin_role->save();
        $super_admin_role->permissions()->attach($super_admin_permission);

        $normal_admin_role = new Role();
        $normal_admin_role->slug = 'normal-admin';
        $normal_admin_role->name = 'Normal access admin';
        $normal_admin_role->save();
        $normal_admin_role->permissions()->attach($normal_admin_permission);

        $super_admin_role = Role::where('slug', 'super-admin')->first();
        $normal_admin_role = Role::where('slug', 'normal-admin')->first();

        $fullPermission = new Permission();
        $fullPermission->slug = 'crud';
        $fullPermission->name = 'Crate Read Update Delete';
        $fullPermission->save();
        $fullPermission->roles()->attach($super_admin_role);

        $normalPermission = new Permission();
        $normalPermission->slug = 'ru';
        $normalPermission->name = 'Read Update';
        $normalPermission->save();
        $normalPermission->roles()->attach($normal_admin_role);

        $super_role = Role::where('slug', 'super-admin')->first();
        $normal_role = Role::where('slug', 'normal-admin')->first();
        $super_perm = Permission::where('slug', 'crud')->first();
        $normal_perm = Permission::where('slug', 'ru')->first();

        $developer = new Admin();
        $developer->name = 'نوید خوش روز';
        $developer->email = 'navid@gmail.com';
        $developer->password = bcrypt('123456789');
        $developer->save();
        $developer->roles()->attach($super_role);
        $developer->permissions()->attach($super_perm);


        $developer = new Admin();
        $developer->name = 'مدیر کل';
        $developer->email = 'bms@gmail.com';
        $developer->password = bcrypt('123456789');
        $developer->save();
        $developer->roles()->attach($super_role);
        $developer->permissions()->attach($super_perm);

        // $developer = new Admin();
        // $developer->name = 'کریم محمدی';
        // $developer->email = 'karim@gmail.com';
        // $developer->password = bcrypt('123456789');
        // $developer->save();
        // $developer->roles()->attach($normal_role);
        // $developer->permissions()->attach($normal_perm);



        Model::reguard();
    }
}
