<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Account\Models\Purchase;
use Faker\Generator as EnFaker;

/*
|--------------------------------------------------------------------------
| Models Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Purchase::class, function (EnFaker $faker) {

    $rnd = $faker->unique()->numberBetween(1, 40);
    return [
        'chapter_id' => $rnd,
        'user_id' => $faker->numberBetween(1, 18),
    ];
});
