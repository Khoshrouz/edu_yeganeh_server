<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Account\Models\Comment;
use Faker\Generator as EnFaker;
use Ybazli\Faker\Facades\Faker as Faker;

/*
|--------------------------------------------------------------------------
| Models Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Comment::class, function (EnFaker $faker) {
    return [
        'content' => Faker::sentence(),
        'stars' => $faker->randomFloat(2, 1, 5),
        'commentable_id' => $faker->numberBetween(1, 8),
        'commentable_type' => 'chapters',
        'confirm' => $faker->numberBetween(0, 1),
    ];
});
