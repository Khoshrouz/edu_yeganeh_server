<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Account\Models\Chapter;
use App\User;
use Faker\Generator as EnFaker;
use Ybazli\Faker\Facades\Faker as Faker;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Models Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Chapter::class, function (EnFaker $faker) {
    return [
        'title' => Faker::word(),
        'icon' => $faker->numberBetween(30,160).'.jpg',
        'description' => Faker::paragraph(),
        'price' => $faker->unique()->numberBetween(250, 4200) * 10,
        'lesson_id' => $faker->numberBetween(1,4),
    ];
});
