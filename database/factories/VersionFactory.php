<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Account\Models\Version;
use Faker\Generator as EnFaker;

$factory->define(Version::class, function (EnFaker $faker) {
    return [
        'device' => $faker->randomElement([
            'android',
            'ios',
        ]),
        'major' => 1,
        'minor' => rand(0,10),
        'patch' => rand(0,10),
    ];

});
