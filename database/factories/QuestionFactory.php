<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as EnFaker;
use Ybazli\Faker\Facades\Faker;

/*
|--------------------------------------------------------------------------
| Models Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Account\Models\Question::class, function (EnFaker $faker) {
    return [
        'question_content' => Faker::sentence(),
        'first_option' => Faker::sentence(),
        'second_option' => Faker::sentence(),
        'third_option' => Faker::sentence(),
        'fourth_option' => Faker::sentence(),
        'answer' => $faker->numberBetween(1,4),
        'answer_content' => Faker::paragraph(),
    ];
});
