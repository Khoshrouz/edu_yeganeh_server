<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as EnFaker;
use Ybazli\Faker\Facades\Faker as Faker;
use Illuminate\Support\Facades\Hash;

/*
|--------------------------------------------------------------------------
| Models Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (EnFaker $faker) {
    return [
        'name' => Faker::fullName(),
        'phone' => "0" . $faker->unique()->numberBetween(9101111111, 9999999999),
        'melliCode' => $faker->unique()->numberBetween(1000000000, 9999999999),
        'university_entrance' => $faker->numberBetween(80, 99),
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => Hash::make('password'), // password
    ];
});
