<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Account\Models\Report;
use Faker\Generator as EnFaker;
use Ybazli\Faker\Facades\Faker as Faker;

/*
|--------------------------------------------------------------------------
| Models Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Report::class, function (EnFaker $faker) {
    return [
        'content' => Faker::sentence(),
        'report_option_id' => $faker->numberBetween(1, 3),
        'question_id' => $faker->unique()->numberBetween(100, 300),
        'user_id' => $faker->unique()->numberBetween(1, 20),
        'confirm' => $faker->numberBetween(0, 1),
    ];
});
