<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Account\Models\Transaction;
use Faker\Generator as EnFaker;

$factory->define(Transaction::class, function (EnFaker $faker) {
    return [
        'status' => $faker->randomElement([
            'pending', 'paid', 'confirmed'
        ]),
        'bank_data' => "data from bank",
        'user_id' => $faker->numberBetween(1, 100),
    ];

});
