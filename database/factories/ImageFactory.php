<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Account\Models\Image;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Image::class, function (Faker $faker) {
    return [
        'link' => $faker->unique()->numberBetween(10000, 9000) . '.jpg',

        'imageable_type' => 'lessons',
        'imageable_id' => $faker->unique()->numberBetween(1, 28)
    ];
});
