<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Account\Models\Part;
use Faker\Generator as EnFaker;
use Ybazli\Faker\Facades\Faker as Faker;

/*
|--------------------------------------------------------------------------
| Models Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Part::class, function (EnFaker $faker) {
    return [
        'title' => Faker::word(),
        'chapter_id' => $faker->numberBetween(1, 40),
    ];
});
