<?php

use Illuminate\Support\Facades\Route;

Route::get('testing-new-package', function () {
    return '<h1>Hello world</h1>';
});

Route::middleware(['role:super-admin', 'auth'])->group(function () {

    Route::get('admin/super', function () {
        return 'Welcome Admin';
    });

});

