<?php

namespace NavidKhoshRouz\AdminRolePermission\middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */

    public function handle($request, Closure $next, $role, $permission = null)
    {
        die(Auth::guard('admin')->user() . ' 55');
        if (Auth::guard('admin')->check()) {
            die('asasd');
        }

        if (!Auth::guard('admin')->user()->hasRole($role)) {
            abort(404);
        }

        if ($permission !== null && !Auth::guard('admin')->user()->can($permission)) {
            abort(404);
        }

        return $next($request);

    }
}
