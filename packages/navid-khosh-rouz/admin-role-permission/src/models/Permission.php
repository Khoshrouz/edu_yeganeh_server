<?php

namespace NavidKhoshRouz\AdminRolePermission\models;

use App\Account\Models\Admin;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{

    public $timestamps = true;

    protected $fillable = [
        'name', 'slug'
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'roles_permissions');
    }

    public function admins()
    {
        return $this->belongsToMany(Admin::class, 'admins_roles');
    }

}