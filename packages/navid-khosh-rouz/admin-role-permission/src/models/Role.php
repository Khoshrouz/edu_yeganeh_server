<?php

namespace NavidKhoshRouz\AdminRolePermission\models;

use App\Account\Models\Admin;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{

    public $timestamps = true;

    protected $fillable = [
        'name', 'slug'
    ];

    public function permissions() {

        return $this->belongsToMany(Permission::class,'roles_permissions');

    }

    public function admins() {

        return $this->belongsToMany(Permission::class,'admins_roles');

    }

}