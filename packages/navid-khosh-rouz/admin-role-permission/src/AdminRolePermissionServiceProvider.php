<?php

namespace NavidKhoshRouz\AdminRolePermission;

use App\Http\Kernel;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use NavidKhoshRouz\AdminRolePermission\models\Permission;

class AdminRolePermissionServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(Kernel $kernel)
    {
        if ($this->app->runningInConsole()) {
            $this->publishResources();
        }

        $this->loadRoutesFrom(__DIR__ . '/routes/routes.php');
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');

        try {
            Permission::get()->map(function ($permission) {
                Gate::define($permission->slug, function ($admin) use ($permission) {
                    return $admin->hasPermissionTo($permission);
                });
            });
        } catch (\Exception $e) {
            report($e);
            return false;
        }

        //Blade directives
        Blade::directive('role', function ($role) {
            return "if(auth()->check() && auth()->admin()->hasRole({$role})) :"; //return this if statement inside php tag
        });

        Blade::directive('endrole', function ($role) {
            return "endif;"; //return this endif statement inside php tag
        });

    }


    protected function publishResources()
    {
        $this->publishes([
            __DIR__ . '/database/seeds/RolesPermissionsSeeder.php' => database_path('seeds/RolesPermissionsSeeder.php'),
        ], 'roles-permissions-seeds');
    }
}
