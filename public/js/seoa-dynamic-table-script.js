$(document).ready(function () {
    var counter = 1;

    $("#addrow").on("click", function () {
        var newRow = $("<tr>");
        var cols = "";

        cols += '<td><select class="custom-select mr-sm-2" onchange="fetchMajors(' + counter + ')" name="group_' + counter + '" id="group_' + counter + '" class="form-control"></select></td>';
        cols += '<td><select class="custom-select mr-sm-2" onchange="fetchUsers(' + counter + ')" name="major_' + counter + '" id="major_' + counter + '" class="form-control"></select></td>';
        cols += '<td><select class="custom-select mr-sm-2" name="user_' + counter + '" id="user_' + counter + '" class="form-control"></select></td>';

        cols += '<td><input type="button" class="ibtnDel btn btn-md btn-danger "  value="خذف"></td>';
        newRow.append(cols);
        $("table.order-list").append(newRow);
        fetchGroups(counter);
        fetchMajors(counter);
        fetchUsers(counter);
        counter++;
    });


    $("table.order-list").on("click", ".ibtnDel", function (event) {
        $(this).closest("tr").remove();
        counter -= 1
    });


});


function calculateRow(row) {
    var price = +row.find('input[name^="price"]').val();

}

function calculateGrandTotal() {
    var grandTotal = 0;
    $("table.order-list").find('input[name^="price"]').each(function () {
        grandTotal += +$(this).val();
    });
    $("#grandtotal").text(grandTotal.toFixed(2));
}