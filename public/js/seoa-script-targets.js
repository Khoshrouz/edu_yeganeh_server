jQuery(document).ready(function ($) {

    /*    event for the target section    */

    $('div[id=reset_targets]').on('click', function () {
        var item = $('h6[id=selected_names]');
        item.empty();
    });

    $('div[id=add_targets]').on('click', function () {
        var selected = $('select[name=major]').val();
        if (selected > 0) {
            console.log('adding targets... major-id(' + selected + ')');
            $.ajax({
                url: window.location.origin + '/admin/mail/major/' + selected,
                type: 'GET',
                dataType: 'json',
                success: function (data) {
                    console.log('received true, target added successfully');
                    //major
                    var selected_majors = $('h6[id=selected_majors]');
                    selected_majors.html(selected_majors.html() + " ( " + data['major'] + " ) ");
                    //names
                    var selected_names = $('h6[id=selected_names]');
                    $.each(data['names'], function (key, value) {
                        selected_names.html(selected_names.html() + " ( " + key + " ) ");
                    });
                },
                error: function (error) {
                    console.log("Error:");
                    console.log(error);
                }
            })
        } else {
            selected = $('select[name=group]').val();
            console.log('adding targets... group-id(' + selected + ')');
            $.ajax({
                url: window.location.origin + '/admin/mail/group/' + selected,
                type: 'GET',
                dataType: 'json',
                success: function (data) {
                    console.log('received true, target added successfully');
                    //group
                    var selected_groups = $('h6[id=selected_groups]');
                    selected_groups.html(selected_groups.html() + " ( " + data['group'] + " ) ");
                    //majors
                    var selected_majors = $('h6[id=selected_majors]');
                    $.each(data['majors'], function (key, value) {
                        selected_majors.html(selected_majors.html() + " ( " + value + " ) ");
                    });
                    //names
                    var selected_names = $('h6[id=selected_names]');
                    $.each(data['names'], function (key, value) {
                        selected_names.html(selected_names.html() + " ( " + key + " ) ");
                    });
                },
                error: function (error) {
                    console.log("Error:");
                    console.log(error);
                }
            })
        }
    });


});