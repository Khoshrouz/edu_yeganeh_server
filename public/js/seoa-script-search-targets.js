function fetchGroups(counter) {
    var url = '/admin/mail/groups';

    console.log('adding targets... groups');
    $.ajax({
        url: window.location.origin + url,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            console.log('received true, target added successfully');
            var select = $('select[id=group_' + counter + ']');
            select.empty();
            select.append('<option value="0">فرقی ندارد</option>');
            if (data['groups'] != null) {
                $.each(data['groups'], function (key, value) {
                    select.append('<option value=' + value + '>' + key + '</option>');
                });
            }
            //select.multiSelect('refresh');
        },
        error: function (error) {
            console.log("Error:");
            console.log(error);
        }
    })
}

function fetchMajors(counter) {
    var select_group = $('select[id=group_' + counter + ']');
    var selected = select_group.find(":selected").attr('value');
    var select = $('select[id=major_' + counter + ']');
    select.empty();
    select.append('<option value="0">فرقی ندارد</option>');
    if (selected > 0) {
        $.ajax({
            url: window.location.origin + '/admin/group/' + selected + '/majors',
            type: 'GET',
            dataType: 'json',

        }).done(function (data) {
            $.each(data, function (key, value) {
                select.append('<option value=' + value.id + '>' + value.title + '</option>');
            });
            fetchUsers(counter);
        })
    } else {
        alert('در این صورت, این اطلاعیه به تمامی کابران ارسال خواهد شد!')
    }
}

function fetchUsers(counter) {
    var select = $('select[id=major_' + counter + ']');
    var selected = select.find(":selected").attr('value');
    var url = '/admin/mail/major/';
    if (selected == 0) {
        selected = $('select[id=group_' + counter + ']').val();
        url = '/admin/mail/group/';
    }

    console.log('adding targets... major-id(' + selected + ')');
    $.ajax({
        url: window.location.origin + url + selected,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            console.log('received true, target added successfully');
            var select = $('select[id=user_' + counter + ']');
            select.empty();
            select.append('<option value="0">فرقی ندارد</option>');
            if (data['names'] != null) {
                $.each(data['names'], function (key, value) {
                    select.append('<option value=' + value + '>' + key + '</option>');
                });
            }
            //select.multiSelect('refresh');
        },
        error: function (error) {
            console.log("Error:");
            console.log(error);
        }
    })
}


/*
$('#public-methods').multiSelect();
$('#select-all').click(function () {
    $('#public-methods').multiSelect('select_all');
    return false;
});
$('#deselect-all').click(function () {
    $('#public-methods').multiSelect('deselect_all');
    return false;
});
$('#refresh').on('click', function () {
    $('#public-methods').multiSelect('refresh');
    return false;
});
$('#add-option').on('click', function () {
    $('#public-methods').multiSelect('addOption', {value: 42, text: 'test 42', index: 0});
    return false;
});


function searchUsers(input_id) {
    // Declare variables
    var input, filter, select, options, option, i, txtValue;
    input = document.getElementById(input_id);
    filter = input.value.toUpperCase();
    select = document.getElementById("public-methods");
    options = select.getElementsByTagName("option");

    // Loop through all table rows, and hide those who don't match the search query
    for (i = 0; i < options.length; i++) {
        option = options[i];
        txtValue = option.textContent || option.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            option.style.display = "";
        } else {
            option.style.display = "none";
        }
    }
    $('select[id=public-methods]').multiSelect('refresh');
}*/
