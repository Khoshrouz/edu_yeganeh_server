jQuery(document).ready(function ($) {
    $('select[name=group]').on('change', function () {
        var selected = $(this).find(":selected").attr('value');
        $.ajax({
            url: window.location.origin + '/admin/group/' + selected + '/majors',
            type: 'GET',
            dataType: 'json',
            
        }).done(function (data) {
            var select = $('select[name=major]');
            select.empty();
            $.each(data, function (key, value) {
                select.append('<option value=' + value.id + '>' + value.title + '</option>');
            });
        })
    });
    
    
    $('input[class=true_availability]').bootstrapToggle('on');
    $('input[class=false_availability]').bootstrapToggle('off');
    
    $('input[name=switch_availability]').on('change', function () {
        var selected = this.value;
        console.log('sending');
        $.ajax({
            url: window.location.origin + '/admin/news/availability/' + selected,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                console.log('received true');
            },
            error: function (error) {
                console.log("Error:");
                console.log(error);
                var item = $('input[name=switch_availability]');
                item.prop('checked', false);
            }
        })
    });
    
    $('input[name=switch_mail_availability]').on('change', function () {
        var selected = this.value;
        console.log('sending');
        $.ajax({
            url: window.location.origin + '/admin/mail/availability/' + selected,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                console.log('received true');
            },
            error: function (error) {
                console.log("Error:");
                console.log(error);
                var item = $('input[name=switch_mail_availability]');
                item.prop('checked', false);
            }
        })
    });
    
    
    $('input[class=true_activity]').bootstrapToggle('on');
    $('input[class=false_activity]').bootstrapToggle('off');
    
    $('input[name=switch_user_activity]').on('change', function () {
        var selected = this.value;
        console.log('sending');
        $.ajax({
            url: window.location.origin + '/admin/user/' + selected + '/activity',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                console.log('received true');
            },
            error: function (error) {
                console.log("Error:");
                console.log(error);
                var item = $('input[name=switch_user_activity]');
                item.prop('checked', false);
            }
        })
    });
    
    
    $('input[name=switch_admin_activity]').on('change', function () {
        var selected = this.value;
        console.log('sending');
        $.ajax({
            url: window.location.origin + '/admin/admin/' + selected + '/activity',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                console.log('received true');
            },
            error: function (error) {
                console.log("Error:");
                console.log(error);
                var item = $('input[name=switch_admin_activity]');
                item.prop('checked', false);
            }
        })
    });
    
    
});