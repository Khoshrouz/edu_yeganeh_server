jQuery(document).ready(function ($) {
    $('select[name=group]').on('change', function () {
        var selected = $(this).find(":selected").attr('value');
        $.ajax({
            url: window.location.origin + '/admin/group/' + selected + '/majors',
            type: 'GET',
            dataType: 'json',

        }).done(function (data) {
            var select = $('select[name=major]');
            select.empty();
            select.append('<option value="0">فرقی ندارد</option>');
            $.each(data, function (key, value) {
                select.append('<option value=' + value.id + '>' + value.title + '</option>');
            });
        })
    });
});
